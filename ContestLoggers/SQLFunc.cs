﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;

namespace ContestLoggers
{
    public class SQLFunc
    {
        public static SQLiteConnection createDB(string fileName)
        {
            SQLiteConnection sqlite_conn;
            SQLiteCommand cmd;
            // Create a new database connection:
            sqlite_conn = new SQLiteConnection(@"URI=file:"+ fileName);
            cmd = new SQLiteCommand(sqlite_conn);
            // Open the connection:
            try
            {
                sqlite_conn.Open();
                cmd.CommandText = @"CREATE TABLE ContestLog (Number INTEGER NOT NULL DEFAULT 1 PRIMARY KEY AUTOINCREMENT UNIQUE, Freq INTEGER, Type TEXT, DateTime TEXT, MyCall TEXT, MyGrid TEXT, TheirCall TEXT, TheirGrid TEXT);";
                Console.WriteLine(cmd.CommandText);
                Console.WriteLine(cmd.ExecuteNonQuery());

            }
            catch (Exception ex)
            {

                    System.Windows.Forms.MessageBox.Show("Error: Couldn't connect or create sqlite database! Debug: " + ex.Message);
                    return sqlite_conn;
                
            }
            try
            {
                sqlite_conn.Close();
                System.Windows.Forms.MessageBox.Show("Database Successfully created!");
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show("What the hell!?");
            }
            return sqlite_conn;
        

    }
        public static DBData[] readContestLogsFromDB(string fileName)
        {
            SQLiteConnection sqlite_conn;
            SQLiteCommand cmd;
            SQLiteDataReader rdr;
            // Create a new database connection:
            sqlite_conn = new SQLiteConnection(@"URI=file:" + fileName);
            cmd = new SQLiteCommand(sqlite_conn);
            
            try
            {
                sqlite_conn.Open();
                cmd.CommandText = @"SELECT * FROM ""main"".""ContestLog"" ORDER BY Number;";
                rdr = cmd.ExecuteReader();

                if (!rdr.HasRows)
                {
                    // Database is empty
                }
                var lst = new List<DBData>();
                while (rdr.Read())
                {
                    var row = new DBData();
                    row.DBmainNumber = rdr.GetInt32(0);
                    row.DBfreq = rdr.GetInt32(1);
                    row.DBtype = rdr.GetString(2);
                    row.DBdateTime= rdr.GetString(3);
                    row.DBmyCall = rdr.GetString(4);
                    row.DBmyGrid = rdr.GetString(5);
                    row.DBtheirCall = rdr.GetString(6);
                    row.DBtheirGrid = rdr.GetString(7);
                    lst.Add(row);
                }
                sqlite_conn.Close();
                return lst.ToArray();
            }
            catch
            {
                var lst = new List<DBData>();
                return lst.ToArray();
            }
        }

        public static bool writeDB(string fileName, DBData dataWrite, int mainNumber = 0) //Append a line
        {
            SQLiteConnection sqlite_conn;
            SQLiteCommand cmd;
            // Create a new database connection:
            sqlite_conn = new SQLiteConnection(@"URI=file:" + fileName);
            cmd = new SQLiteCommand(sqlite_conn);
            // Open the connection:
            try
            {
                sqlite_conn.Open();
                if (mainNumber == 0)
                {
                    cmd.CommandText = @"INSERT INTO ""main"".""ContestLog""(""Freq"",""Type"",""DateTime"",""MyCall"",""MyGrid"",""TheirCall"",""TheirGrid"") VALUES (" + Convert.ToString(dataWrite.DBfreq) + ",\"" + dataWrite.DBtype + "\",\"" + dataWrite.DBdateTime + "\",\"" + dataWrite.DBmyCall + "\",\"" + dataWrite.DBmyGrid + "\",\"" + dataWrite.DBtheirCall + "\",\"" + dataWrite.DBtheirGrid + "\");";
                }
                else
                {
                    cmd.CommandText = @"INSERT INTO ""main"".""ContestLog""(""Number"",""Freq"",""Type"",""DateTime"",""MyCall"",""MyGrid"",""TheirCall"",""TheirGrid"") VALUES ("+ Convert.ToString(mainNumber)+"," + Convert.ToString(dataWrite.DBfreq) + ",\"" + dataWrite.DBtype + "\",\"" + dataWrite.DBdateTime + "\",\"" + dataWrite.DBmyCall + "\",\"" + dataWrite.DBmyGrid + "\",\"" + dataWrite.DBtheirCall + "\",\"" + dataWrite.DBtheirGrid + "\");";
                }
                Console.WriteLine(cmd.CommandText);
                Console.WriteLine(cmd.ExecuteNonQuery());
                
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show("Error: Couldn't connect or write to sqlite database! Debug: " + ex.Message);
                return false;
            }
            try
            {
                sqlite_conn.Close();
                //System.Windows.Forms.MessageBox.Show("Database Successfully created!");
                
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show("What the hell!?"); //This shouldn't happen
                return false;
            }
            return true;


        }
    
        public static bool updateDBEntry(string fileName, DBData dataWrite, int entryNumber) //Update function
        {

            SQLiteConnection sqlite_conn;
            SQLiteCommand cmd;
            // Create a new database connection:
            sqlite_conn = new SQLiteConnection(@"URI=file:" + fileName);
            cmd = new SQLiteCommand(sqlite_conn);
            // Open the connection:
            try
            {
                sqlite_conn.Open();
                cmd.CommandText = "UPDATE \"main\".\"ContestLog\" SET \"Freq\"=" + Convert.ToString(dataWrite.DBfreq) + ",\"Type\"=\"" + dataWrite.DBtype + "\",\"MyCall\"=\"" + dataWrite.DBmyCall + "\",\"MyGrid\"=\"" + dataWrite.DBmyGrid + "\",\"TheirCall\"=\"" + dataWrite.DBtheirCall + "\",\"TheirGrid\"=\"" + dataWrite.DBtheirGrid + "\" WHERE \"Number\"=" + Convert.ToString(entryNumber) + ";";
                Console.WriteLine(cmd.CommandText);
                Console.WriteLine(cmd.ExecuteNonQuery());

            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show("Error: Couldn't connect or write to sqlite database! Debug: " + ex.Message);
                return false;
            }
            try
            {
                sqlite_conn.Close();
                //System.Windows.Forms.MessageBox.Show("Database Successfully created!");

            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show("What the hell!?"); //This shouldn't happen
                return false;
            }
            return true;


        }
        public static bool deleteDBEntry(string fileName, int entryNumber)
        {
            SQLiteConnection sqlite_conn;
            SQLiteCommand cmd;
            // Create a new database connection:
            sqlite_conn = new SQLiteConnection(@"URI=file:" + fileName);
            cmd = new SQLiteCommand(sqlite_conn);
            // Open the connection:
            try
            {
                sqlite_conn.Open();
                cmd.CommandText = "DELETE FROM \"main\".\"ContestLog\" WHERE Number IN ('" + Convert.ToString(entryNumber) + "');";
                Console.WriteLine(cmd.CommandText);
                Console.WriteLine(cmd.ExecuteNonQuery());

            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show("Error: Couldn't connect or write to sqlite database! Debug: " + ex.Message);
                return false;
            }
            try
            {
                sqlite_conn.Close();
                //System.Windows.Forms.MessageBox.Show("Database Successfully created!");

            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show("What the hell!?"); //This shouldn't happen
                return false;
            }
            return true;
        }
        public static bool renumberDBIDs(string fileName) //May not be needed, but useful for post contest CSV and Excel documents
        {
            SQLiteConnection sqlite_conn;
            SQLiteCommand cmd;
            // Create a new database connection:
            sqlite_conn = new SQLiteConnection(@"URI=file:" + fileName);
            cmd = new SQLiteCommand(sqlite_conn);
            // Open the connection:
            try
            {
                sqlite_conn.Open();
                cmd.CommandText = "SET @var_name = 0; UPDATE \"main\".\"ContestLog\" SET Number = (@var_name:= @var_name + 1); ALTER TABLE \"main\".\"ContestLog\" AUTO_INCREMENT = 1";
                Console.WriteLine(cmd.CommandText);
                Console.WriteLine(cmd.ExecuteNonQuery());

            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show("Error: Couldn't connect or write to sqlite database! Debug: " + ex.Message);
                return false;
            }
            try
            {
                sqlite_conn.Close();
                //System.Windows.Forms.MessageBox.Show("Database Successfully created!");

            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show("What the hell!?"); //This shouldn't happen
                return false;
            }
            return true;
        }
        static void cabrilloToDB(string fileName)
        {
            
        }

        static void dbToCabrillo(string fileName)
        {

        }

        public static bool customDBCommand(string fileName, string sqlCommand) //Used for import/export
        {
            SQLiteConnection sqlite_conn;
            SQLiteCommand cmd;
            // Create a new database connection:
            sqlite_conn = new SQLiteConnection(@"URI=file:" + fileName);
            cmd = new SQLiteCommand(sqlite_conn);
            // Open the connection:
            try
            {
                sqlite_conn.Open();
                cmd.CommandText = sqlCommand;
                Console.WriteLine(cmd.CommandText);
                Console.WriteLine(cmd.ExecuteNonQuery());

            }
            catch (Exception ex)
            {
               // System.Windows.Forms.MessageBox.Show("Error: Couldn't connect or write to sqlite database! Debug: " + ex.Message);
                return false;
            }
            try
            {
                sqlite_conn.Close();
                //System.Windows.Forms.MessageBox.Show("Database Successfully created!");

            }
            catch (Exception ex)
            {
               // System.Windows.Forms.MessageBox.Show("What the hell!?"); //This shouldn't happen
                return false;
            }
            return true;
        }
        public static DBData[] customDBQuery(string fileName, string sqlCommand) //Used for Import/Expord
        {
            {
                SQLiteConnection sqlite_conn;
                SQLiteCommand cmd;
                SQLiteDataReader rdr;
                // Create a new database connection:
                sqlite_conn = new SQLiteConnection(@"URI=file:" + fileName);
                cmd = new SQLiteCommand(sqlite_conn);

                try
                {
                    sqlite_conn.Open();
                    cmd.CommandText = sqlCommand;
                    rdr = cmd.ExecuteReader();

                    if (!rdr.HasRows)
                    {
                        // throw exception
                    }
                    var lst = new List<DBData>();
                    while (rdr.Read())
                    {
                        var row = new DBData();
                        row.DBmainNumber = rdr.GetInt32(0);
                        row.DBfreq = rdr.GetInt32(1);
                        row.DBtype = rdr.GetString(2);
                        row.DBdateTime = rdr.GetString(3);
                        row.DBmyCall = rdr.GetString(4);
                        row.DBmyGrid = rdr.GetString(5);
                        row.DBtheirCall = rdr.GetString(6);
                        row.DBtheirGrid = rdr.GetString(7);
                        lst.Add(row);
                    }
                    sqlite_conn.Close();
                    return lst.ToArray();
                }
                catch
                {
                    var lst = new List<DBData>();
                    return lst.ToArray();
                }
            }
        }
    }


}
    public class DBData //Spawn these as an array[] for log lists, spawn without array for single entries
    {
        public int DBmainNumber { get; set; }
        public int DBfreq { get; set; } // this might be another data type
        public string DBtype { get; set; }
        public string DBdateTime { get; set; }
        public string DBmyCall { get; set; }
        public string DBmyGrid { get; set; }
        public string DBtheirCall { get; set; }
        public string DBtheirGrid { get; set; }
    }


