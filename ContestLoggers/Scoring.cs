﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContestLoggers
{
    class Scoring
    {
        public static int calculateMultipliers(List<string> rxGridSquares, List<string> txGridSquares, bool roverAdditionalPointTX)
        {
            //send this function a DISTINCT list for accurate results (unique entries only)
            int score = 0;
            score += rxGridSquares.Count;
            if (roverAdditionalPointTX == true) { score += txGridSquares.Count; }
            return score;
        }
        public static String checkLimitedRoverRules(DBData[] scorestoCheck, bool outputToTxt = false)
        {
            List<String> concatScores = new List<String>();
            for (int i = 0; i < scorestoCheck.Length; i++)
            {

                concatScores.Add(scorestoCheck[i].DBtheirCall); //

            }
            concatScores.Sort();
            Dictionary<string, int> callsignDictionary = new Dictionary<string, int>();
            foreach (String callSigns in concatScores)
            {
                if (!callsignDictionary.ContainsKey(callSigns))
                {
                    callsignDictionary.Add(callSigns, 1);
                }
                else
                {
                    int count = 0;
                    callsignDictionary.TryGetValue(callSigns, out count);
                    callsignDictionary.Remove(callSigns);
                    callsignDictionary.Add(callSigns, count + 1);
                }
            }
            String Output = "";
            // output the results, each item with quantity
            foreach (KeyValuePair<string, int> entry in callsignDictionary)
            {
                if (entry.Value > 100)
                {
                    Output = Output + entry.Key + ": " + entry.Value + " entries! ";
                }
                    
                

            }
            return Output;
        }
        public static int calculatePoints(ScoreTable Scores, DBData[] scorestoConCat, bool outputToTXT = false) //Give your score calculation and log list to calculate
        {
            var uniqueScores = new List<DBData>();
            int dbLength = 0;
            List<String> concatScores = new List<String>();
            List<String> Dupes = new List<String>();
            for (int i = 0; i < scorestoConCat.Length; i++)
            {
                
                concatScores.Add(scorestoConCat[i].DBfreq + scorestoConCat[i].DBtheirCall + scorestoConCat[i].DBtheirGrid + scorestoConCat[i].DBmyGrid); //put the fields we care about in a single string
                concatScores = concatScores.Distinct().ToList(); //delete the last line if it's a dupe
                int currentLength = concatScores.Count();
                if (currentLength != dbLength) //if the last line was a dupe don't add it to our new DBData[] property
                {
                    uniqueScores.Add(scorestoConCat[i]);
                    dbLength = currentLength;
                }
                else
                {
                    if (outputToTXT == true)
                    {
                        Dupes.Add(scorestoConCat[i].DBmainNumber + "," + Convert.ToString(scorestoConCat[i].DBfreq) + "," + scorestoConCat[i].DBtype + "," + scorestoConCat[i].DBdateTime + "," + scorestoConCat[i].DBmyCall + "," + scorestoConCat[i].DBmyGrid + "," + scorestoConCat[i].DBtheirCall + "," + scorestoConCat[i].DBtheirGrid);
                    }
                }

            }
            if (outputToTXT == true)
            {
                System.IO.File.WriteAllLines("Dupes.csv", Dupes);
                System.Diagnostics.Process.Start("Dupes.csv");
            }
            // concatScores = concatScores.Distinct().ToList();
            int currentScore = 0;
            for (int i = 0; i < concatScores.Count(); i++)
            {
                if (uniqueScores[i].DBfreq == 50) { currentScore += Scores.m6; }
                else if (uniqueScores[i].DBfreq == 144) { currentScore += Scores.m2; }
                else if (uniqueScores[i].DBfreq == 222) { currentScore += Scores.m1; }
                else if (uniqueScores[i].DBfreq == 432) { currentScore += Scores.cm70; }
                else if (uniqueScores[i].DBfreq == 902) { currentScore += Scores.cm33; }
                else if (uniqueScores[i].DBfreq == 1200) { currentScore += Scores.cm23; }
                else if (uniqueScores[i].DBfreq == 2300) { currentScore += Scores.cm13; }
                else if (uniqueScores[i].DBfreq == 3400) { currentScore += Scores.cm9; }
                else if (uniqueScores[i].DBfreq == 5700) { currentScore += Scores.cm5; }
                else if (uniqueScores[i].DBfreq == 275000) { currentScore += Scores.light; }
            }
            return currentScore;
        }

    }

    class ScoreTable
    {
        public int m6 { get; set; }
        public int m2 { get; set; }
        public int m1 { get; set; }
        public int cm70 { get; set; }
        public int cm33 { get; set; }
        public int cm23 { get; set; }
        public int cm13 { get; set; }
        public int cm9 { get; set; }
        public int cm5 { get; set; }
        public int light { get; set; }
    }


    }

