﻿namespace ContestLoggers
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cabrilloToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aDIFToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importCSVToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.overwriteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.updateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.appendToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cabrilloToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.aDIFToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.exportCSVToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabletModeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nightModeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dBBrowserToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.renumberRowsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quickReferenceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.oKROVERToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.scoreLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.pointsLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.multiplierLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.qsoLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.rateLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.buildLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.fakeTab1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel19 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.createLogBtn = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.myCallsignTextbox = new System.Windows.Forms.TextBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.label30 = new System.Windows.Forms.Label();
            this.updateSecs = new System.Windows.Forms.NumericUpDown();
            this.network_mode_checkbox = new System.Windows.Forms.CheckBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel21 = new System.Windows.Forms.TableLayoutPanel();
            this.enable6m = new System.Windows.Forms.CheckBox();
            this.enable2m = new System.Windows.Forms.CheckBox();
            this.enable9cm = new System.Windows.Forms.CheckBox();
            this.enable1m = new System.Windows.Forms.CheckBox();
            this.enable13cm = new System.Windows.Forms.CheckBox();
            this.enable70cm = new System.Windows.Forms.CheckBox();
            this.enable23cm = new System.Windows.Forms.CheckBox();
            this.enable33cm = new System.Windows.Forms.CheckBox();
            this.enableLight = new System.Windows.Forms.CheckBox();
            this.enable5cm = new System.Windows.Forms.CheckBox();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel22 = new System.Windows.Forms.TableLayoutPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.operatorCategory = new System.Windows.Forms.ComboBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.contestType = new System.Windows.Forms.ComboBox();
            this.assisted = new System.Windows.Forms.ComboBox();
            this.stationCategory = new System.Windows.Forms.ComboBox();
            this.transmitterCategory = new System.Windows.Forms.ComboBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel20 = new System.Windows.Forms.TableLayoutPanel();
            this.logBox = new System.Windows.Forms.ListBox();
            this.tableLayoutPanel18 = new System.Windows.Forms.TableLayoutPanel();
            this.submitButton = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.clearButton = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.tableLayoutPanel17 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel15 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.radio6mLabel = new System.Windows.Forms.Label();
            this.radio6m = new System.Windows.Forms.RadioButton();
            this.radio2mLabel = new System.Windows.Forms.Label();
            this.radio2m = new System.Windows.Forms.RadioButton();
            this.radio1mLabel = new System.Windows.Forms.Label();
            this.radio1m = new System.Windows.Forms.RadioButton();
            this.radio70cmLabel = new System.Windows.Forms.Label();
            this.radio70cm = new System.Windows.Forms.RadioButton();
            this.radio33cmLabel = new System.Windows.Forms.Label();
            this.radio33cm = new System.Windows.Forms.RadioButton();
            this.radio23cmLabel = new System.Windows.Forms.Label();
            this.radio23cm = new System.Windows.Forms.RadioButton();
            this.radio13cmLabel = new System.Windows.Forms.Label();
            this.radio13cm = new System.Windows.Forms.RadioButton();
            this.radio9cmLabel = new System.Windows.Forms.Label();
            this.radio9cm = new System.Windows.Forms.RadioButton();
            this.radio5cmLabel = new System.Windows.Forms.Label();
            this.radio5cm = new System.Windows.Forms.RadioButton();
            this.radioLightLabel = new System.Windows.Forms.Label();
            this.radioLight = new System.Windows.Forms.RadioButton();
            this.utcLabel = new System.Windows.Forms.Label();
            this.ctzLabel = new System.Windows.Forms.Label();
            this.modeBox = new System.Windows.Forms.ListBox();
            this.tableLayoutPanel16 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel14 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel11 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel13 = new System.Windows.Forms.TableLayoutPanel();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.gridTXSelect5 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.gridTXSelect4 = new System.Windows.Forms.Button();
            this.gridTXSelect1 = new System.Windows.Forms.Button();
            this.gridTXSelect3 = new System.Windows.Forms.Button();
            this.gridTXSelect2 = new System.Windows.Forms.Button();
            this.label25 = new System.Windows.Forms.Label();
            this.tableLayoutPanel12 = new System.Windows.Forms.TableLayoutPanel();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.gridTX = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            this.upperLeftTX = new System.Windows.Forms.Button();
            this.lowerRightTX = new System.Windows.Forms.Button();
            this.upperTX = new System.Windows.Forms.Button();
            this.lowerTX = new System.Windows.Forms.Button();
            this.upperRightTX = new System.Windows.Forms.Button();
            this.lowerLeftTX = new System.Windows.Forms.Button();
            this.leftTX = new System.Windows.Forms.Button();
            this.rightTX = new System.Windows.Forms.Button();
            this.centerTX = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.gridRXSelect5 = new System.Windows.Forms.Button();
            this.gridRXSelect1 = new System.Windows.Forms.Button();
            this.gridRXSelect4 = new System.Windows.Forms.Button();
            this.gridRXSelect2 = new System.Windows.Forms.Button();
            this.gridRXSelect3 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.gridRX = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.callSelect5 = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.callSelect4 = new System.Windows.Forms.Button();
            this.callSelect1 = new System.Windows.Forms.Button();
            this.callSelect3 = new System.Windows.Forms.Button();
            this.callSelect2 = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.callRX = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.upperLeftRX = new System.Windows.Forms.Button();
            this.lowerRightRX = new System.Windows.Forms.Button();
            this.upperRX = new System.Windows.Forms.Button();
            this.lowerRX = new System.Windows.Forms.Button();
            this.upperRightRX = new System.Windows.Forms.Button();
            this.lowerLeftRX = new System.Windows.Forms.Button();
            this.leftRX = new System.Windows.Forms.Button();
            this.centerRX = new System.Windows.Forms.Button();
            this.rightRX = new System.Windows.Forms.Button();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.errorBox = new System.Windows.Forms.ListBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel23 = new System.Windows.Forms.TableLayoutPanel();
            this.dupeCSVButton = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.fakeTab1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tableLayoutPanel19.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.updateSecs)).BeginInit();
            this.groupBox7.SuspendLayout();
            this.tableLayoutPanel21.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.tableLayoutPanel22.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tableLayoutPanel20.SuspendLayout();
            this.tableLayoutPanel18.SuspendLayout();
            this.tableLayoutPanel17.SuspendLayout();
            this.tableLayoutPanel15.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tableLayoutPanel16.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tableLayoutPanel14.SuspendLayout();
            this.tableLayoutPanel11.SuspendLayout();
            this.tableLayoutPanel13.SuspendLayout();
            this.tableLayoutPanel12.SuspendLayout();
            this.tableLayoutPanel10.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tabPage4.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.tableLayoutPanel23.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Font = new System.Drawing.Font("OCR A Extended", 7.875F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem,
            this.toolsToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(3, 1, 0, 1);
            this.menuStrip1.Size = new System.Drawing.Size(1028, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveToolStripMenuItem,
            this.loadToolStripMenuItem,
            this.importToolStripMenuItem,
            this.exportToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Font = new System.Drawing.Font("OCR A Extended", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(60, 22);
            this.fileToolStripMenuItem.Text = "File";
            this.fileToolStripMenuItem.Click += new System.EventHandler(this.fileToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(296, 22);
            this.saveToolStripMenuItem.Text = "Connect to Log DB";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // loadToolStripMenuItem
            // 
            this.loadToolStripMenuItem.Name = "loadToolStripMenuItem";
            this.loadToolStripMenuItem.Size = new System.Drawing.Size(296, 22);
            this.loadToolStripMenuItem.Text = "Disconnect from Log DB";
            this.loadToolStripMenuItem.Click += new System.EventHandler(this.loadToolStripMenuItem_Click);
            // 
            // importToolStripMenuItem
            // 
            this.importToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cabrilloToolStripMenuItem,
            this.aDIFToolStripMenuItem,
            this.importCSVToolStripMenuItem});
            this.importToolStripMenuItem.Name = "importToolStripMenuItem";
            this.importToolStripMenuItem.Size = new System.Drawing.Size(296, 22);
            this.importToolStripMenuItem.Text = "Import";
            // 
            // cabrilloToolStripMenuItem
            // 
            this.cabrilloToolStripMenuItem.Enabled = false;
            this.cabrilloToolStripMenuItem.Name = "cabrilloToolStripMenuItem";
            this.cabrilloToolStripMenuItem.Size = new System.Drawing.Size(226, 22);
            this.cabrilloToolStripMenuItem.Text = "Import Cabrillo";
            // 
            // aDIFToolStripMenuItem
            // 
            this.aDIFToolStripMenuItem.Enabled = false;
            this.aDIFToolStripMenuItem.Name = "aDIFToolStripMenuItem";
            this.aDIFToolStripMenuItem.Size = new System.Drawing.Size(226, 22);
            this.aDIFToolStripMenuItem.Text = "Import ADIF";
            // 
            // importCSVToolStripMenuItem
            // 
            this.importCSVToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.overwriteToolStripMenuItem,
            this.updateToolStripMenuItem,
            this.appendToolStripMenuItem});
            this.importCSVToolStripMenuItem.Name = "importCSVToolStripMenuItem";
            this.importCSVToolStripMenuItem.Size = new System.Drawing.Size(226, 22);
            this.importCSVToolStripMenuItem.Text = "Import CSV";
            // 
            // overwriteToolStripMenuItem
            // 
            this.overwriteToolStripMenuItem.Name = "overwriteToolStripMenuItem";
            this.overwriteToolStripMenuItem.Size = new System.Drawing.Size(526, 22);
            this.overwriteToolStripMenuItem.Text = "Overwrite (Delete Existing Rows/Add New Rows)";
            this.overwriteToolStripMenuItem.Click += new System.EventHandler(this.overwriteToolStripMenuItem_Click);
            // 
            // updateToolStripMenuItem
            // 
            this.updateToolStripMenuItem.Name = "updateToolStripMenuItem";
            this.updateToolStripMenuItem.Size = new System.Drawing.Size(526, 22);
            this.updateToolStripMenuItem.Text = "Update (Add New Rows/Update Existing Rows)";
            this.updateToolStripMenuItem.Click += new System.EventHandler(this.updateToolStripMenuItem_Click);
            // 
            // appendToolStripMenuItem
            // 
            this.appendToolStripMenuItem.Name = "appendToolStripMenuItem";
            this.appendToolStripMenuItem.Size = new System.Drawing.Size(526, 22);
            this.appendToolStripMenuItem.Text = "Append (Add New Entries Only)";
            this.appendToolStripMenuItem.Click += new System.EventHandler(this.appendToolStripMenuItem_Click);
            // 
            // exportToolStripMenuItem
            // 
            this.exportToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cabrilloToolStripMenuItem1,
            this.aDIFToolStripMenuItem1,
            this.exportCSVToolStripMenuItem});
            this.exportToolStripMenuItem.Name = "exportToolStripMenuItem";
            this.exportToolStripMenuItem.Size = new System.Drawing.Size(296, 22);
            this.exportToolStripMenuItem.Text = "Export";
            // 
            // cabrilloToolStripMenuItem1
            // 
            this.cabrilloToolStripMenuItem1.Name = "cabrilloToolStripMenuItem1";
            this.cabrilloToolStripMenuItem1.Size = new System.Drawing.Size(296, 22);
            this.cabrilloToolStripMenuItem1.Text = "Export Cabrillo";
            this.cabrilloToolStripMenuItem1.Click += new System.EventHandler(this.cabrilloToolStripMenuItem1_Click);
            // 
            // aDIFToolStripMenuItem1
            // 
            this.aDIFToolStripMenuItem1.Enabled = false;
            this.aDIFToolStripMenuItem1.Name = "aDIFToolStripMenuItem1";
            this.aDIFToolStripMenuItem1.Size = new System.Drawing.Size(296, 22);
            this.aDIFToolStripMenuItem1.Text = "Export ADIF";
            // 
            // exportCSVToolStripMenuItem
            // 
            this.exportCSVToolStripMenuItem.Name = "exportCSVToolStripMenuItem";
            this.exportCSVToolStripMenuItem.Size = new System.Drawing.Size(296, 22);
            this.exportCSVToolStripMenuItem.Text = "Export CSV (For Excel)";
            this.exportCSVToolStripMenuItem.Click += new System.EventHandler(this.exportCSVToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(296, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.settingsToolStripMenuItem,
            this.tabletModeToolStripMenuItem,
            this.nightModeToolStripMenuItem});
            this.editToolStripMenuItem.Font = new System.Drawing.Font("OCR A Extended", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(90, 22);
            this.editToolStripMenuItem.Text = "Options";
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.settingsToolStripMenuItem.Text = "Settings";
            // 
            // tabletModeToolStripMenuItem
            // 
            this.tabletModeToolStripMenuItem.Name = "tabletModeToolStripMenuItem";
            this.tabletModeToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.tabletModeToolStripMenuItem.Text = "Tablet Mode";
            // 
            // nightModeToolStripMenuItem
            // 
            this.nightModeToolStripMenuItem.Name = "nightModeToolStripMenuItem";
            this.nightModeToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.nightModeToolStripMenuItem.Text = "Night Mode";
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dBBrowserToolStripMenuItem,
            this.renumberRowsToolStripMenuItem});
            this.toolsToolStripMenuItem.Font = new System.Drawing.Font("OCR A Extended", 12F);
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(70, 22);
            this.toolsToolStripMenuItem.Text = "Tools";
            // 
            // dBBrowserToolStripMenuItem
            // 
            this.dBBrowserToolStripMenuItem.Name = "dBBrowserToolStripMenuItem";
            this.dBBrowserToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.dBBrowserToolStripMenuItem.Text = "DB Browser";
            // 
            // renumberRowsToolStripMenuItem
            // 
            this.renumberRowsToolStripMenuItem.Name = "renumberRowsToolStripMenuItem";
            this.renumberRowsToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.renumberRowsToolStripMenuItem.Text = "Renumber Rows";
            this.renumberRowsToolStripMenuItem.Click += new System.EventHandler(this.renumberRowsToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem,
            this.quickReferenceToolStripMenuItem,
            this.oKROVERToolStripMenuItem});
            this.helpToolStripMenuItem.Font = new System.Drawing.Font("OCR A Extended", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(60, 22);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(226, 22);
            this.aboutToolStripMenuItem.Text = "About";
            // 
            // quickReferenceToolStripMenuItem
            // 
            this.quickReferenceToolStripMenuItem.Name = "quickReferenceToolStripMenuItem";
            this.quickReferenceToolStripMenuItem.Size = new System.Drawing.Size(226, 22);
            this.quickReferenceToolStripMenuItem.Text = "Quick Reference";
            // 
            // oKROVERToolStripMenuItem
            // 
            this.oKROVERToolStripMenuItem.Name = "oKROVERToolStripMenuItem";
            this.oKROVERToolStripMenuItem.Size = new System.Drawing.Size(226, 22);
            this.oKROVERToolStripMenuItem.Text = "OKROVER.INFO";
            this.oKROVERToolStripMenuItem.Click += new System.EventHandler(this.oKROVERToolStripMenuItem_Click);
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.Text = "notifyIcon1";
            this.notifyIcon1.Visible = true;
            // 
            // statusStrip1
            // 
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusLabel,
            this.scoreLabel,
            this.pointsLabel,
            this.multiplierLabel,
            this.qsoLabel,
            this.rateLabel,
            this.buildLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 724);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(0, 0, 7, 0);
            this.statusStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.ManagerRenderMode;
            this.statusStrip1.Size = new System.Drawing.Size(1028, 25);
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            this.statusStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.statusStrip1_ItemClicked);
            // 
            // statusLabel
            // 
            this.statusLabel.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.statusLabel.Font = new System.Drawing.Font("OCR A Extended", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(105, 20);
            this.statusLabel.Text = "Ready...";
            // 
            // scoreLabel
            // 
            this.scoreLabel.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.scoreLabel.Font = new System.Drawing.Font("OCR A Extended", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.scoreLabel.Name = "scoreLabel";
            this.scoreLabel.Size = new System.Drawing.Size(105, 20);
            this.scoreLabel.Text = "Score:0 ";
            // 
            // pointsLabel
            // 
            this.pointsLabel.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.pointsLabel.Font = new System.Drawing.Font("OCR A Extended", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pointsLabel.Name = "pointsLabel";
            this.pointsLabel.Size = new System.Drawing.Size(117, 20);
            this.pointsLabel.Text = "Points:0 ";
            this.pointsLabel.Click += new System.EventHandler(this.pointsLabel_Click);
            // 
            // multiplierLabel
            // 
            this.multiplierLabel.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.multiplierLabel.Font = new System.Drawing.Font("OCR A Extended", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.multiplierLabel.Name = "multiplierLabel";
            this.multiplierLabel.Size = new System.Drawing.Size(93, 20);
            this.multiplierLabel.Text = "Mult:0 ";
            // 
            // qsoLabel
            // 
            this.qsoLabel.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.qsoLabel.Font = new System.Drawing.Font("OCR A Extended", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.qsoLabel.Name = "qsoLabel";
            this.qsoLabel.Size = new System.Drawing.Size(93, 20);
            this.qsoLabel.Text = "QSOs:0 ";
            // 
            // rateLabel
            // 
            this.rateLabel.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.rateLabel.Font = new System.Drawing.Font("OCR A Extended", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rateLabel.Name = "rateLabel";
            this.rateLabel.Size = new System.Drawing.Size(129, 20);
            this.rateLabel.Text = "Rate:0CPH ";
            // 
            // buildLabel
            // 
            this.buildLabel.Font = new System.Drawing.Font("Elgethy Upper Bold", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buildLabel.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.buildLabel.Name = "buildLabel";
            this.buildLabel.Size = new System.Drawing.Size(80, 20);
            this.buildLabel.Text = "BUILDINDICATOR";
            // 
            // fakeTab1
            // 
            this.fakeTab1.Controls.Add(this.tabPage1);
            this.fakeTab1.Controls.Add(this.tabPage2);
            this.fakeTab1.Controls.Add(this.tabPage5);
            this.fakeTab1.Controls.Add(this.tabPage3);
            this.fakeTab1.Controls.Add(this.tabPage4);
            this.fakeTab1.Controls.Add(this.tabPage6);
            this.fakeTab1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fakeTab1.Font = new System.Drawing.Font("Elgethy Upper Bold", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fakeTab1.Location = new System.Drawing.Point(0, 24);
            this.fakeTab1.Margin = new System.Windows.Forms.Padding(2);
            this.fakeTab1.Name = "fakeTab1";
            this.fakeTab1.SelectedIndex = 0;
            this.fakeTab1.Size = new System.Drawing.Size(1028, 700);
            this.fakeTab1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.Black;
            this.tabPage1.Controls.Add(this.tableLayoutPanel19);
            this.tabPage1.Location = new System.Drawing.Point(4, 33);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(2);
            this.tabPage1.Size = new System.Drawing.Size(1020, 663);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "SETUP";
            // 
            // tableLayoutPanel19
            // 
            this.tableLayoutPanel19.ColumnCount = 1;
            this.tableLayoutPanel19.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel19.Controls.Add(this.groupBox5, 0, 0);
            this.tableLayoutPanel19.Controls.Add(this.groupBox6, 0, 1);
            this.tableLayoutPanel19.Controls.Add(this.groupBox7, 0, 2);
            this.tableLayoutPanel19.Controls.Add(this.groupBox8, 0, 3);
            this.tableLayoutPanel19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel19.Location = new System.Drawing.Point(2, 2);
            this.tableLayoutPanel19.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanel19.Name = "tableLayoutPanel19";
            this.tableLayoutPanel19.RowCount = 4;
            this.tableLayoutPanel19.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel19.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel19.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel19.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel19.Size = new System.Drawing.Size(1016, 659);
            this.tableLayoutPanel19.TabIndex = 3;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.createLogBtn);
            this.groupBox5.Controls.Add(this.groupBox4);
            this.groupBox5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox5.ForeColor = System.Drawing.Color.White;
            this.groupBox5.Location = new System.Drawing.Point(2, 2);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox5.Size = new System.Drawing.Size(1012, 160);
            this.groupBox5.TabIndex = 1;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "LOGGING";
            // 
            // createLogBtn
            // 
            this.createLogBtn.Dock = System.Windows.Forms.DockStyle.Right;
            this.createLogBtn.ForeColor = System.Drawing.Color.Black;
            this.createLogBtn.Location = new System.Drawing.Point(608, 25);
            this.createLogBtn.Name = "createLogBtn";
            this.createLogBtn.Size = new System.Drawing.Size(402, 133);
            this.createLogBtn.TabIndex = 1;
            this.createLogBtn.Text = "CREATE A NEW LOG DB";
            this.createLogBtn.UseVisualStyleBackColor = true;
            this.createLogBtn.Click += new System.EventHandler(this.createLogBtn_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.myCallsignTextbox);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox4.ForeColor = System.Drawing.Color.White;
            this.groupBox4.Location = new System.Drawing.Point(2, 25);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox4.Size = new System.Drawing.Size(291, 133);
            this.groupBox4.TabIndex = 0;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "YOUR CALLSIGN";
            // 
            // myCallsignTextbox
            // 
            this.myCallsignTextbox.Location = new System.Drawing.Point(3, 45);
            this.myCallsignTextbox.Margin = new System.Windows.Forms.Padding(2);
            this.myCallsignTextbox.Name = "myCallsignTextbox";
            this.myCallsignTextbox.Size = new System.Drawing.Size(255, 30);
            this.myCallsignTextbox.TabIndex = 0;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.label30);
            this.groupBox6.Controls.Add(this.updateSecs);
            this.groupBox6.Controls.Add(this.network_mode_checkbox);
            this.groupBox6.Controls.Add(this.checkBox3);
            this.groupBox6.Controls.Add(this.checkBox2);
            this.groupBox6.Controls.Add(this.checkBox1);
            this.groupBox6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox6.ForeColor = System.Drawing.Color.White;
            this.groupBox6.Location = new System.Drawing.Point(2, 166);
            this.groupBox6.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox6.Size = new System.Drawing.Size(1012, 160);
            this.groupBox6.TabIndex = 2;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "LOOK AND FEEL";
            this.groupBox6.Enter += new System.EventHandler(this.groupBox6_Enter);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(574, 60);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(279, 24);
            this.label30.TabIndex = 5;
            this.label30.Text = "SECS BETWEEN NET UPDATE:";
            // 
            // updateSecs
            // 
            this.updateSecs.Location = new System.Drawing.Point(864, 58);
            this.updateSecs.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.updateSecs.Name = "updateSecs";
            this.updateSecs.Size = new System.Drawing.Size(61, 30);
            this.updateSecs.TabIndex = 4;
            this.updateSecs.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.updateSecs.ValueChanged += new System.EventHandler(this.updateSecs_ValueChanged);
            // 
            // network_mode_checkbox
            // 
            this.network_mode_checkbox.AutoSize = true;
            this.network_mode_checkbox.Location = new System.Drawing.Point(168, 59);
            this.network_mode_checkbox.Margin = new System.Windows.Forms.Padding(2);
            this.network_mode_checkbox.Name = "network_mode_checkbox";
            this.network_mode_checkbox.Size = new System.Drawing.Size(292, 28);
            this.network_mode_checkbox.TabIndex = 3;
            this.network_mode_checkbox.Text = "NETWORK MODE (FILESHARE)";
            this.network_mode_checkbox.UseVisualStyleBackColor = true;
            this.network_mode_checkbox.CheckedChanged += new System.EventHandler(this.network_mode_checkbox_CheckedChanged);
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Location = new System.Drawing.Point(168, 27);
            this.checkBox3.Margin = new System.Windows.Forms.Padding(2);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(219, 28);
            this.checkBox3.TabIndex = 2;
            this.checkBox3.Text = "DEBUGGING CONSOLE";
            this.checkBox3.UseVisualStyleBackColor = true;
            this.checkBox3.CheckedChanged += new System.EventHandler(this.checkBox3_CheckedChanged);
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(7, 59);
            this.checkBox2.Margin = new System.Windows.Forms.Padding(2);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(137, 28);
            this.checkBox2.TabIndex = 1;
            this.checkBox2.Text = "NIGHT MODE";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(7, 27);
            this.checkBox1.Margin = new System.Windows.Forms.Padding(2);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(158, 28);
            this.checkBox1.TabIndex = 0;
            this.checkBox1.Text = "TABLET MODE";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.tableLayoutPanel21);
            this.groupBox7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox7.ForeColor = System.Drawing.Color.White;
            this.groupBox7.Location = new System.Drawing.Point(2, 330);
            this.groupBox7.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox7.Size = new System.Drawing.Size(1012, 160);
            this.groupBox7.TabIndex = 3;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "BANDS";
            // 
            // tableLayoutPanel21
            // 
            this.tableLayoutPanel21.ColumnCount = 4;
            this.tableLayoutPanel21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel21.Controls.Add(this.enable6m, 0, 0);
            this.tableLayoutPanel21.Controls.Add(this.enable2m, 0, 1);
            this.tableLayoutPanel21.Controls.Add(this.enable9cm, 2, 1);
            this.tableLayoutPanel21.Controls.Add(this.enable1m, 0, 2);
            this.tableLayoutPanel21.Controls.Add(this.enable13cm, 2, 0);
            this.tableLayoutPanel21.Controls.Add(this.enable70cm, 1, 0);
            this.tableLayoutPanel21.Controls.Add(this.enable23cm, 1, 2);
            this.tableLayoutPanel21.Controls.Add(this.enable33cm, 1, 1);
            this.tableLayoutPanel21.Controls.Add(this.enableLight, 3, 0);
            this.tableLayoutPanel21.Controls.Add(this.enable5cm, 2, 2);
            this.tableLayoutPanel21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel21.Location = new System.Drawing.Point(2, 25);
            this.tableLayoutPanel21.Name = "tableLayoutPanel21";
            this.tableLayoutPanel21.RowCount = 3;
            this.tableLayoutPanel21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel21.Size = new System.Drawing.Size(1008, 133);
            this.tableLayoutPanel21.TabIndex = 9;
            // 
            // enable6m
            // 
            this.enable6m.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.enable6m.AutoSize = true;
            this.enable6m.Checked = true;
            this.enable6m.CheckState = System.Windows.Forms.CheckState.Checked;
            this.enable6m.Location = new System.Drawing.Point(97, 8);
            this.enable6m.Margin = new System.Windows.Forms.Padding(2);
            this.enable6m.Name = "enable6m";
            this.enable6m.Size = new System.Drawing.Size(57, 28);
            this.enable6m.TabIndex = 0;
            this.enable6m.Text = "6m";
            this.enable6m.UseVisualStyleBackColor = true;
            this.enable6m.CheckedChanged += new System.EventHandler(this.enable6m_CheckedChanged);
            this.enable6m.Click += new System.EventHandler(this.enable6m_Clicked);
            // 
            // enable2m
            // 
            this.enable2m.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.enable2m.AutoSize = true;
            this.enable2m.Checked = true;
            this.enable2m.CheckState = System.Windows.Forms.CheckState.Checked;
            this.enable2m.Location = new System.Drawing.Point(97, 52);
            this.enable2m.Margin = new System.Windows.Forms.Padding(2);
            this.enable2m.Name = "enable2m";
            this.enable2m.Size = new System.Drawing.Size(57, 28);
            this.enable2m.TabIndex = 1;
            this.enable2m.Text = "2m";
            this.enable2m.UseVisualStyleBackColor = true;
            this.enable2m.CheckedChanged += new System.EventHandler(this.enable2m_CheckedChanged);
            this.enable2m.Click += new System.EventHandler(this.enable6m_Clicked);
            // 
            // enable9cm
            // 
            this.enable9cm.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.enable9cm.AutoSize = true;
            this.enable9cm.Checked = true;
            this.enable9cm.CheckState = System.Windows.Forms.CheckState.Checked;
            this.enable9cm.Location = new System.Drawing.Point(596, 52);
            this.enable9cm.Margin = new System.Windows.Forms.Padding(2);
            this.enable9cm.Name = "enable9cm";
            this.enable9cm.Size = new System.Drawing.Size(67, 28);
            this.enable9cm.TabIndex = 7;
            this.enable9cm.Text = "9cm";
            this.enable9cm.UseVisualStyleBackColor = true;
            this.enable9cm.CheckedChanged += new System.EventHandler(this.enable9cm_CheckedChanged);
            this.enable9cm.Click += new System.EventHandler(this.enable6m_Clicked);
            // 
            // enable1m
            // 
            this.enable1m.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.enable1m.AutoSize = true;
            this.enable1m.Checked = true;
            this.enable1m.CheckState = System.Windows.Forms.CheckState.Checked;
            this.enable1m.Location = new System.Drawing.Point(86, 96);
            this.enable1m.Margin = new System.Windows.Forms.Padding(2);
            this.enable1m.Name = "enable1m";
            this.enable1m.Size = new System.Drawing.Size(79, 28);
            this.enable1m.TabIndex = 2;
            this.enable1m.Text = "1.25m";
            this.enable1m.UseVisualStyleBackColor = true;
            this.enable1m.CheckedChanged += new System.EventHandler(this.enable1m_CheckedChanged);
            this.enable1m.Click += new System.EventHandler(this.enable6m_Clicked);
            // 
            // enable13cm
            // 
            this.enable13cm.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.enable13cm.AutoSize = true;
            this.enable13cm.Checked = true;
            this.enable13cm.CheckState = System.Windows.Forms.CheckState.Checked;
            this.enable13cm.Location = new System.Drawing.Point(593, 8);
            this.enable13cm.Margin = new System.Windows.Forms.Padding(2);
            this.enable13cm.Name = "enable13cm";
            this.enable13cm.Size = new System.Drawing.Size(74, 28);
            this.enable13cm.TabIndex = 6;
            this.enable13cm.Text = "13cm";
            this.enable13cm.UseVisualStyleBackColor = true;
            this.enable13cm.CheckedChanged += new System.EventHandler(this.enable13cm_CheckedChanged);
            this.enable13cm.Click += new System.EventHandler(this.enable6m_Clicked);
            // 
            // enable70cm
            // 
            this.enable70cm.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.enable70cm.AutoSize = true;
            this.enable70cm.Checked = true;
            this.enable70cm.CheckState = System.Windows.Forms.CheckState.Checked;
            this.enable70cm.Location = new System.Drawing.Point(338, 8);
            this.enable70cm.Margin = new System.Windows.Forms.Padding(2);
            this.enable70cm.Name = "enable70cm";
            this.enable70cm.Size = new System.Drawing.Size(79, 28);
            this.enable70cm.TabIndex = 3;
            this.enable70cm.Text = "70cm";
            this.enable70cm.UseVisualStyleBackColor = true;
            this.enable70cm.CheckedChanged += new System.EventHandler(this.enable70cm_CheckedChanged);
            this.enable70cm.Click += new System.EventHandler(this.enable6m_Clicked);
            // 
            // enable23cm
            // 
            this.enable23cm.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.enable23cm.AutoSize = true;
            this.enable23cm.Checked = true;
            this.enable23cm.CheckState = System.Windows.Forms.CheckState.Checked;
            this.enable23cm.Location = new System.Drawing.Point(338, 96);
            this.enable23cm.Margin = new System.Windows.Forms.Padding(2);
            this.enable23cm.Name = "enable23cm";
            this.enable23cm.Size = new System.Drawing.Size(79, 28);
            this.enable23cm.TabIndex = 5;
            this.enable23cm.Text = "23cm";
            this.enable23cm.UseVisualStyleBackColor = true;
            this.enable23cm.CheckedChanged += new System.EventHandler(this.enable23cm_CheckedChanged);
            this.enable23cm.Click += new System.EventHandler(this.enable6m_Clicked);
            // 
            // enable33cm
            // 
            this.enable33cm.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.enable33cm.AutoSize = true;
            this.enable33cm.Checked = true;
            this.enable33cm.CheckState = System.Windows.Forms.CheckState.Checked;
            this.enable33cm.Location = new System.Drawing.Point(338, 52);
            this.enable33cm.Margin = new System.Windows.Forms.Padding(2);
            this.enable33cm.Name = "enable33cm";
            this.enable33cm.Size = new System.Drawing.Size(79, 28);
            this.enable33cm.TabIndex = 4;
            this.enable33cm.Text = "33cm";
            this.enable33cm.UseVisualStyleBackColor = true;
            this.enable33cm.CheckedChanged += new System.EventHandler(this.enable33cm_CheckedChanged);
            this.enable33cm.Click += new System.EventHandler(this.enable6m_Clicked);
            // 
            // enableLight
            // 
            this.enableLight.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.enableLight.AutoSize = true;
            this.enableLight.Checked = true;
            this.enableLight.CheckState = System.Windows.Forms.CheckState.Checked;
            this.enableLight.Location = new System.Drawing.Point(849, 8);
            this.enableLight.Margin = new System.Windows.Forms.Padding(2);
            this.enableLight.Name = "enableLight";
            this.enableLight.Size = new System.Drawing.Size(65, 28);
            this.enableLight.TabIndex = 8;
            this.enableLight.Text = "light";
            this.enableLight.UseVisualStyleBackColor = true;
            this.enableLight.CheckedChanged += new System.EventHandler(this.enableLight_CheckedChanged);
            this.enableLight.Click += new System.EventHandler(this.enable6m_Clicked);
            // 
            // enable5cm
            // 
            this.enable5cm.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.enable5cm.AutoSize = true;
            this.enable5cm.Checked = true;
            this.enable5cm.CheckState = System.Windows.Forms.CheckState.Checked;
            this.enable5cm.Location = new System.Drawing.Point(596, 96);
            this.enable5cm.Margin = new System.Windows.Forms.Padding(2);
            this.enable5cm.Name = "enable5cm";
            this.enable5cm.Size = new System.Drawing.Size(67, 28);
            this.enable5cm.TabIndex = 9;
            this.enable5cm.Text = "5cm";
            this.enable5cm.UseVisualStyleBackColor = true;
            this.enable5cm.CheckedChanged += new System.EventHandler(this.enable5cm_CheckedChanged);
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.tableLayoutPanel22);
            this.groupBox8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox8.ForeColor = System.Drawing.Color.White;
            this.groupBox8.Location = new System.Drawing.Point(3, 495);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(1010, 161);
            this.groupBox8.TabIndex = 4;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "CONTEST AND SCORING";
            // 
            // tableLayoutPanel22
            // 
            this.tableLayoutPanel22.ColumnCount = 5;
            this.tableLayoutPanel22.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel22.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel22.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel22.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel22.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel22.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel22.Controls.Add(this.label26, 1, 0);
            this.tableLayoutPanel22.Controls.Add(this.operatorCategory, 1, 1);
            this.tableLayoutPanel22.Controls.Add(this.label27, 2, 0);
            this.tableLayoutPanel22.Controls.Add(this.label28, 3, 0);
            this.tableLayoutPanel22.Controls.Add(this.label29, 4, 0);
            this.tableLayoutPanel22.Controls.Add(this.contestType, 0, 1);
            this.tableLayoutPanel22.Controls.Add(this.assisted, 4, 1);
            this.tableLayoutPanel22.Controls.Add(this.stationCategory, 3, 1);
            this.tableLayoutPanel22.Controls.Add(this.transmitterCategory, 2, 1);
            this.tableLayoutPanel22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel22.Location = new System.Drawing.Point(3, 26);
            this.tableLayoutPanel22.Name = "tableLayoutPanel22";
            this.tableLayoutPanel22.RowCount = 2;
            this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel22.Size = new System.Drawing.Size(1004, 132);
            this.tableLayoutPanel22.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(25, 21);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(149, 24);
            this.label2.TabIndex = 3;
            this.label2.Text = "CONTEST TYPE";
            // 
            // label26
            // 
            this.label26.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(243, 9);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(113, 48);
            this.label26.TabIndex = 10;
            this.label26.Text = "OPERATOR CATEGORY";
            // 
            // operatorCategory
            // 
            this.operatorCategory.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.operatorCategory.FormattingEnabled = true;
            this.operatorCategory.Items.AddRange(new object[] {
            "ROVER"});
            this.operatorCategory.Location = new System.Drawing.Point(209, 88);
            this.operatorCategory.Name = "operatorCategory";
            this.operatorCategory.Size = new System.Drawing.Size(181, 32);
            this.operatorCategory.TabIndex = 34;
            // 
            // label27
            // 
            this.label27.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(406, 21);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(188, 24);
            this.label27.TabIndex = 11;
            this.label27.Text = "STATION CATEGORY";
            // 
            // label28
            // 
            this.label28.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(624, 21);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(152, 24);
            this.label28.TabIndex = 12;
            this.label28.Text = "STATION RULES";
            // 
            // label29
            // 
            this.label29.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(853, 21);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(97, 24);
            this.label29.TabIndex = 13;
            this.label29.Text = "ASSISTED";
            // 
            // contestType
            // 
            this.contestType.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.contestType.FormattingEnabled = true;
            this.contestType.Items.AddRange(new object[] {
            "ARRL-VHF-JAN",
            "ARRL-VHF-JUN",
            "ARRL-VHF-SEP",
            "CUSTOM"});
            this.contestType.Location = new System.Drawing.Point(9, 88);
            this.contestType.Name = "contestType";
            this.contestType.Size = new System.Drawing.Size(181, 32);
            this.contestType.TabIndex = 9;
            this.contestType.Text = "ARRL-VHF-JUN";
            this.contestType.SelectedIndexChanged += new System.EventHandler(this.contestType_SelectedIndexChanged);
            // 
            // assisted
            // 
            this.assisted.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.assisted.FormattingEnabled = true;
            this.assisted.Items.AddRange(new object[] {
            "ASSISTED",
            "NON-ASSISTED"});
            this.assisted.Location = new System.Drawing.Point(810, 88);
            this.assisted.Name = "assisted";
            this.assisted.Size = new System.Drawing.Size(184, 32);
            this.assisted.TabIndex = 37;
            // 
            // stationCategory
            // 
            this.stationCategory.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.stationCategory.FormattingEnabled = true;
            this.stationCategory.Items.AddRange(new object[] {
            "ROVER",
            "LIMITED ROVER"});
            this.stationCategory.Location = new System.Drawing.Point(609, 88);
            this.stationCategory.Name = "stationCategory";
            this.stationCategory.Size = new System.Drawing.Size(181, 32);
            this.stationCategory.TabIndex = 35;
            // 
            // transmitterCategory
            // 
            this.transmitterCategory.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.transmitterCategory.FormattingEnabled = true;
            this.transmitterCategory.Items.AddRange(new object[] {
            "ONE",
            "TWO",
            "LIMITED",
            "UNLIMITED"});
            this.transmitterCategory.Location = new System.Drawing.Point(409, 88);
            this.transmitterCategory.Name = "transmitterCategory";
            this.transmitterCategory.Size = new System.Drawing.Size(181, 32);
            this.transmitterCategory.TabIndex = 36;
            this.transmitterCategory.Text = "UNLIMITED";
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.Black;
            this.tabPage2.Controls.Add(this.tableLayoutPanel20);
            this.tabPage2.Location = new System.Drawing.Point(4, 33);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(2);
            this.tabPage2.Size = new System.Drawing.Size(1020, 663);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "CONTEST";
            this.tabPage2.Click += new System.EventHandler(this.tabPage2_Click);
            // 
            // tableLayoutPanel20
            // 
            this.tableLayoutPanel20.ColumnCount = 1;
            this.tableLayoutPanel20.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel20.Controls.Add(this.logBox, 0, 2);
            this.tableLayoutPanel20.Controls.Add(this.tableLayoutPanel18, 0, 1);
            this.tableLayoutPanel20.Controls.Add(this.tableLayoutPanel17, 0, 0);
            this.tableLayoutPanel20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel20.Location = new System.Drawing.Point(2, 2);
            this.tableLayoutPanel20.Name = "tableLayoutPanel20";
            this.tableLayoutPanel20.RowCount = 3;
            this.tableLayoutPanel20.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 83F));
            this.tableLayoutPanel20.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7F));
            this.tableLayoutPanel20.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel20.Size = new System.Drawing.Size(1016, 659);
            this.tableLayoutPanel20.TabIndex = 60;
            // 
            // logBox
            // 
            this.logBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.logBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.logBox.FormattingEnabled = true;
            this.logBox.ItemHeight = 16;
            this.logBox.Location = new System.Drawing.Point(3, 595);
            this.logBox.Name = "logBox";
            this.logBox.Size = new System.Drawing.Size(1010, 61);
            this.logBox.TabIndex = 64;
            this.logBox.TabStop = false;
            this.logBox.SelectedValueChanged += new System.EventHandler(this.logBox_SelectedValueChanged);
            // 
            // tableLayoutPanel18
            // 
            this.tableLayoutPanel18.ColumnCount = 4;
            this.tableLayoutPanel18.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel18.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel18.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel18.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel18.Controls.Add(this.submitButton, 0, 0);
            this.tableLayoutPanel18.Controls.Add(this.button1, 1, 0);
            this.tableLayoutPanel18.Controls.Add(this.clearButton, 2, 0);
            this.tableLayoutPanel18.Controls.Add(this.button2, 3, 0);
            this.tableLayoutPanel18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel18.Location = new System.Drawing.Point(3, 549);
            this.tableLayoutPanel18.Name = "tableLayoutPanel18";
            this.tableLayoutPanel18.RowCount = 1;
            this.tableLayoutPanel18.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel18.Size = new System.Drawing.Size(1010, 40);
            this.tableLayoutPanel18.TabIndex = 71;
            // 
            // submitButton
            // 
            this.submitButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.submitButton.Font = new System.Drawing.Font("OCR A Extended", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.submitButton.Location = new System.Drawing.Point(2, 2);
            this.submitButton.Margin = new System.Windows.Forms.Padding(2);
            this.submitButton.Name = "submitButton";
            this.submitButton.Size = new System.Drawing.Size(248, 36);
            this.submitButton.TabIndex = 61;
            this.submitButton.TabStop = false;
            this.submitButton.Text = "Submit";
            this.submitButton.UseVisualStyleBackColor = true;
            this.submitButton.Click += new System.EventHandler(this.submitButton_Click);
            // 
            // button1
            // 
            this.button1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button1.Font = new System.Drawing.Font("OCR A Extended", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(255, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(246, 34);
            this.button1.TabIndex = 67;
            this.button1.Text = "Update Entry";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // clearButton
            // 
            this.clearButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.clearButton.Font = new System.Drawing.Font("OCR A Extended", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clearButton.Location = new System.Drawing.Point(506, 2);
            this.clearButton.Margin = new System.Windows.Forms.Padding(2);
            this.clearButton.Name = "clearButton";
            this.clearButton.Size = new System.Drawing.Size(248, 36);
            this.clearButton.TabIndex = 63;
            this.clearButton.TabStop = false;
            this.clearButton.Text = "Clear";
            this.clearButton.UseVisualStyleBackColor = true;
            this.clearButton.Click += new System.EventHandler(this.clearButton_Click);
            // 
            // button2
            // 
            this.button2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button2.Font = new System.Drawing.Font("OCR A Extended", 16F);
            this.button2.Location = new System.Drawing.Point(759, 3);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(248, 34);
            this.button2.TabIndex = 68;
            this.button2.Text = "Delete Entry";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // tableLayoutPanel17
            // 
            this.tableLayoutPanel17.AutoSize = true;
            this.tableLayoutPanel17.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel17.ColumnCount = 2;
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel17.Controls.Add(this.tableLayoutPanel15, 1, 0);
            this.tableLayoutPanel17.Controls.Add(this.tableLayoutPanel16, 0, 0);
            this.tableLayoutPanel17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel17.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel17.Name = "tableLayoutPanel17";
            this.tableLayoutPanel17.RowCount = 1;
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel17.Size = new System.Drawing.Size(1010, 540);
            this.tableLayoutPanel17.TabIndex = 70;
            // 
            // tableLayoutPanel15
            // 
            this.tableLayoutPanel15.AutoSize = true;
            this.tableLayoutPanel15.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel15.ColumnCount = 1;
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel15.Controls.Add(this.groupBox3, 0, 0);
            this.tableLayoutPanel15.Controls.Add(this.utcLabel, 0, 3);
            this.tableLayoutPanel15.Controls.Add(this.ctzLabel, 0, 2);
            this.tableLayoutPanel15.Controls.Add(this.modeBox, 0, 1);
            this.tableLayoutPanel15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel15.Location = new System.Drawing.Point(757, 3);
            this.tableLayoutPanel15.Name = "tableLayoutPanel15";
            this.tableLayoutPanel15.RowCount = 4;
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel15.Size = new System.Drawing.Size(250, 534);
            this.tableLayoutPanel15.TabIndex = 68;
            // 
            // groupBox3
            // 
            this.groupBox3.AutoSize = true;
            this.groupBox3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupBox3.Controls.Add(this.radio6mLabel);
            this.groupBox3.Controls.Add(this.radio6m);
            this.groupBox3.Controls.Add(this.radio2mLabel);
            this.groupBox3.Controls.Add(this.radio2m);
            this.groupBox3.Controls.Add(this.radio1mLabel);
            this.groupBox3.Controls.Add(this.radio1m);
            this.groupBox3.Controls.Add(this.radio70cmLabel);
            this.groupBox3.Controls.Add(this.radio70cm);
            this.groupBox3.Controls.Add(this.radio33cmLabel);
            this.groupBox3.Controls.Add(this.radio33cm);
            this.groupBox3.Controls.Add(this.radio23cmLabel);
            this.groupBox3.Controls.Add(this.radio23cm);
            this.groupBox3.Controls.Add(this.radio13cmLabel);
            this.groupBox3.Controls.Add(this.radio13cm);
            this.groupBox3.Controls.Add(this.radio9cmLabel);
            this.groupBox3.Controls.Add(this.radio9cm);
            this.groupBox3.Controls.Add(this.radio5cmLabel);
            this.groupBox3.Controls.Add(this.radio5cm);
            this.groupBox3.Controls.Add(this.radioLightLabel);
            this.groupBox3.Controls.Add(this.radioLight);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Font = new System.Drawing.Font("OCR A Extended", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.groupBox3.Location = new System.Drawing.Point(3, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(244, 437);
            this.groupBox3.TabIndex = 62;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "BAND";
            // 
            // radio6mLabel
            // 
            this.radio6mLabel.AutoSize = true;
            this.radio6mLabel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.radio6mLabel.Font = new System.Drawing.Font("OCR A Extended", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radio6mLabel.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.radio6mLabel.Location = new System.Drawing.Point(3, 34);
            this.radio6mLabel.Name = "radio6mLabel";
            this.radio6mLabel.Size = new System.Drawing.Size(68, 12);
            this.radio6mLabel.TabIndex = 47;
            this.radio6mLabel.Text = "50 MHz F1";
            // 
            // radio6m
            // 
            this.radio6m.AutoSize = true;
            this.radio6m.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.radio6m.Font = new System.Drawing.Font("OCR A Extended", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radio6m.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.radio6m.Location = new System.Drawing.Point(3, 46);
            this.radio6m.Name = "radio6m";
            this.radio6m.Size = new System.Drawing.Size(238, 28);
            this.radio6m.TabIndex = 5;
            this.radio6m.Text = "6m";
            this.radio6m.UseVisualStyleBackColor = true;
            this.radio6m.CheckedChanged += new System.EventHandler(this.radio6m_CheckedChanged);
            // 
            // radio2mLabel
            // 
            this.radio2mLabel.AutoSize = true;
            this.radio2mLabel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.radio2mLabel.Font = new System.Drawing.Font("OCR A Extended", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radio2mLabel.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.radio2mLabel.Location = new System.Drawing.Point(3, 74);
            this.radio2mLabel.Name = "radio2mLabel";
            this.radio2mLabel.Size = new System.Drawing.Size(75, 12);
            this.radio2mLabel.TabIndex = 48;
            this.radio2mLabel.Text = "144 MHz F2";
            // 
            // radio2m
            // 
            this.radio2m.AutoSize = true;
            this.radio2m.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.radio2m.Font = new System.Drawing.Font("OCR A Extended", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radio2m.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.radio2m.Location = new System.Drawing.Point(3, 86);
            this.radio2m.Name = "radio2m";
            this.radio2m.Size = new System.Drawing.Size(238, 28);
            this.radio2m.TabIndex = 6;
            this.radio2m.Text = "2m";
            this.radio2m.UseVisualStyleBackColor = true;
            this.radio2m.CheckedChanged += new System.EventHandler(this.radio2m_CheckedChanged);
            // 
            // radio1mLabel
            // 
            this.radio1mLabel.AutoSize = true;
            this.radio1mLabel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.radio1mLabel.Font = new System.Drawing.Font("OCR A Extended", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radio1mLabel.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.radio1mLabel.Location = new System.Drawing.Point(3, 114);
            this.radio1mLabel.Name = "radio1mLabel";
            this.radio1mLabel.Size = new System.Drawing.Size(75, 12);
            this.radio1mLabel.TabIndex = 49;
            this.radio1mLabel.Text = "223 MHz F3";
            // 
            // radio1m
            // 
            this.radio1m.AutoSize = true;
            this.radio1m.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.radio1m.Font = new System.Drawing.Font("OCR A Extended", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radio1m.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.radio1m.Location = new System.Drawing.Point(3, 126);
            this.radio1m.Name = "radio1m";
            this.radio1m.Size = new System.Drawing.Size(238, 28);
            this.radio1m.TabIndex = 7;
            this.radio1m.Text = "1.25m";
            this.radio1m.UseVisualStyleBackColor = true;
            this.radio1m.CheckedChanged += new System.EventHandler(this.radio1m_CheckedChanged);
            // 
            // radio70cmLabel
            // 
            this.radio70cmLabel.AutoSize = true;
            this.radio70cmLabel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.radio70cmLabel.Font = new System.Drawing.Font("OCR A Extended", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radio70cmLabel.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.radio70cmLabel.Location = new System.Drawing.Point(3, 154);
            this.radio70cmLabel.Name = "radio70cmLabel";
            this.radio70cmLabel.Size = new System.Drawing.Size(75, 12);
            this.radio70cmLabel.TabIndex = 50;
            this.radio70cmLabel.Text = "440 MHz F4";
            // 
            // radio70cm
            // 
            this.radio70cm.AutoSize = true;
            this.radio70cm.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.radio70cm.Font = new System.Drawing.Font("OCR A Extended", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radio70cm.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.radio70cm.Location = new System.Drawing.Point(3, 166);
            this.radio70cm.Name = "radio70cm";
            this.radio70cm.Size = new System.Drawing.Size(238, 28);
            this.radio70cm.TabIndex = 8;
            this.radio70cm.Text = "70cm";
            this.radio70cm.UseVisualStyleBackColor = true;
            this.radio70cm.CheckedChanged += new System.EventHandler(this.radio70cm_CheckedChanged);
            // 
            // radio33cmLabel
            // 
            this.radio33cmLabel.AutoSize = true;
            this.radio33cmLabel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.radio33cmLabel.Font = new System.Drawing.Font("OCR A Extended", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radio33cmLabel.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.radio33cmLabel.Location = new System.Drawing.Point(3, 194);
            this.radio33cmLabel.Name = "radio33cmLabel";
            this.radio33cmLabel.Size = new System.Drawing.Size(75, 12);
            this.radio33cmLabel.TabIndex = 51;
            this.radio33cmLabel.Text = "900 MHz F5";
            // 
            // radio33cm
            // 
            this.radio33cm.AutoSize = true;
            this.radio33cm.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.radio33cm.Font = new System.Drawing.Font("OCR A Extended", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radio33cm.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.radio33cm.Location = new System.Drawing.Point(3, 206);
            this.radio33cm.Name = "radio33cm";
            this.radio33cm.Size = new System.Drawing.Size(238, 28);
            this.radio33cm.TabIndex = 42;
            this.radio33cm.Text = "33cm";
            this.radio33cm.UseVisualStyleBackColor = true;
            this.radio33cm.CheckedChanged += new System.EventHandler(this.radio33cm_CheckedChanged);
            // 
            // radio23cmLabel
            // 
            this.radio23cmLabel.AutoSize = true;
            this.radio23cmLabel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.radio23cmLabel.Font = new System.Drawing.Font("OCR A Extended", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radio23cmLabel.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.radio23cmLabel.Location = new System.Drawing.Point(3, 234);
            this.radio23cmLabel.Name = "radio23cmLabel";
            this.radio23cmLabel.Size = new System.Drawing.Size(75, 12);
            this.radio23cmLabel.TabIndex = 52;
            this.radio23cmLabel.Text = "1.2 GHz F6";
            // 
            // radio23cm
            // 
            this.radio23cm.AutoSize = true;
            this.radio23cm.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.radio23cm.Font = new System.Drawing.Font("OCR A Extended", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radio23cm.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.radio23cm.Location = new System.Drawing.Point(3, 246);
            this.radio23cm.Name = "radio23cm";
            this.radio23cm.Size = new System.Drawing.Size(238, 28);
            this.radio23cm.TabIndex = 44;
            this.radio23cm.Text = "23cm";
            this.radio23cm.UseVisualStyleBackColor = true;
            this.radio23cm.CheckedChanged += new System.EventHandler(this.radio23cm_CheckedChanged);
            // 
            // radio13cmLabel
            // 
            this.radio13cmLabel.AutoSize = true;
            this.radio13cmLabel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.radio13cmLabel.Font = new System.Drawing.Font("OCR A Extended", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radio13cmLabel.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.radio13cmLabel.Location = new System.Drawing.Point(3, 274);
            this.radio13cmLabel.Name = "radio13cmLabel";
            this.radio13cmLabel.Size = new System.Drawing.Size(75, 12);
            this.radio13cmLabel.TabIndex = 53;
            this.radio13cmLabel.Text = "2.4 GHz F7";
            // 
            // radio13cm
            // 
            this.radio13cm.AutoSize = true;
            this.radio13cm.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.radio13cm.Font = new System.Drawing.Font("OCR A Extended", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radio13cm.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.radio13cm.Location = new System.Drawing.Point(3, 286);
            this.radio13cm.Name = "radio13cm";
            this.radio13cm.Size = new System.Drawing.Size(238, 28);
            this.radio13cm.TabIndex = 43;
            this.radio13cm.Text = "13cm";
            this.radio13cm.UseVisualStyleBackColor = true;
            this.radio13cm.CheckedChanged += new System.EventHandler(this.radio13cm_CheckedChanged);
            // 
            // radio9cmLabel
            // 
            this.radio9cmLabel.AutoSize = true;
            this.radio9cmLabel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.radio9cmLabel.Font = new System.Drawing.Font("OCR A Extended", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radio9cmLabel.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.radio9cmLabel.Location = new System.Drawing.Point(3, 314);
            this.radio9cmLabel.Name = "radio9cmLabel";
            this.radio9cmLabel.Size = new System.Drawing.Size(75, 12);
            this.radio9cmLabel.TabIndex = 54;
            this.radio9cmLabel.Text = "3.4 GHz F8";
            // 
            // radio9cm
            // 
            this.radio9cm.AutoSize = true;
            this.radio9cm.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.radio9cm.Font = new System.Drawing.Font("OCR A Extended", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radio9cm.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.radio9cm.Location = new System.Drawing.Point(3, 326);
            this.radio9cm.Name = "radio9cm";
            this.radio9cm.Size = new System.Drawing.Size(238, 28);
            this.radio9cm.TabIndex = 45;
            this.radio9cm.Text = "9cm";
            this.radio9cm.UseVisualStyleBackColor = true;
            this.radio9cm.CheckedChanged += new System.EventHandler(this.radio9cm_CheckedChanged);
            // 
            // radio5cmLabel
            // 
            this.radio5cmLabel.AutoSize = true;
            this.radio5cmLabel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.radio5cmLabel.Font = new System.Drawing.Font("OCR A Extended", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radio5cmLabel.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.radio5cmLabel.Location = new System.Drawing.Point(3, 354);
            this.radio5cmLabel.Name = "radio5cmLabel";
            this.radio5cmLabel.Size = new System.Drawing.Size(68, 12);
            this.radio5cmLabel.TabIndex = 57;
            this.radio5cmLabel.Text = "5.7GHz F9";
            // 
            // radio5cm
            // 
            this.radio5cm.AutoSize = true;
            this.radio5cm.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.radio5cm.Font = new System.Drawing.Font("OCR A Extended", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radio5cm.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.radio5cm.Location = new System.Drawing.Point(3, 366);
            this.radio5cm.Name = "radio5cm";
            this.radio5cm.Size = new System.Drawing.Size(238, 28);
            this.radio5cm.TabIndex = 56;
            this.radio5cm.Text = "5cm";
            this.radio5cm.UseVisualStyleBackColor = true;
            this.radio5cm.CheckedChanged += new System.EventHandler(this.radio5cm_CheckedChanged);
            // 
            // radioLightLabel
            // 
            this.radioLightLabel.AutoSize = true;
            this.radioLightLabel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.radioLightLabel.Font = new System.Drawing.Font("OCR A Extended", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioLightLabel.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.radioLightLabel.Location = new System.Drawing.Point(3, 394);
            this.radioLightLabel.Name = "radioLightLabel";
            this.radioLightLabel.Size = new System.Drawing.Size(89, 12);
            this.radioLightLabel.TabIndex = 55;
            this.radioLightLabel.Text = "275 Ghz+ F10";
            // 
            // radioLight
            // 
            this.radioLight.AutoSize = true;
            this.radioLight.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.radioLight.Font = new System.Drawing.Font("OCR A Extended", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioLight.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.radioLight.Location = new System.Drawing.Point(3, 406);
            this.radioLight.Name = "radioLight";
            this.radioLight.Size = new System.Drawing.Size(238, 28);
            this.radioLight.TabIndex = 46;
            this.radioLight.Text = "light";
            this.radioLight.UseVisualStyleBackColor = true;
            this.radioLight.CheckedChanged += new System.EventHandler(this.radioLight_CheckedChanged);
            // 
            // utcLabel
            // 
            this.utcLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.utcLabel.AutoSize = true;
            this.utcLabel.Font = new System.Drawing.Font("Elgethy Upper Bold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.utcLabel.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.utcLabel.Location = new System.Drawing.Point(77, 511);
            this.utcLabel.Name = "utcLabel";
            this.utcLabel.Size = new System.Drawing.Size(95, 23);
            this.utcLabel.TabIndex = 65;
            this.utcLabel.Text = "UTC: 12:00";
            this.utcLabel.Click += new System.EventHandler(this.utcLabel_Click);
            // 
            // ctzLabel
            // 
            this.ctzLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ctzLabel.AutoSize = true;
            this.ctzLabel.Font = new System.Drawing.Font("Elgethy Upper Bold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctzLabel.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ctzLabel.Location = new System.Drawing.Point(64, 488);
            this.ctzLabel.Name = "ctzLabel";
            this.ctzLabel.Size = new System.Drawing.Size(122, 23);
            this.ctzLabel.TabIndex = 66;
            this.ctzLabel.Text = "LOCAL: 07:00";
            // 
            // modeBox
            // 
            this.modeBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.modeBox.Font = new System.Drawing.Font("Elgethy Upper Bold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.modeBox.FormattingEnabled = true;
            this.modeBox.ItemHeight = 18;
            this.modeBox.Items.AddRange(new object[] {
            "MODE: PH (PHONE)",
            "MODE: DG (DIGITAL)",
            "MODE: CW (MORSE)"});
            this.modeBox.Location = new System.Drawing.Point(3, 446);
            this.modeBox.MinimumSize = new System.Drawing.Size(4, 56);
            this.modeBox.Name = "modeBox";
            this.modeBox.Size = new System.Drawing.Size(244, 56);
            this.modeBox.TabIndex = 67;
            // 
            // tableLayoutPanel16
            // 
            this.tableLayoutPanel16.AutoSize = true;
            this.tableLayoutPanel16.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel16.ColumnCount = 1;
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel16.Controls.Add(this.groupBox2, 0, 1);
            this.tableLayoutPanel16.Controls.Add(this.groupBox1, 0, 0);
            this.tableLayoutPanel16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel16.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel16.Name = "tableLayoutPanel16";
            this.tableLayoutPanel16.RowCount = 2;
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 65F));
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tableLayoutPanel16.Size = new System.Drawing.Size(748, 534);
            this.tableLayoutPanel16.TabIndex = 69;
            // 
            // groupBox2
            // 
            this.groupBox2.AutoSize = true;
            this.groupBox2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupBox2.Controls.Add(this.tableLayoutPanel14);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Font = new System.Drawing.Font("Elgethy Upper Bold", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.SystemColors.Window;
            this.groupBox2.Location = new System.Drawing.Point(2, 349);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox2.Size = new System.Drawing.Size(744, 183);
            this.groupBox2.TabIndex = 60;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "TX";
            // 
            // tableLayoutPanel14
            // 
            this.tableLayoutPanel14.ColumnCount = 2;
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 61.61616F));
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 38.38384F));
            this.tableLayoutPanel14.Controls.Add(this.tableLayoutPanel11, 0, 0);
            this.tableLayoutPanel14.Controls.Add(this.tableLayoutPanel10, 1, 0);
            this.tableLayoutPanel14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel14.Location = new System.Drawing.Point(2, 25);
            this.tableLayoutPanel14.Name = "tableLayoutPanel14";
            this.tableLayoutPanel14.RowCount = 1;
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel14.Size = new System.Drawing.Size(740, 156);
            this.tableLayoutPanel14.TabIndex = 53;
            // 
            // tableLayoutPanel11
            // 
            this.tableLayoutPanel11.AutoSize = true;
            this.tableLayoutPanel11.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel11.ColumnCount = 1;
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel11.Controls.Add(this.tableLayoutPanel13, 0, 3);
            this.tableLayoutPanel11.Controls.Add(this.label25, 0, 0);
            this.tableLayoutPanel11.Controls.Add(this.tableLayoutPanel12, 0, 2);
            this.tableLayoutPanel11.Controls.Add(this.gridTX, 0, 1);
            this.tableLayoutPanel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel11.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel11.Name = "tableLayoutPanel11";
            this.tableLayoutPanel11.RowCount = 4;
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel11.Size = new System.Drawing.Size(449, 150);
            this.tableLayoutPanel11.TabIndex = 52;
            // 
            // tableLayoutPanel13
            // 
            this.tableLayoutPanel13.AutoSize = true;
            this.tableLayoutPanel13.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel13.ColumnCount = 5;
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel13.Controls.Add(this.label22, 0, 0);
            this.tableLayoutPanel13.Controls.Add(this.label21, 1, 0);
            this.tableLayoutPanel13.Controls.Add(this.label20, 2, 0);
            this.tableLayoutPanel13.Controls.Add(this.label19, 3, 0);
            this.tableLayoutPanel13.Controls.Add(this.gridTXSelect5, 4, 1);
            this.tableLayoutPanel13.Controls.Add(this.label1, 4, 0);
            this.tableLayoutPanel13.Controls.Add(this.gridTXSelect4, 3, 1);
            this.tableLayoutPanel13.Controls.Add(this.gridTXSelect1, 0, 1);
            this.tableLayoutPanel13.Controls.Add(this.gridTXSelect3, 2, 1);
            this.tableLayoutPanel13.Controls.Add(this.gridTXSelect2, 1, 1);
            this.tableLayoutPanel13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel13.Location = new System.Drawing.Point(3, 81);
            this.tableLayoutPanel13.Name = "tableLayoutPanel13";
            this.tableLayoutPanel13.RowCount = 2;
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel13.Size = new System.Drawing.Size(443, 66);
            this.tableLayoutPanel13.TabIndex = 54;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("OCR A Extended", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Lime;
            this.label22.Location = new System.Drawing.Point(2, 0);
            this.label22.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(15, 15);
            this.label22.TabIndex = 37;
            this.label22.Text = "1";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("OCR A Extended", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Lime;
            this.label21.Location = new System.Drawing.Point(90, 0);
            this.label21.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(15, 15);
            this.label21.TabIndex = 38;
            this.label21.Text = "2";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("OCR A Extended", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Lime;
            this.label20.Location = new System.Drawing.Point(178, 0);
            this.label20.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(15, 15);
            this.label20.TabIndex = 39;
            this.label20.Text = "3";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("OCR A Extended", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Lime;
            this.label19.Location = new System.Drawing.Point(266, 0);
            this.label19.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(15, 15);
            this.label19.TabIndex = 40;
            this.label19.Text = "4";
            // 
            // gridTXSelect5
            // 
            this.gridTXSelect5.AutoSize = true;
            this.gridTXSelect5.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.gridTXSelect5.BackColor = System.Drawing.Color.Lime;
            this.gridTXSelect5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridTXSelect5.Font = new System.Drawing.Font("Elgethy Upper Bold", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridTXSelect5.ForeColor = System.Drawing.SystemColors.InfoText;
            this.gridTXSelect5.Location = new System.Drawing.Point(354, 17);
            this.gridTXSelect5.Margin = new System.Windows.Forms.Padding(2);
            this.gridTXSelect5.Name = "gridTXSelect5";
            this.gridTXSelect5.Size = new System.Drawing.Size(87, 47);
            this.gridTXSelect5.TabIndex = 35;
            this.gridTXSelect5.TabStop = false;
            this.gridTXSelect5.UseVisualStyleBackColor = false;
            this.gridTXSelect5.Click += new System.EventHandler(this.gridTXSelect5_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("OCR A Extended", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Lime;
            this.label1.Location = new System.Drawing.Point(354, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(15, 15);
            this.label1.TabIndex = 41;
            this.label1.Text = "5";
            // 
            // gridTXSelect4
            // 
            this.gridTXSelect4.AutoSize = true;
            this.gridTXSelect4.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.gridTXSelect4.BackColor = System.Drawing.Color.Lime;
            this.gridTXSelect4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridTXSelect4.Font = new System.Drawing.Font("Elgethy Upper Bold", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridTXSelect4.ForeColor = System.Drawing.SystemColors.InfoText;
            this.gridTXSelect4.Location = new System.Drawing.Point(266, 17);
            this.gridTXSelect4.Margin = new System.Windows.Forms.Padding(2);
            this.gridTXSelect4.Name = "gridTXSelect4";
            this.gridTXSelect4.Size = new System.Drawing.Size(84, 47);
            this.gridTXSelect4.TabIndex = 34;
            this.gridTXSelect4.TabStop = false;
            this.gridTXSelect4.UseVisualStyleBackColor = false;
            this.gridTXSelect4.Click += new System.EventHandler(this.gridTXSelect4_Click);
            // 
            // gridTXSelect1
            // 
            this.gridTXSelect1.AutoSize = true;
            this.gridTXSelect1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.gridTXSelect1.BackColor = System.Drawing.Color.Lime;
            this.gridTXSelect1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridTXSelect1.Font = new System.Drawing.Font("Elgethy Upper Bold", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridTXSelect1.ForeColor = System.Drawing.SystemColors.InfoText;
            this.gridTXSelect1.Location = new System.Drawing.Point(2, 17);
            this.gridTXSelect1.Margin = new System.Windows.Forms.Padding(2);
            this.gridTXSelect1.Name = "gridTXSelect1";
            this.gridTXSelect1.Size = new System.Drawing.Size(84, 47);
            this.gridTXSelect1.TabIndex = 31;
            this.gridTXSelect1.TabStop = false;
            this.gridTXSelect1.Text = " ";
            this.gridTXSelect1.UseVisualStyleBackColor = false;
            this.gridTXSelect1.Click += new System.EventHandler(this.gridTXSelect1_Click);
            // 
            // gridTXSelect3
            // 
            this.gridTXSelect3.AutoSize = true;
            this.gridTXSelect3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.gridTXSelect3.BackColor = System.Drawing.Color.Lime;
            this.gridTXSelect3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridTXSelect3.Font = new System.Drawing.Font("Elgethy Upper Bold", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridTXSelect3.ForeColor = System.Drawing.SystemColors.InfoText;
            this.gridTXSelect3.Location = new System.Drawing.Point(178, 17);
            this.gridTXSelect3.Margin = new System.Windows.Forms.Padding(2);
            this.gridTXSelect3.Name = "gridTXSelect3";
            this.gridTXSelect3.Size = new System.Drawing.Size(84, 47);
            this.gridTXSelect3.TabIndex = 33;
            this.gridTXSelect3.TabStop = false;
            this.gridTXSelect3.UseVisualStyleBackColor = false;
            this.gridTXSelect3.Click += new System.EventHandler(this.gridTXSelect3_Click);
            // 
            // gridTXSelect2
            // 
            this.gridTXSelect2.AutoSize = true;
            this.gridTXSelect2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.gridTXSelect2.BackColor = System.Drawing.Color.Lime;
            this.gridTXSelect2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridTXSelect2.Font = new System.Drawing.Font("Elgethy Upper Bold", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridTXSelect2.ForeColor = System.Drawing.SystemColors.InfoText;
            this.gridTXSelect2.Location = new System.Drawing.Point(90, 17);
            this.gridTXSelect2.Margin = new System.Windows.Forms.Padding(2);
            this.gridTXSelect2.Name = "gridTXSelect2";
            this.gridTXSelect2.Size = new System.Drawing.Size(84, 47);
            this.gridTXSelect2.TabIndex = 32;
            this.gridTXSelect2.TabStop = false;
            this.gridTXSelect2.UseVisualStyleBackColor = false;
            this.gridTXSelect2.Click += new System.EventHandler(this.gridTXSelect2_Click);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("OCR A Extended", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Lime;
            this.label25.Location = new System.Drawing.Point(2, 0);
            this.label25.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(154, 23);
            this.label25.TabIndex = 28;
            this.label25.Text = "GRID (ALT+M)";
            // 
            // tableLayoutPanel12
            // 
            this.tableLayoutPanel12.AutoSize = true;
            this.tableLayoutPanel12.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel12.ColumnCount = 2;
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel12.Controls.Add(this.label24, 0, 0);
            this.tableLayoutPanel12.Controls.Add(this.label23, 1, 0);
            this.tableLayoutPanel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel12.Location = new System.Drawing.Point(3, 60);
            this.tableLayoutPanel12.Name = "tableLayoutPanel12";
            this.tableLayoutPanel12.RowCount = 1;
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel12.Size = new System.Drawing.Size(443, 15);
            this.tableLayoutPanel12.TabIndex = 53;
            // 
            // label24
            // 
            this.label24.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("OCR A Extended", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Lime;
            this.label24.Location = new System.Drawing.Point(63, 0);
            this.label24.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(95, 15);
            this.label24.TabIndex = 30;
            this.label24.Text = "QuickSelect";
            // 
            // label23
            // 
            this.label23.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("OCR A Extended", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Lime;
            this.label23.Location = new System.Drawing.Point(308, 0);
            this.label23.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(47, 15);
            this.label23.TabIndex = 36;
            this.label23.Text = "ALT +";
            // 
            // gridTX
            // 
            this.gridTX.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridTX.BackColor = System.Drawing.Color.Lime;
            this.gridTX.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.gridTX.Font = new System.Drawing.Font("Elgethy Upper Bold", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridTX.Location = new System.Drawing.Point(2, 25);
            this.gridTX.Margin = new System.Windows.Forms.Padding(2);
            this.gridTX.Name = "gridTX";
            this.gridTX.Size = new System.Drawing.Size(445, 30);
            this.gridTX.TabIndex = 29;
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.ColumnCount = 3;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel10.Controls.Add(this.upperLeftTX, 0, 0);
            this.tableLayoutPanel10.Controls.Add(this.lowerRightTX, 2, 2);
            this.tableLayoutPanel10.Controls.Add(this.upperTX, 1, 0);
            this.tableLayoutPanel10.Controls.Add(this.lowerTX, 1, 2);
            this.tableLayoutPanel10.Controls.Add(this.upperRightTX, 2, 0);
            this.tableLayoutPanel10.Controls.Add(this.lowerLeftTX, 0, 2);
            this.tableLayoutPanel10.Controls.Add(this.leftTX, 0, 1);
            this.tableLayoutPanel10.Controls.Add(this.rightTX, 2, 1);
            this.tableLayoutPanel10.Controls.Add(this.centerTX, 1, 1);
            this.tableLayoutPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel10.Location = new System.Drawing.Point(458, 3);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 3;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel10.Size = new System.Drawing.Size(279, 150);
            this.tableLayoutPanel10.TabIndex = 51;
            // 
            // upperLeftTX
            // 
            this.upperLeftTX.AutoSize = true;
            this.upperLeftTX.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.upperLeftTX.BackColor = System.Drawing.Color.Lime;
            this.upperLeftTX.Dock = System.Windows.Forms.DockStyle.Fill;
            this.upperLeftTX.Font = new System.Drawing.Font("Elgethy Upper Bold", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.upperLeftTX.ForeColor = System.Drawing.Color.Black;
            this.upperLeftTX.Location = new System.Drawing.Point(3, 3);
            this.upperLeftTX.Name = "upperLeftTX";
            this.upperLeftTX.Size = new System.Drawing.Size(87, 44);
            this.upperLeftTX.TabIndex = 42;
            this.upperLeftTX.TabStop = false;
            this.upperLeftTX.Text = "EM04";
            this.upperLeftTX.UseVisualStyleBackColor = false;
            this.upperLeftTX.Click += new System.EventHandler(this.upperLeftTX_Click);
            // 
            // lowerRightTX
            // 
            this.lowerRightTX.AutoSize = true;
            this.lowerRightTX.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.lowerRightTX.BackColor = System.Drawing.Color.Lime;
            this.lowerRightTX.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lowerRightTX.Font = new System.Drawing.Font("Elgethy Upper Bold", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lowerRightTX.ForeColor = System.Drawing.Color.Black;
            this.lowerRightTX.Location = new System.Drawing.Point(189, 103);
            this.lowerRightTX.Name = "lowerRightTX";
            this.lowerRightTX.Size = new System.Drawing.Size(87, 44);
            this.lowerRightTX.TabIndex = 50;
            this.lowerRightTX.TabStop = false;
            this.lowerRightTX.Text = "EM22";
            this.lowerRightTX.UseVisualStyleBackColor = false;
            this.lowerRightTX.Click += new System.EventHandler(this.lowerRightTX_Click);
            // 
            // upperTX
            // 
            this.upperTX.AutoSize = true;
            this.upperTX.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.upperTX.BackColor = System.Drawing.Color.Lime;
            this.upperTX.Dock = System.Windows.Forms.DockStyle.Fill;
            this.upperTX.Font = new System.Drawing.Font("Elgethy Upper Bold", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.upperTX.ForeColor = System.Drawing.Color.Black;
            this.upperTX.Location = new System.Drawing.Point(96, 3);
            this.upperTX.Name = "upperTX";
            this.upperTX.Size = new System.Drawing.Size(87, 44);
            this.upperTX.TabIndex = 43;
            this.upperTX.TabStop = false;
            this.upperTX.Text = "EM14";
            this.upperTX.UseVisualStyleBackColor = false;
            this.upperTX.Click += new System.EventHandler(this.upperTX_Click);
            // 
            // lowerTX
            // 
            this.lowerTX.AutoSize = true;
            this.lowerTX.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.lowerTX.BackColor = System.Drawing.Color.Lime;
            this.lowerTX.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lowerTX.Font = new System.Drawing.Font("Elgethy Upper Bold", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lowerTX.ForeColor = System.Drawing.Color.Black;
            this.lowerTX.Location = new System.Drawing.Point(96, 103);
            this.lowerTX.Name = "lowerTX";
            this.lowerTX.Size = new System.Drawing.Size(87, 44);
            this.lowerTX.TabIndex = 49;
            this.lowerTX.TabStop = false;
            this.lowerTX.Text = "EM12";
            this.lowerTX.UseVisualStyleBackColor = false;
            this.lowerTX.Click += new System.EventHandler(this.lowerTX_Click);
            // 
            // upperRightTX
            // 
            this.upperRightTX.AutoSize = true;
            this.upperRightTX.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.upperRightTX.BackColor = System.Drawing.Color.Lime;
            this.upperRightTX.Dock = System.Windows.Forms.DockStyle.Fill;
            this.upperRightTX.Font = new System.Drawing.Font("Elgethy Upper Bold", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.upperRightTX.ForeColor = System.Drawing.Color.Black;
            this.upperRightTX.Location = new System.Drawing.Point(189, 3);
            this.upperRightTX.Name = "upperRightTX";
            this.upperRightTX.Size = new System.Drawing.Size(87, 44);
            this.upperRightTX.TabIndex = 44;
            this.upperRightTX.TabStop = false;
            this.upperRightTX.Text = "EM24";
            this.upperRightTX.UseVisualStyleBackColor = false;
            this.upperRightTX.Click += new System.EventHandler(this.upperRightTX_Click);
            // 
            // lowerLeftTX
            // 
            this.lowerLeftTX.AutoSize = true;
            this.lowerLeftTX.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.lowerLeftTX.BackColor = System.Drawing.Color.Lime;
            this.lowerLeftTX.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lowerLeftTX.Font = new System.Drawing.Font("Elgethy Upper Bold", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lowerLeftTX.ForeColor = System.Drawing.Color.Black;
            this.lowerLeftTX.Location = new System.Drawing.Point(3, 103);
            this.lowerLeftTX.Name = "lowerLeftTX";
            this.lowerLeftTX.Size = new System.Drawing.Size(87, 44);
            this.lowerLeftTX.TabIndex = 48;
            this.lowerLeftTX.TabStop = false;
            this.lowerLeftTX.Text = "EM02";
            this.lowerLeftTX.UseVisualStyleBackColor = false;
            this.lowerLeftTX.Click += new System.EventHandler(this.lowerLeftTX_Click);
            // 
            // leftTX
            // 
            this.leftTX.AutoSize = true;
            this.leftTX.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.leftTX.BackColor = System.Drawing.Color.Lime;
            this.leftTX.Dock = System.Windows.Forms.DockStyle.Fill;
            this.leftTX.Font = new System.Drawing.Font("Elgethy Upper Bold", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.leftTX.ForeColor = System.Drawing.Color.Black;
            this.leftTX.Location = new System.Drawing.Point(3, 53);
            this.leftTX.Name = "leftTX";
            this.leftTX.Size = new System.Drawing.Size(87, 44);
            this.leftTX.TabIndex = 45;
            this.leftTX.TabStop = false;
            this.leftTX.Text = "EM03";
            this.leftTX.UseVisualStyleBackColor = false;
            this.leftTX.Click += new System.EventHandler(this.leftTX_Click);
            // 
            // rightTX
            // 
            this.rightTX.AutoSize = true;
            this.rightTX.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.rightTX.BackColor = System.Drawing.Color.Lime;
            this.rightTX.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rightTX.Font = new System.Drawing.Font("Elgethy Upper Bold", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rightTX.ForeColor = System.Drawing.Color.Black;
            this.rightTX.Location = new System.Drawing.Point(189, 53);
            this.rightTX.Name = "rightTX";
            this.rightTX.Size = new System.Drawing.Size(87, 44);
            this.rightTX.TabIndex = 47;
            this.rightTX.TabStop = false;
            this.rightTX.Text = "EM23";
            this.rightTX.UseVisualStyleBackColor = false;
            this.rightTX.Click += new System.EventHandler(this.rightTX_Click);
            // 
            // centerTX
            // 
            this.centerTX.AutoSize = true;
            this.centerTX.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.centerTX.BackColor = System.Drawing.Color.Lime;
            this.centerTX.Dock = System.Windows.Forms.DockStyle.Fill;
            this.centerTX.Font = new System.Drawing.Font("Elgethy Upper Bold", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.centerTX.ForeColor = System.Drawing.Color.Black;
            this.centerTX.Location = new System.Drawing.Point(96, 53);
            this.centerTX.Name = "centerTX";
            this.centerTX.Size = new System.Drawing.Size(87, 44);
            this.centerTX.TabIndex = 46;
            this.centerTX.TabStop = false;
            this.centerTX.Text = "EM13";
            this.centerTX.UseVisualStyleBackColor = false;
            this.centerTX.Click += new System.EventHandler(this.centerTX_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.AutoSize = true;
            this.groupBox1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupBox1.Controls.Add(this.tableLayoutPanel2);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Font = new System.Drawing.Font("Elgethy Upper Bold", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.SystemColors.Window;
            this.groupBox1.Location = new System.Drawing.Point(2, 2);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(744, 343);
            this.groupBox1.TabIndex = 59;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "RX";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.AutoSize = true;
            this.tableLayoutPanel2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 61.14899F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 38.85101F));
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel3, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel1, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(2, 25);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(740, 316);
            this.tableLayoutPanel2.TabIndex = 61;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.AutoSize = true;
            this.tableLayoutPanel3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel4, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel6, 0, 1);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(446, 310);
            this.tableLayoutPanel3.TabIndex = 0;
            this.tableLayoutPanel3.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel3_Paint);
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel4.ColumnCount = 1;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel9, 0, 2);
            this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel5, 0, 3);
            this.tableLayoutPanel4.Controls.Add(this.label3, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.gridRX, 0, 1);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 4;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(440, 149);
            this.tableLayoutPanel4.TabIndex = 62;
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.AutoSize = true;
            this.tableLayoutPanel9.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel9.ColumnCount = 2;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.Controls.Add(this.label18, 0, 0);
            this.tableLayoutPanel9.Controls.Add(this.label17, 1, 0);
            this.tableLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel9.Location = new System.Drawing.Point(3, 60);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 1;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(434, 15);
            this.tableLayoutPanel9.TabIndex = 67;
            // 
            // label18
            // 
            this.label18.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("OCR A Extended", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Yellow;
            this.label18.Location = new System.Drawing.Point(61, 0);
            this.label18.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(95, 15);
            this.label18.TabIndex = 16;
            this.label18.Text = "QuickSelect";
            // 
            // label17
            // 
            this.label17.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("OCR A Extended", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Yellow;
            this.label17.Location = new System.Drawing.Point(298, 0);
            this.label17.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(55, 15);
            this.label17.TabIndex = 22;
            this.label17.Text = "CTRL +";
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.AutoSize = true;
            this.tableLayoutPanel5.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel5.ColumnCount = 5;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel5.Controls.Add(this.label16, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.label15, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.label14, 2, 0);
            this.tableLayoutPanel5.Controls.Add(this.label13, 3, 0);
            this.tableLayoutPanel5.Controls.Add(this.label12, 4, 0);
            this.tableLayoutPanel5.Controls.Add(this.gridRXSelect5, 4, 1);
            this.tableLayoutPanel5.Controls.Add(this.gridRXSelect1, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.gridRXSelect4, 3, 1);
            this.tableLayoutPanel5.Controls.Add(this.gridRXSelect2, 1, 1);
            this.tableLayoutPanel5.Controls.Add(this.gridRXSelect3, 2, 1);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(0, 78);
            this.tableLayoutPanel5.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 2;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel5.Size = new System.Drawing.Size(440, 71);
            this.tableLayoutPanel5.TabIndex = 63;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("OCR A Extended", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Yellow;
            this.label16.Location = new System.Drawing.Point(2, 0);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(15, 15);
            this.label16.TabIndex = 23;
            this.label16.Text = "1";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("OCR A Extended", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Yellow;
            this.label15.Location = new System.Drawing.Point(90, 0);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(15, 15);
            this.label15.TabIndex = 24;
            this.label15.Text = "2";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("OCR A Extended", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Yellow;
            this.label14.Location = new System.Drawing.Point(178, 0);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(15, 15);
            this.label14.TabIndex = 25;
            this.label14.Text = "3";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("OCR A Extended", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Yellow;
            this.label13.Location = new System.Drawing.Point(266, 0);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(15, 15);
            this.label13.TabIndex = 26;
            this.label13.Text = "4";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("OCR A Extended", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Yellow;
            this.label12.Location = new System.Drawing.Point(354, 0);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(15, 15);
            this.label12.TabIndex = 27;
            this.label12.Text = "5";
            // 
            // gridRXSelect5
            // 
            this.gridRXSelect5.AutoSize = true;
            this.gridRXSelect5.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.gridRXSelect5.BackColor = System.Drawing.Color.Yellow;
            this.gridRXSelect5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridRXSelect5.Font = new System.Drawing.Font("Elgethy Upper Bold", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridRXSelect5.ForeColor = System.Drawing.SystemColors.InfoText;
            this.gridRXSelect5.Location = new System.Drawing.Point(354, 17);
            this.gridRXSelect5.Margin = new System.Windows.Forms.Padding(2);
            this.gridRXSelect5.Name = "gridRXSelect5";
            this.gridRXSelect5.Size = new System.Drawing.Size(84, 52);
            this.gridRXSelect5.TabIndex = 21;
            this.gridRXSelect5.TabStop = false;
            this.gridRXSelect5.UseVisualStyleBackColor = false;
            this.gridRXSelect5.Click += new System.EventHandler(this.gridRXSelect5_Click);
            // 
            // gridRXSelect1
            // 
            this.gridRXSelect1.AutoSize = true;
            this.gridRXSelect1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.gridRXSelect1.BackColor = System.Drawing.Color.Yellow;
            this.gridRXSelect1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridRXSelect1.Font = new System.Drawing.Font("Elgethy Upper Bold", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridRXSelect1.ForeColor = System.Drawing.SystemColors.InfoText;
            this.gridRXSelect1.Location = new System.Drawing.Point(2, 17);
            this.gridRXSelect1.Margin = new System.Windows.Forms.Padding(2);
            this.gridRXSelect1.MinimumSize = new System.Drawing.Size(82, 36);
            this.gridRXSelect1.Name = "gridRXSelect1";
            this.gridRXSelect1.Size = new System.Drawing.Size(84, 52);
            this.gridRXSelect1.TabIndex = 17;
            this.gridRXSelect1.TabStop = false;
            this.gridRXSelect1.Text = " ";
            this.gridRXSelect1.UseVisualStyleBackColor = false;
            this.gridRXSelect1.Click += new System.EventHandler(this.gridRXSelect1_Click);
            // 
            // gridRXSelect4
            // 
            this.gridRXSelect4.AutoSize = true;
            this.gridRXSelect4.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.gridRXSelect4.BackColor = System.Drawing.Color.Yellow;
            this.gridRXSelect4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridRXSelect4.Font = new System.Drawing.Font("Elgethy Upper Bold", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridRXSelect4.ForeColor = System.Drawing.SystemColors.InfoText;
            this.gridRXSelect4.Location = new System.Drawing.Point(266, 17);
            this.gridRXSelect4.Margin = new System.Windows.Forms.Padding(2);
            this.gridRXSelect4.Name = "gridRXSelect4";
            this.gridRXSelect4.Size = new System.Drawing.Size(84, 52);
            this.gridRXSelect4.TabIndex = 20;
            this.gridRXSelect4.TabStop = false;
            this.gridRXSelect4.UseVisualStyleBackColor = false;
            this.gridRXSelect4.Click += new System.EventHandler(this.gridRXSelect4_Click);
            // 
            // gridRXSelect2
            // 
            this.gridRXSelect2.AutoSize = true;
            this.gridRXSelect2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.gridRXSelect2.BackColor = System.Drawing.Color.Yellow;
            this.gridRXSelect2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridRXSelect2.Font = new System.Drawing.Font("Elgethy Upper Bold", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridRXSelect2.ForeColor = System.Drawing.SystemColors.InfoText;
            this.gridRXSelect2.Location = new System.Drawing.Point(90, 17);
            this.gridRXSelect2.Margin = new System.Windows.Forms.Padding(2);
            this.gridRXSelect2.Name = "gridRXSelect2";
            this.gridRXSelect2.Size = new System.Drawing.Size(84, 52);
            this.gridRXSelect2.TabIndex = 18;
            this.gridRXSelect2.TabStop = false;
            this.gridRXSelect2.UseVisualStyleBackColor = false;
            this.gridRXSelect2.Click += new System.EventHandler(this.gridRXSelect2_Click);
            // 
            // gridRXSelect3
            // 
            this.gridRXSelect3.AutoSize = true;
            this.gridRXSelect3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.gridRXSelect3.BackColor = System.Drawing.Color.Yellow;
            this.gridRXSelect3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridRXSelect3.Font = new System.Drawing.Font("Elgethy Upper Bold", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridRXSelect3.ForeColor = System.Drawing.SystemColors.InfoText;
            this.gridRXSelect3.Location = new System.Drawing.Point(178, 17);
            this.gridRXSelect3.Margin = new System.Windows.Forms.Padding(2);
            this.gridRXSelect3.Name = "gridRXSelect3";
            this.gridRXSelect3.Size = new System.Drawing.Size(84, 52);
            this.gridRXSelect3.TabIndex = 19;
            this.gridRXSelect3.TabStop = false;
            this.gridRXSelect3.UseVisualStyleBackColor = false;
            this.gridRXSelect3.Click += new System.EventHandler(this.gridRXSelect3_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("OCR A Extended", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Yellow;
            this.label3.Location = new System.Drawing.Point(2, 0);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(166, 23);
            this.label3.TabIndex = 0;
            this.label3.Text = "GRID (CTRL+O)";
            // 
            // gridRX
            // 
            this.gridRX.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.gridRX.BackColor = System.Drawing.Color.Yellow;
            this.gridRX.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.gridRX.Font = new System.Drawing.Font("Elgethy Upper Bold", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridRX.Location = new System.Drawing.Point(2, 25);
            this.gridRX.Margin = new System.Windows.Forms.Padding(2);
            this.gridRX.Name = "gridRX";
            this.gridRX.Size = new System.Drawing.Size(436, 30);
            this.gridRX.TabIndex = 1;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel6.ColumnCount = 1;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.Controls.Add(this.tableLayoutPanel8, 0, 2);
            this.tableLayoutPanel6.Controls.Add(this.tableLayoutPanel7, 0, 3);
            this.tableLayoutPanel6.Controls.Add(this.label11, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.callRX, 0, 1);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(3, 158);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 4;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(440, 149);
            this.tableLayoutPanel6.TabIndex = 62;
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 2;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.Controls.Add(this.label4, 0, 0);
            this.tableLayoutPanel8.Controls.Add(this.label5, 1, 0);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(3, 63);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 1;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(434, 14);
            this.tableLayoutPanel8.TabIndex = 51;
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("OCR A Extended", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label4.Location = new System.Drawing.Point(61, 0);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(95, 14);
            this.label4.TabIndex = 2;
            this.label4.Text = "QuickSelect";
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("OCR A Extended", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label5.Location = new System.Drawing.Point(298, 0);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(55, 14);
            this.label5.TabIndex = 8;
            this.label5.Text = "CTRL +";
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.AutoSize = true;
            this.tableLayoutPanel7.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel7.ColumnCount = 5;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel7.Controls.Add(this.label6, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.label7, 1, 0);
            this.tableLayoutPanel7.Controls.Add(this.label8, 2, 0);
            this.tableLayoutPanel7.Controls.Add(this.label9, 3, 0);
            this.tableLayoutPanel7.Controls.Add(this.callSelect5, 4, 1);
            this.tableLayoutPanel7.Controls.Add(this.label10, 4, 0);
            this.tableLayoutPanel7.Controls.Add(this.callSelect4, 3, 1);
            this.tableLayoutPanel7.Controls.Add(this.callSelect1, 0, 1);
            this.tableLayoutPanel7.Controls.Add(this.callSelect3, 2, 1);
            this.tableLayoutPanel7.Controls.Add(this.callSelect2, 1, 1);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(3, 83);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 2;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(434, 63);
            this.tableLayoutPanel7.TabIndex = 63;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("OCR A Extended", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label6.Location = new System.Drawing.Point(2, 0);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(15, 15);
            this.label6.TabIndex = 9;
            this.label6.Text = "6";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("OCR A Extended", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label7.Location = new System.Drawing.Point(88, 0);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(15, 15);
            this.label7.TabIndex = 10;
            this.label7.Text = "7";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("OCR A Extended", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label8.Location = new System.Drawing.Point(174, 0);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(15, 15);
            this.label8.TabIndex = 11;
            this.label8.Text = "8";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("OCR A Extended", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label9.Location = new System.Drawing.Point(260, 0);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(15, 15);
            this.label9.TabIndex = 12;
            this.label9.Text = "9";
            // 
            // callSelect5
            // 
            this.callSelect5.AutoSize = true;
            this.callSelect5.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.callSelect5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.callSelect5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.callSelect5.Font = new System.Drawing.Font("OCR A Extended", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.callSelect5.ForeColor = System.Drawing.SystemColors.InfoText;
            this.callSelect5.Location = new System.Drawing.Point(346, 17);
            this.callSelect5.Margin = new System.Windows.Forms.Padding(2);
            this.callSelect5.Name = "callSelect5";
            this.callSelect5.Size = new System.Drawing.Size(86, 44);
            this.callSelect5.TabIndex = 7;
            this.callSelect5.TabStop = false;
            this.callSelect5.UseVisualStyleBackColor = false;
            this.callSelect5.Click += new System.EventHandler(this.callSelect5_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("OCR A Extended", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label10.Location = new System.Drawing.Point(346, 0);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(15, 15);
            this.label10.TabIndex = 13;
            this.label10.Text = "0";
            // 
            // callSelect4
            // 
            this.callSelect4.AutoSize = true;
            this.callSelect4.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.callSelect4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.callSelect4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.callSelect4.Font = new System.Drawing.Font("OCR A Extended", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.callSelect4.ForeColor = System.Drawing.SystemColors.InfoText;
            this.callSelect4.Location = new System.Drawing.Point(260, 17);
            this.callSelect4.Margin = new System.Windows.Forms.Padding(2);
            this.callSelect4.Name = "callSelect4";
            this.callSelect4.Size = new System.Drawing.Size(82, 44);
            this.callSelect4.TabIndex = 6;
            this.callSelect4.TabStop = false;
            this.callSelect4.UseVisualStyleBackColor = false;
            this.callSelect4.Click += new System.EventHandler(this.callSelect4_Click);
            // 
            // callSelect1
            // 
            this.callSelect1.AutoSize = true;
            this.callSelect1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.callSelect1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.callSelect1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.callSelect1.Font = new System.Drawing.Font("OCR A Extended", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.callSelect1.ForeColor = System.Drawing.SystemColors.InfoText;
            this.callSelect1.Location = new System.Drawing.Point(2, 17);
            this.callSelect1.Margin = new System.Windows.Forms.Padding(2);
            this.callSelect1.Name = "callSelect1";
            this.callSelect1.Size = new System.Drawing.Size(82, 44);
            this.callSelect1.TabIndex = 3;
            this.callSelect1.TabStop = false;
            this.callSelect1.Text = " ";
            this.callSelect1.UseVisualStyleBackColor = false;
            this.callSelect1.Click += new System.EventHandler(this.callSelect1_Click);
            // 
            // callSelect3
            // 
            this.callSelect3.AutoSize = true;
            this.callSelect3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.callSelect3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.callSelect3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.callSelect3.Font = new System.Drawing.Font("OCR A Extended", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.callSelect3.ForeColor = System.Drawing.SystemColors.InfoText;
            this.callSelect3.Location = new System.Drawing.Point(174, 17);
            this.callSelect3.Margin = new System.Windows.Forms.Padding(2);
            this.callSelect3.Name = "callSelect3";
            this.callSelect3.Size = new System.Drawing.Size(82, 44);
            this.callSelect3.TabIndex = 5;
            this.callSelect3.TabStop = false;
            this.callSelect3.UseVisualStyleBackColor = false;
            this.callSelect3.Click += new System.EventHandler(this.callSelect3_Click);
            // 
            // callSelect2
            // 
            this.callSelect2.AutoSize = true;
            this.callSelect2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.callSelect2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.callSelect2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.callSelect2.Font = new System.Drawing.Font("OCR A Extended", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.callSelect2.ForeColor = System.Drawing.SystemColors.InfoText;
            this.callSelect2.Location = new System.Drawing.Point(88, 17);
            this.callSelect2.Margin = new System.Windows.Forms.Padding(2);
            this.callSelect2.Name = "callSelect2";
            this.callSelect2.Size = new System.Drawing.Size(82, 44);
            this.callSelect2.TabIndex = 4;
            this.callSelect2.TabStop = false;
            this.callSelect2.UseVisualStyleBackColor = false;
            this.callSelect2.Click += new System.EventHandler(this.callSelect2_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("OCR A Extended", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label11.Location = new System.Drawing.Point(2, 0);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(166, 23);
            this.label11.TabIndex = 14;
            this.label11.Text = "CALL (CTRL+C)";
            // 
            // callRX
            // 
            this.callRX.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.callRX.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.callRX.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.callRX.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.callRX.Location = new System.Drawing.Point(2, 25);
            this.callRX.Margin = new System.Windows.Forms.Padding(2);
            this.callRX.Name = "callRX";
            this.callRX.Size = new System.Drawing.Size(436, 33);
            this.callRX.TabIndex = 15;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.Controls.Add(this.upperLeftRX, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.lowerRightRX, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.upperRX, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.lowerRX, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.upperRightRX, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.lowerLeftRX, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.leftRX, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.centerRX, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.rightRX, 2, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(455, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(282, 310);
            this.tableLayoutPanel1.TabIndex = 60;
            // 
            // upperLeftRX
            // 
            this.upperLeftRX.AutoSize = true;
            this.upperLeftRX.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.upperLeftRX.BackColor = System.Drawing.Color.Yellow;
            this.upperLeftRX.Dock = System.Windows.Forms.DockStyle.Fill;
            this.upperLeftRX.Font = new System.Drawing.Font("Elgethy Upper Bold", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.upperLeftRX.ForeColor = System.Drawing.Color.Black;
            this.upperLeftRX.Location = new System.Drawing.Point(3, 3);
            this.upperLeftRX.Name = "upperLeftRX";
            this.upperLeftRX.Size = new System.Drawing.Size(88, 97);
            this.upperLeftRX.TabIndex = 51;
            this.upperLeftRX.TabStop = false;
            this.upperLeftRX.Text = "EM04";
            this.upperLeftRX.UseVisualStyleBackColor = false;
            this.upperLeftRX.Click += new System.EventHandler(this.upperLeftRX_Click);
            // 
            // lowerRightRX
            // 
            this.lowerRightRX.AutoSize = true;
            this.lowerRightRX.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.lowerRightRX.BackColor = System.Drawing.Color.Yellow;
            this.lowerRightRX.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lowerRightRX.Font = new System.Drawing.Font("Elgethy Upper Bold", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lowerRightRX.ForeColor = System.Drawing.Color.Black;
            this.lowerRightRX.Location = new System.Drawing.Point(191, 209);
            this.lowerRightRX.Name = "lowerRightRX";
            this.lowerRightRX.Size = new System.Drawing.Size(88, 98);
            this.lowerRightRX.TabIndex = 59;
            this.lowerRightRX.TabStop = false;
            this.lowerRightRX.Text = "EM22";
            this.lowerRightRX.UseVisualStyleBackColor = false;
            this.lowerRightRX.Click += new System.EventHandler(this.lowerRightRX_Click);
            // 
            // upperRX
            // 
            this.upperRX.AutoSize = true;
            this.upperRX.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.upperRX.BackColor = System.Drawing.Color.Yellow;
            this.upperRX.Dock = System.Windows.Forms.DockStyle.Fill;
            this.upperRX.Font = new System.Drawing.Font("Elgethy Upper Bold", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.upperRX.ForeColor = System.Drawing.Color.Black;
            this.upperRX.Location = new System.Drawing.Point(97, 3);
            this.upperRX.Name = "upperRX";
            this.upperRX.Size = new System.Drawing.Size(88, 97);
            this.upperRX.TabIndex = 52;
            this.upperRX.TabStop = false;
            this.upperRX.Text = "EM14";
            this.upperRX.UseVisualStyleBackColor = false;
            this.upperRX.Click += new System.EventHandler(this.upperRX_Click);
            // 
            // lowerRX
            // 
            this.lowerRX.AutoSize = true;
            this.lowerRX.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.lowerRX.BackColor = System.Drawing.Color.Yellow;
            this.lowerRX.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lowerRX.Font = new System.Drawing.Font("Elgethy Upper Bold", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lowerRX.ForeColor = System.Drawing.Color.Black;
            this.lowerRX.Location = new System.Drawing.Point(97, 209);
            this.lowerRX.Name = "lowerRX";
            this.lowerRX.Size = new System.Drawing.Size(88, 98);
            this.lowerRX.TabIndex = 58;
            this.lowerRX.TabStop = false;
            this.lowerRX.Text = "EM12";
            this.lowerRX.UseVisualStyleBackColor = false;
            this.lowerRX.Click += new System.EventHandler(this.lowerRX_Click);
            // 
            // upperRightRX
            // 
            this.upperRightRX.AutoSize = true;
            this.upperRightRX.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.upperRightRX.BackColor = System.Drawing.Color.Yellow;
            this.upperRightRX.Dock = System.Windows.Forms.DockStyle.Fill;
            this.upperRightRX.Font = new System.Drawing.Font("Elgethy Upper Bold", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.upperRightRX.ForeColor = System.Drawing.Color.Black;
            this.upperRightRX.Location = new System.Drawing.Point(191, 3);
            this.upperRightRX.Name = "upperRightRX";
            this.upperRightRX.Size = new System.Drawing.Size(88, 97);
            this.upperRightRX.TabIndex = 53;
            this.upperRightRX.TabStop = false;
            this.upperRightRX.Text = "EM24";
            this.upperRightRX.UseVisualStyleBackColor = false;
            this.upperRightRX.Click += new System.EventHandler(this.upperRightRX_Click);
            // 
            // lowerLeftRX
            // 
            this.lowerLeftRX.AutoSize = true;
            this.lowerLeftRX.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.lowerLeftRX.BackColor = System.Drawing.Color.Yellow;
            this.lowerLeftRX.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lowerLeftRX.Font = new System.Drawing.Font("Elgethy Upper Bold", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lowerLeftRX.ForeColor = System.Drawing.Color.Black;
            this.lowerLeftRX.Location = new System.Drawing.Point(3, 209);
            this.lowerLeftRX.Name = "lowerLeftRX";
            this.lowerLeftRX.Size = new System.Drawing.Size(88, 98);
            this.lowerLeftRX.TabIndex = 57;
            this.lowerLeftRX.TabStop = false;
            this.lowerLeftRX.Text = "EM02";
            this.lowerLeftRX.UseVisualStyleBackColor = false;
            this.lowerLeftRX.Click += new System.EventHandler(this.lowerLeftRX_Click);
            // 
            // leftRX
            // 
            this.leftRX.AutoSize = true;
            this.leftRX.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.leftRX.BackColor = System.Drawing.Color.Yellow;
            this.leftRX.Dock = System.Windows.Forms.DockStyle.Fill;
            this.leftRX.Font = new System.Drawing.Font("Elgethy Upper Bold", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.leftRX.ForeColor = System.Drawing.Color.Black;
            this.leftRX.Location = new System.Drawing.Point(3, 106);
            this.leftRX.Name = "leftRX";
            this.leftRX.Size = new System.Drawing.Size(88, 97);
            this.leftRX.TabIndex = 54;
            this.leftRX.TabStop = false;
            this.leftRX.Text = "EM03";
            this.leftRX.UseVisualStyleBackColor = false;
            this.leftRX.Click += new System.EventHandler(this.leftRX_Click);
            // 
            // centerRX
            // 
            this.centerRX.AutoSize = true;
            this.centerRX.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.centerRX.BackColor = System.Drawing.Color.Yellow;
            this.centerRX.Dock = System.Windows.Forms.DockStyle.Fill;
            this.centerRX.Font = new System.Drawing.Font("Elgethy Upper Bold", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.centerRX.ForeColor = System.Drawing.Color.Black;
            this.centerRX.Location = new System.Drawing.Point(97, 106);
            this.centerRX.Name = "centerRX";
            this.centerRX.Size = new System.Drawing.Size(88, 97);
            this.centerRX.TabIndex = 55;
            this.centerRX.TabStop = false;
            this.centerRX.Text = "EM13";
            this.centerRX.UseVisualStyleBackColor = false;
            this.centerRX.Click += new System.EventHandler(this.centerRX_Click);
            // 
            // rightRX
            // 
            this.rightRX.AutoSize = true;
            this.rightRX.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.rightRX.BackColor = System.Drawing.Color.Yellow;
            this.rightRX.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rightRX.Font = new System.Drawing.Font("Elgethy Upper Bold", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rightRX.ForeColor = System.Drawing.Color.Black;
            this.rightRX.Location = new System.Drawing.Point(191, 106);
            this.rightRX.Name = "rightRX";
            this.rightRX.Size = new System.Drawing.Size(88, 97);
            this.rightRX.TabIndex = 56;
            this.rightRX.TabStop = false;
            this.rightRX.Text = "EM23";
            this.rightRX.UseVisualStyleBackColor = false;
            this.rightRX.Click += new System.EventHandler(this.rightRX_Click);
            // 
            // tabPage5
            // 
            this.tabPage5.BackColor = System.Drawing.Color.Black;
            this.tabPage5.Controls.Add(this.errorBox);
            this.tabPage5.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.tabPage5.Location = new System.Drawing.Point(4, 33);
            this.tabPage5.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(2);
            this.tabPage5.Size = new System.Drawing.Size(1020, 663);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "MESSAGE LOG";
            // 
            // errorBox
            // 
            this.errorBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.errorBox.Font = new System.Drawing.Font("OCR A Extended", 13.875F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.errorBox.FormattingEnabled = true;
            this.errorBox.ItemHeight = 19;
            this.errorBox.Location = new System.Drawing.Point(2, 2);
            this.errorBox.Margin = new System.Windows.Forms.Padding(2);
            this.errorBox.Name = "errorBox";
            this.errorBox.Size = new System.Drawing.Size(1016, 659);
            this.errorBox.TabIndex = 0;
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.Black;
            this.tabPage3.Controls.Add(this.pictureBox1);
            this.tabPage3.Location = new System.Drawing.Point(4, 33);
            this.tabPage3.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(2);
            this.tabPage3.Size = new System.Drawing.Size(1020, 663);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "VISUAL";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Location = new System.Drawing.Point(2, 2);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1016, 659);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // tabPage4
            // 
            this.tabPage4.BackColor = System.Drawing.Color.Black;
            this.tabPage4.Controls.Add(this.groupBox10);
            this.tabPage4.Controls.Add(this.groupBox9);
            this.tabPage4.Location = new System.Drawing.Point(4, 33);
            this.tabPage4.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(2);
            this.tabPage4.Size = new System.Drawing.Size(1020, 663);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "POST";
            // 
            // groupBox10
            // 
            this.groupBox10.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox10.Location = new System.Drawing.Point(5, 215);
            this.groupBox10.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox10.Size = new System.Drawing.Size(758, 288);
            this.groupBox10.TabIndex = 2;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "INFO";
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.tableLayoutPanel23);
            this.groupBox9.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox9.Location = new System.Drawing.Point(4, 4);
            this.groupBox9.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox9.Size = new System.Drawing.Size(1027, 209);
            this.groupBox9.TabIndex = 1;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "DUPLICATES";
            // 
            // tableLayoutPanel23
            // 
            this.tableLayoutPanel23.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel23.ColumnCount = 2;
            this.tableLayoutPanel23.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel23.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel23.Controls.Add(this.dupeCSVButton, 0, 0);
            this.tableLayoutPanel23.Controls.Add(this.button4, 0, 1);
            this.tableLayoutPanel23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel23.Location = new System.Drawing.Point(2, 25);
            this.tableLayoutPanel23.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanel23.Name = "tableLayoutPanel23";
            this.tableLayoutPanel23.RowCount = 2;
            this.tableLayoutPanel23.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel23.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel23.Size = new System.Drawing.Size(1023, 182);
            this.tableLayoutPanel23.TabIndex = 0;
            // 
            // dupeCSVButton
            // 
            this.dupeCSVButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.dupeCSVButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dupeCSVButton.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.dupeCSVButton.Location = new System.Drawing.Point(2, 2);
            this.dupeCSVButton.Margin = new System.Windows.Forms.Padding(2);
            this.dupeCSVButton.Name = "dupeCSVButton";
            this.dupeCSVButton.Size = new System.Drawing.Size(507, 87);
            this.dupeCSVButton.TabIndex = 0;
            this.dupeCSVButton.Text = "OUTPUT DUPLICATES TO CSV";
            this.dupeCSVButton.UseVisualStyleBackColor = true;
            this.dupeCSVButton.Click += new System.EventHandler(this.dupeCSVButton_Click);
            // 
            // button4
            // 
            this.button4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button4.Enabled = false;
            this.button4.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button4.Location = new System.Drawing.Point(2, 93);
            this.button4.Margin = new System.Windows.Forms.Padding(2);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(507, 87);
            this.button4.TabIndex = 1;
            this.button4.Text = "VIEW/REMOVE DUPLICATES";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // tabPage6
            // 
            this.tabPage6.Location = new System.Drawing.Point(4, 33);
            this.tabPage6.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(2);
            this.tabPage6.Size = new System.Drawing.Size(1020, 663);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "LIMITED DISTRIBUTION";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // timer2
            // 
            this.timer2.Interval = 10000;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(1028, 749);
            this.Controls.Add(this.fakeTab1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.KeyPreview = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Form1";
            this.Text = "Rover Contest Logger";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.fakeTab1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tableLayoutPanel19.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.updateSecs)).EndInit();
            this.groupBox7.ResumeLayout(false);
            this.tableLayoutPanel21.ResumeLayout(false);
            this.tableLayoutPanel21.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.tableLayoutPanel22.ResumeLayout(false);
            this.tableLayoutPanel22.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tableLayoutPanel20.ResumeLayout(false);
            this.tableLayoutPanel20.PerformLayout();
            this.tableLayoutPanel18.ResumeLayout(false);
            this.tableLayoutPanel17.ResumeLayout(false);
            this.tableLayoutPanel17.PerformLayout();
            this.tableLayoutPanel15.ResumeLayout(false);
            this.tableLayoutPanel15.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.tableLayoutPanel16.ResumeLayout(false);
            this.tableLayoutPanel16.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.tableLayoutPanel14.ResumeLayout(false);
            this.tableLayoutPanel14.PerformLayout();
            this.tableLayoutPanel11.ResumeLayout(false);
            this.tableLayoutPanel11.PerformLayout();
            this.tableLayoutPanel13.ResumeLayout(false);
            this.tableLayoutPanel13.PerformLayout();
            this.tableLayoutPanel12.ResumeLayout(false);
            this.tableLayoutPanel12.PerformLayout();
            this.tableLayoutPanel10.ResumeLayout(false);
            this.tableLayoutPanel10.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.tableLayoutPanel9.ResumeLayout(false);
            this.tableLayoutPanel9.PerformLayout();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.tableLayoutPanel8.ResumeLayout(false);
            this.tableLayoutPanel8.PerformLayout();
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel7.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            this.tableLayoutPanel23.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel statusLabel;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripStatusLabel scoreLabel;
        private System.Windows.Forms.ToolStripStatusLabel pointsLabel;
        private System.Windows.Forms.ToolStripStatusLabel multiplierLabel;
        private System.Windows.Forms.ToolStripStatusLabel qsoLabel;
        private System.Windows.Forms.ToolStripStatusLabel rateLabel;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cabrilloToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aDIFToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cabrilloToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem aDIFToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quickReferenceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem oKROVERToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tabletModeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nightModeToolStripMenuItem;
        private System.Windows.Forms.TabControl fakeTab1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button lowerRightRX;
        private System.Windows.Forms.Button lowerRX;
        private System.Windows.Forms.Button lowerLeftRX;
        private System.Windows.Forms.Button rightRX;
        private System.Windows.Forms.Button centerRX;
        private System.Windows.Forms.Button leftRX;
        private System.Windows.Forms.Button upperRightRX;
        private System.Windows.Forms.Button upperRX;
        private System.Windows.Forms.Button upperLeftRX;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button gridRXSelect5;
        private System.Windows.Forms.Button gridRXSelect4;
        private System.Windows.Forms.Button gridRXSelect3;
        private System.Windows.Forms.Button gridRXSelect2;
        private System.Windows.Forms.Button gridRXSelect1;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox callRX;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button callSelect5;
        private System.Windows.Forms.Button callSelect4;
        private System.Windows.Forms.Button callSelect3;
        private System.Windows.Forms.Button callSelect2;
        private System.Windows.Forms.Button callSelect1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox gridRX;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListBox logBox;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button lowerRightTX;
        private System.Windows.Forms.Button lowerTX;
        private System.Windows.Forms.Button lowerLeftTX;
        private System.Windows.Forms.Button rightTX;
        private System.Windows.Forms.Button centerTX;
        private System.Windows.Forms.Button leftTX;
        private System.Windows.Forms.Button upperRightTX;
        private System.Windows.Forms.Button upperTX;
        private System.Windows.Forms.Button upperLeftTX;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox gridTX;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Button clearButton;
        private System.Windows.Forms.Button submitButton;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton radio6m;
        private System.Windows.Forms.Label radioLightLabel;
        private System.Windows.Forms.Label radio6mLabel;
        private System.Windows.Forms.RadioButton radioLight;
        private System.Windows.Forms.Label radio9cmLabel;
        private System.Windows.Forms.RadioButton radio2m;
        private System.Windows.Forms.Label radio13cmLabel;
        private System.Windows.Forms.RadioButton radio9cm;
        private System.Windows.Forms.Label radio2mLabel;
        private System.Windows.Forms.Label radio23cmLabel;
        private System.Windows.Forms.RadioButton radio1m;
        private System.Windows.Forms.RadioButton radio13cm;
        private System.Windows.Forms.Label radio33cmLabel;
        private System.Windows.Forms.Label radio1mLabel;
        private System.Windows.Forms.RadioButton radio23cm;
        private System.Windows.Forms.Label radio70cmLabel;
        private System.Windows.Forms.RadioButton radio70cm;
        private System.Windows.Forms.RadioButton radio33cm;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox myCallsignTextbox;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button createLogBtn;
        private System.Windows.Forms.Label ctzLabel;
        private System.Windows.Forms.Label utcLabel;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel14;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel11;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel12;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel18;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel17;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel15;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel16;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel20;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ListBox modeBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel13;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Button gridTXSelect5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button gridTXSelect4;
        private System.Windows.Forms.Button gridTXSelect1;
        private System.Windows.Forms.Button gridTXSelect3;
        private System.Windows.Forms.Button gridTXSelect2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel19;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.CheckBox enable6m;
        private System.Windows.Forms.CheckBox enableLight;
        private System.Windows.Forms.CheckBox enable9cm;
        private System.Windows.Forms.CheckBox enable13cm;
        private System.Windows.Forms.CheckBox enable23cm;
        private System.Windows.Forms.CheckBox enable33cm;
        private System.Windows.Forms.CheckBox enable70cm;
        private System.Windows.Forms.CheckBox enable1m;
        private System.Windows.Forms.CheckBox enable2m;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel21;
        private System.Windows.Forms.ToolStripMenuItem importCSVToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportCSVToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dBBrowserToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem renumberRowsToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel22;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox contestType;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.ComboBox operatorCategory;
        private System.Windows.Forms.ComboBox stationCategory;
        private System.Windows.Forms.ComboBox transmitterCategory;
        private System.Windows.Forms.ComboBox assisted;
        private System.Windows.Forms.ToolStripMenuItem overwriteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem updateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem appendToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel23;
        private System.Windows.Forms.Button dupeCSVButton;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.ListBox errorBox;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.ToolStripStatusLabel buildLabel;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.CheckBox network_mode_checkbox;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.NumericUpDown updateSecs;
        private System.Windows.Forms.CheckBox enable5cm;
        private System.Windows.Forms.Label radio5cmLabel;
        private System.Windows.Forms.RadioButton radio5cm;
    }
}

