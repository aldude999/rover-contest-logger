﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ContestLoggers
{
    public partial class cabrilloExport : Form
    {
        DBData[] logExport;
        string logCallsign;
        string logContest;
        string logOperatorCategory;
        string logStationCategory;
        string logTransmitterCategory;
        string logClaimedScore;
        public cabrilloExport(DBData[] contestLog, string callsign, string contest, string operatorCategory, string stationCategory, string transmitterCategory, string claimedScore)
        {
            logExport = contestLog;
            logCallsign = callsign;
            logContest = contest;
            logOperatorCategory = operatorCategory;
            logStationCategory = stationCategory;
            logTransmitterCategory = transmitterCategory;
            logClaimedScore = claimedScore;
            InitializeComponent();
        }

        private void label11_Click(object sender, EventArgs e)
        {

        }

        private void callsign_TextChanged(object sender, EventArgs e)
        {

        }

        private void cabrilloExport_Load(object sender, EventArgs e)
        {
            callsign.Text = logCallsign;
            contestType.Text = logContest;
            operatorCategory.Text = logOperatorCategory;
            stationCategory.Text = logStationCategory;
            transmitterCategory.Text = logTransmitterCategory;
            claimedScore.Text = logClaimedScore;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var filePath = string.Empty;

            using (SaveFileDialog createDBFileDialog = new SaveFileDialog())
            {
                // openFileDialog.InitialDirectory = "c:\\";
                createDBFileDialog.Filter = "Text File (*.txt)|*.txt";
                createDBFileDialog.FilterIndex = 2;
                createDBFileDialog.RestoreDirectory = true;

                if (createDBFileDialog.ShowDialog() == DialogResult.OK)
                {
                    //Get the path of specified file
                    filePath = createDBFileDialog.FileName;
                    ImportExport.exportCabrillo(filePath, logExport, callsign.Text, contestType.Text, location.Text, operatorCategory.Text, stationCategory.Text, transmitterCategory.Text, power.Text, assisted.Text, bands.Text, modes.Text, club.Text, claimedScore.Text, operators.Text, name.Text, address.Text, city.Text, state.Text, zip.Text, country.Text, email.Text, soapbox.Text);

                    //Read the contents of the file into a stream
                    //var fileStream = createDBFileDialog.OpenFile();

                    // using (StreamReader reader = new StreamReader(fileStream))
                    // {
                    // fileContent = reader.ReadToEnd();
                    // }
                    this.Close();
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
