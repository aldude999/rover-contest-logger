﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace ContestLoggers
{
    class ImportExport
    {
        public static bool exportCSV(string fileName, DBData[] exportData)
        {
            var csvOutput = new StringBuilder();
            int counter = 0;
            csvOutput.AppendLine("Log Number,Freqency,Mode,Date Time,RX Call,RX Grid,TX Call, TX Grid");
            while (counter < exportData.Length)
            {
                csvOutput.AppendLine(exportData[counter].DBmainNumber + "," + Convert.ToString(exportData[counter].DBfreq) + "," + exportData[counter].DBtype + "," + exportData[counter].DBdateTime + "," + exportData[counter].DBmyCall + "," + exportData[counter].DBmyGrid + "," + exportData[counter].DBtheirCall + "," + exportData[counter].DBtheirGrid);
                counter++;
            }
            File.WriteAllText(fileName, csvOutput.ToString());
            return true;
        }

        public static bool importCSV(string importFilename, string dbFileName, int importMode)
        {
            // 1. Overwrite
            // 2. Update
            // 3. Append
            var newDatabase = new List<DBData>();
            try
            {
                using (var reader = new StreamReader(importFilename))
                {

                    while (!reader.EndOfStream)
                    {
                        try
                        {
                            var line = reader.ReadLine();
                            var values = line.Split(',');
                            var row = new DBData();
                            row.DBmainNumber = Convert.ToInt32(values[0]);
                            row.DBfreq = Convert.ToInt32(values[1]);
                            row.DBtype = values[2];
                            row.DBdateTime = values[3];
                            row.DBmyCall = values[4];
                            row.DBmyGrid = values[5];
                            row.DBtheirCall = values[6];
                            row.DBtheirGrid = values[7];
                            newDatabase.Add(row);
                        }
                        catch { } //Sometimes we get bad info, if we do, ignore the line


                    }
                }
            }
            catch (Exception ex1)
            {
                // Couldn't read the file, or file has an error
                Console.WriteLine(ex1);
                return false;
            }



            if (importMode == 1) //Overwrite
            {
                SQLFunc.customDBCommand(dbFileName, "DELETE FROM \"main\".\"ContestLog\";");
                SQLFunc.customDBCommand(dbFileName, "UPDATE SQLITE_SEQUENCE SET SEQ=0 WHERE NAME='ContestLog';");

                for (int i = 0; newDatabase.Count > i; i++)
                {
                    SQLFunc.writeDB(dbFileName, newDatabase[i]);
                }
                return true;
            }

            if (importMode == 2) //Update
            {
                for (int i = 0; newDatabase.Count > i; i++)
                {

                       DBData[] testme = SQLFunc.customDBQuery(dbFileName, "SELECT * FROM ContestLog WHERE Number=" + Convert.ToString(newDatabase[i].DBmainNumber) + ";");
                    if (testme.Length == 0)
                    {
                        SQLFunc.writeDB(dbFileName, newDatabase[i], newDatabase[i].DBmainNumber);
                    }
                    else
                    {
                        SQLFunc.updateDBEntry(dbFileName, newDatabase[i], newDatabase[i].DBmainNumber);
                    }
                    }
                return true;
            }

            if (importMode == 3) //Append 
            {
                for (int i = 0; newDatabase.Count > i; i++)
                {
                    try
                    {
                       DBData[] testme = SQLFunc.customDBQuery(dbFileName, "SELECT * FROM ContestLog WHERE Number="+ Convert.ToString(newDatabase[i].DBmainNumber)+";");
                        if (testme.Length == 0)
                        {
                            SQLFunc.writeDB(dbFileName, newDatabase[i], newDatabase[i].DBmainNumber);
                        }
                    }
                    catch {  } //If we fail, no big deal, we only want to add entries that don't exist yet!
                }
                return true;
            }

            return false;
        }

        public static bool exportCabrillo(string fileName, DBData[] exportData, string callsign, string contestType, string location, string categoryOperator, string categoryStation, string categoryTransmitter, string categoryPower, string categoryAssisted, string categoryBand, string categoryMode, string club, string claimedScore, string operators, string name, string address, string addressCity, string addressStateProvince, string addressPostalCode, string addressCountry, string email, string soapBox)
        {
            var cabrilloOutput = new StringBuilder();
            int counter = 0;
            //soapBox is multilined
            string[] soapBoxOutput = soapBox.Split(new[] { Environment.NewLine }, StringSplitOptions.None);



            //Headers


            cabrilloOutput.AppendLine("START-OF-LOG:\t3.0");
            cabrilloOutput.AppendLine("CONTEST:\t" + contestType);
            cabrilloOutput.AppendLine("CALLSIGN:\t" + callsign);
            cabrilloOutput.AppendLine("LOCATION:\t" + location);
            cabrilloOutput.AppendLine("CATEGORY-OPERATOR:\t" + categoryOperator);
            cabrilloOutput.AppendLine("CATEGORY-STATION:\t" + categoryStation);
            cabrilloOutput.AppendLine("CATEGORY-TRANSMITTER:\t" + categoryTransmitter);
            cabrilloOutput.AppendLine("CATEGORY-POWER:\t" + categoryPower);
            cabrilloOutput.AppendLine("CATEGORY-ASSISTED:\t" + categoryAssisted);
            cabrilloOutput.AppendLine("CATEGORY-BAND:\t" + categoryBand);
            cabrilloOutput.AppendLine("CATEGORY-MODE:\t" + categoryMode);
            cabrilloOutput.AppendLine("CLUB:\t" + club);
            cabrilloOutput.AppendLine("CLAIMED-SCORE:\t" + claimedScore);
            cabrilloOutput.AppendLine("OPERATORS:\t" + operators);
            cabrilloOutput.AppendLine("NAME:\t" + name);
            cabrilloOutput.AppendLine("ADDRESS:\t" + address);
            cabrilloOutput.AppendLine("ADDRESS-CITY:\t" + addressCity);
            cabrilloOutput.AppendLine("ADDRESS-STATE-PROVINCE:\t" + addressStateProvince);
            cabrilloOutput.AppendLine("ADDRESS-POSTALCODE:\t" + addressPostalCode);
            cabrilloOutput.AppendLine("ADDERSS-COUNTRY:\t" + addressCountry);
            cabrilloOutput.AppendLine("EMAIL:\t" + email);
            for (int i = 0; soapBoxOutput.Length > i; i++)
            {
                cabrilloOutput.AppendLine("SOAPBOX:" + soapBoxOutput[i]);
            }
            

            string dateRegexPattern = @":\d\d\s*$";
            while (counter < exportData.Length)
            {
                exportData[counter].DBdateTime = Regex.Replace(exportData[counter].DBdateTime, dateRegexPattern, ""); //Remove seconds
                exportData[counter].DBdateTime = exportData[counter].DBdateTime.Replace(":", ""); //Remove any colons
                cabrilloOutput.AppendLine("QSO:\t" + Convert.ToString(exportData[counter].DBfreq) + "\t" + exportData[counter].DBtype + "\t" + exportData[counter].DBdateTime + "\t" + exportData[counter].DBmyCall + "\t" + exportData[counter].DBmyGrid + "\t" + exportData[counter].DBtheirCall + "\t" + exportData[counter].DBtheirGrid);
                counter++;
            }

            cabrilloOutput.AppendLine("END-OF-LOG:");
            File.WriteAllText(fileName, cabrilloOutput.ToString());
            return true;

        }
    }
}
