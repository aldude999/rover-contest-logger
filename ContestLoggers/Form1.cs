﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace ContestLoggers
{
    public partial class Form1 : Form
    {
        //Used for grid square map calculations, lets make these Setting fields in the future so our program remembers our last one on applicaiton exit
        char rxMaiden1 = 'E';
        char rxMaiden2 = 'M';
        int rxMaiden3 = 1;
        int rxMaiden4 = 5;
        string rxMaiden = "EM15";

        char txMaiden1 = 'E';
        char txMaiden2 = 'M';
        int txMaiden3 = 1;
        int txMaiden4 = 5;
        string txMaiden = "EM15";
        //scoring globals
        int points;
        int multiplier;
        int score;
        //Database vars, view the end of SQLFunc.cs to see what a DBData is
        DBData[] CurrentDatabase;
        string dbfile = "";

        int currentFreq = 0;
        public Form1()
        {
            InitializeComponent();
        }

        //This function reads key inputs for hotkeys
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            //GRID RX Select
            if (keyData == (Keys.Control | Keys.O))
            {
                gridRX.Focus();
                gridRX.SelectAll();
                return true;
            }
            //CALL RX Select
            if (keyData == (Keys.Control | Keys.C))
            {
                callRX.Focus();
                callRX.SelectAll();
                return true;
            }
            //GRID TX Select
            if (keyData == (Keys.Alt | Keys.M))
            {
                gridTX.Focus();
                gridTX.SelectAll();
                return true;
            }
            //GRID RX HOTKEYS
            if (keyData == (Keys.Control | Keys.D1))
            {
                gridRXSelect1_Click(null, null);
                return true;
            }
            if (keyData == (Keys.Control | Keys.D2))
            {
                gridRXSelect2_Click(null, null);
                return true;
            }
            if (keyData == (Keys.Control | Keys.D3))
            {
                gridRXSelect3_Click(null, null);
                return true;
            }
            if (keyData == (Keys.Control | Keys.D4))
            {
                gridRXSelect4_Click(null, null);
                return true;
            }
            if (keyData == (Keys.Control | Keys.D5))
            {
                gridRXSelect5_Click(null, null);
                return true;
            }
            //CALL RX HOTKEYS
            if (keyData == (Keys.Control | Keys.D6))
            {
                callSelect1_Click(null, null);
                return true;
            }
            if (keyData == (Keys.Control | Keys.D7))
            {
                callSelect2_Click(null, null);
                return true;
            }
            if (keyData == (Keys.Control | Keys.D8))
            {
                callSelect3_Click(null, null);
                return true;
            }
            if (keyData == (Keys.Control | Keys.D9))
            {
                callSelect4_Click(null, null);
                return true;
            }
            if (keyData == (Keys.Control | Keys.D0))
            {
                callSelect5_Click(null, null);
                return true;
            }
            //GRID TX HOTKEYS
            if (keyData == (Keys.Alt | Keys.D1))
            {
                gridTXSelect1_Click(null, null);
                return true;
            }
            if (keyData == (Keys.Alt | Keys.D2))
            {
                gridTXSelect2_Click(null, null);
                return true;
            }
            if (keyData == (Keys.Alt | Keys.D3))
            {
                gridTXSelect3_Click(null, null);
                return true;
            }
            if (keyData == (Keys.Alt | Keys.D4))
            {
                gridTXSelect4_Click(null, null);
                return true;
            }
            if (keyData == (Keys.Alt | Keys.D5))
            {
                gridTXSelect5_Click(null, null);
                return true;
            }
            //FREQ HOTKEYS
            if (keyData == (Keys.F1))
            {
                radio6m.Checked = true;
                radio6m_CheckedChanged(null, null);
                return true;
            }
            if (keyData == (Keys.F2))
            {
                radio2m.Checked = true;
                radio2m_CheckedChanged(null, null);
                return true;
            }
            if (keyData == (Keys.F3))
            {
                radio1m.Checked = true;
                radio1m_CheckedChanged(null, null);
                return true;
            }
            if (keyData == (Keys.F4))
            {
                radio70cm.Checked = true;
                radio70cm_CheckedChanged(null, null);
                return true;
            }
            if (keyData == (Keys.F5))
            {
                radio33cm.Checked = true;
                radio33cm_CheckedChanged(null, null);
                return true;
            }
            if (keyData == (Keys.F6))
            {
                radio23cm.Checked = true;
                radio23cm_CheckedChanged(null, null);
                return true;
            }
            if (keyData == (Keys.F7))
            {
                radio13cm.Checked = true;
                radio13cm_CheckedChanged(null, null);
                return true;
            }
            if (keyData == (Keys.F8))
            {
                radio9cm.Checked = true;
                radio9cm_CheckedChanged(null, null);
                return true;
            }
            if (keyData == (Keys.F9))
            {
                radio5cm.Checked = true;
                radio5cm_CheckedChanged(null, null);
                return true;
            }
            if (keyData == (Keys.F10))
            {
                radioLight.Checked = true;
                radioLight_CheckedChanged(null, null);
                return true;
            }
            //SUBMIT HOTKEY
            if (keyData == (Keys.Enter))
            {
                submitButton_Click(null, null);
                return true;
            }
            //CLEAR HOTKEY
            if (keyData == (Keys.Escape))
            {
                clearButton_Click(null, null);
                return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void gridRXSelect1_Click(object sender, EventArgs e)
        {
            gridRX.Text = gridRXSelect1.Text;
        }

        private void gridRXSelect2_Click(object sender, EventArgs e)
        {
            gridRX.Text = gridRXSelect2.Text;
        }

        private void gridRXSelect3_Click(object sender, EventArgs e)
        {
            gridRX.Text = gridRXSelect3.Text;
        }

        private void gridRXSelect4_Click(object sender, EventArgs e)
        {
            gridRX.Text = gridRXSelect4.Text;
        }

        private void gridRXSelect5_Click(object sender, EventArgs e)
        {
            gridRX.Text = gridRXSelect5.Text;
        }

        private void gridTXSelect1_Click(object sender, EventArgs e)
        {
            gridTX.Text = gridTXSelect1.Text;
        }

        private void gridTXSelect2_Click(object sender, EventArgs e)
        {
            gridTX.Text = gridTXSelect2.Text;
        }

        private void gridTXSelect3_Click(object sender, EventArgs e)
        {
            gridTX.Text = gridTXSelect3.Text;
        }

        private void gridTXSelect4_Click(object sender, EventArgs e)
        {
            gridTX.Text = gridTXSelect4.Text;
        }

        private void gridTXSelect5_Click(object sender, EventArgs e)
        {
            gridTX.Text = gridTXSelect5.Text;
        }

        private void callSelect1_Click(object sender, EventArgs e)
        {
            callRX.Text = callSelect1.Text;
        }

        private void callSelect2_Click(object sender, EventArgs e)
        {
            callRX.Text = callSelect2.Text;
        }

        private void callSelect3_Click(object sender, EventArgs e)
        {
            callRX.Text = callSelect3.Text;
        }

        private void callSelect4_Click(object sender, EventArgs e)
        {
            callRX.Text = callSelect4.Text;
        }

        private void callSelect5_Click(object sender, EventArgs e)
        {
            callRX.Text = callSelect5.Text;
        }

        private void submitButton_Click(object sender, EventArgs e)
        {
            string previousScore = scoreLabel.Text;
            try
            {
                setrxGridButton(gridRX.Text);
                Recalculate_RXGrids();

                settxGridButton(gridTX.Text);
                Recalculate_TXGrids();
            }
            catch
            {
                statusLabel.Text = "Grid Entries incorrect!";
                errorBox.Items.Add(statusLabel.Text);
                errorBox.Items.Add(statusLabel.Text);
                return;
            }
            if (callRX.Text == "" | myCallsignTextbox.Text == "")
            {
                statusLabel.Text = "Check Callsigns!";
                errorBox.Items.Add(statusLabel.Text);
                errorBox.Items.Add(statusLabel.Text);
                return;
            }
            DBData toWrite = new DBData();
            currentFreq = getCurrentFreq();
            if (currentFreq != 0)
            {
                

                toWrite.DBfreq = currentFreq;
                toWrite.DBmyCall = myCallsignTextbox.Text;
                toWrite.DBmyGrid = gridTX.Text;
                toWrite.DBtheirCall = callRX.Text;
                toWrite.DBtheirGrid = gridRX.Text;
                toWrite.DBtype = modeBoxParse();
                toWrite.DBdateTime = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss");
                SQLFunc.writeDB(dbfile, toWrite);
                SQLFunc.readContestLogsFromDB(dbfile);
                statusLabel.ForeColor = Color.Black;
                statusLabel.Text = "Entry added!";
                errorBox.Items.Add(statusLabel.Text);
                updateCurrentLogs();
                if (scoreLabel.Text == previousScore)
                {
                    statusLabel.ForeColor = Color.Red;
                    statusLabel.Text = "Duplicate!";
                    errorBox.Items.Add(statusLabel.Text);

                }
                callRX.Text = "";
                gridRX.Text = "";
                //gridTX.Text = "";
                radio6m.Checked = false;
                radio2m.Checked = false;
                radio1m.Checked = false;
                radio70cm.Checked = false;
                radio33cm.Checked = false;
                radio23cm.Checked = false;
                radio13cm.Checked = false;
                radio9cm.Checked = false;
                radioLight.Checked = false;
                clearRadioColors();

            }
            else
            {
                statusLabel.Text = "Enter a frequency!";
                errorBox.Items.Add(statusLabel.Text);
            }


        }



        private void clearRadioColors()
        {
            radio6m.ForeColor = Color.White;
            radio2m.ForeColor = Color.White;
            radio1m.ForeColor = Color.White;
            radio70cm.ForeColor = Color.White;
            radio33cm.ForeColor = Color.White;
            radio23cm.ForeColor = Color.White;
            radio13cm.ForeColor = Color.White;
            radio9cm.ForeColor = Color.White;
            radio5cm.ForeColor = Color.White;
            radioLight.ForeColor = Color.White;

        }

        private void radio6m_CheckedChanged(object sender, EventArgs e)
        {
            clearRadioColors();
            radio6m.ForeColor = Color.Red;
        }

        private void radio2m_CheckedChanged(object sender, EventArgs e)
        {
            clearRadioColors();
            radio2m.ForeColor = Color.Red;
        }

        private void radio1m_CheckedChanged(object sender, EventArgs e)
        {
            clearRadioColors();
            radio1m.ForeColor = Color.Red;
        }

        private void radio70cm_CheckedChanged(object sender, EventArgs e)
        {
            clearRadioColors();
            radio70cm.ForeColor = Color.Red;
        }

        private void radio33cm_CheckedChanged(object sender, EventArgs e)
        {
            clearRadioColors();
            radio33cm.ForeColor = Color.Red;
        }

        private void radio23cm_CheckedChanged(object sender, EventArgs e)
        {
            clearRadioColors();
            radio23cm.ForeColor = Color.Red;
        }

        private void radio13cm_CheckedChanged(object sender, EventArgs e)
        {
            clearRadioColors();
            radio13cm.ForeColor = Color.Red;
        }

        private void radio9cm_CheckedChanged(object sender, EventArgs e)
        {
            clearRadioColors();
            radio9cm.ForeColor = Color.Red;
        }
        private void radio5cm_CheckedChanged(object sender, EventArgs e)
        {
            clearRadioColors();
            radio5cm.ForeColor = Color.Red;
        }

        private void radioLight_CheckedChanged(object sender, EventArgs e)
        {
            clearRadioColors();
            radioLight.ForeColor = Color.Red;
        }

        private void fileToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void clearButton_Click(object sender, EventArgs e)
        {
            callRX.Text = "";
            gridRX.Text = "";
            //gridTX.Text = "";
            radio6m.Checked = false;
            radio2m.Checked = false;
            radio1m.Checked = false;
            radio70cm.Checked = false;
            radio33cm.Checked = false;
            radio23cm.Checked = false;
            radio13cm.Checked = false;
            radio9cm.Checked = false;
            radioLight.Checked = false;
            clearRadioColors();
        }

        private void oKROVERToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            statusLabel.ForeColor = Color.Black;
            statusLabel.Text = "Reading Database...";
            errorBox.Items.Add(statusLabel.Text);
            Update();
            var fileContent = string.Empty;
            var filePath = string.Empty;
            
            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                // openFileDialog.InitialDirectory = "c:\\";
                openFileDialog.Filter = "GCL files (*.gcl)|*.gcl";
                openFileDialog.FilterIndex = 2;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    //Get the path of specified file
                    filePath = openFileDialog.FileName;

                    //Read the contents of the file into a stream
                    // var fileStream = openFileDialog.OpenFile();

                    // using (StreamReader reader = new StreamReader(fileStream))
                    //{
                    // fileContent = reader.ReadToEnd();
                    //}
                    dbfile = filePath;
                    Properties.Settings.Default.SettingLastDB = dbfile;
                    Properties.Settings.Default.Save();
                    //CurrentDatabase = SQLFunc.readContestLogsFromDB(filePath);
                    updateCurrentLogs();
                }
            }
            // SQLFunc.connectToDB(filePath);
        }

        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var fileContent = string.Empty;
            var filePath = string.Empty;

            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                // openFileDialog.InitialDirectory = "c:\\";
                openFileDialog.Filter = "GCL files (*.gcl)|*.gcl";
                openFileDialog.FilterIndex = 2;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    //Get the path of specified file
                    filePath = openFileDialog.FileName;

                    //Read the contents of the file into a stream
                    var fileStream = openFileDialog.OpenFile();

                    using (StreamReader reader = new StreamReader(fileStream))
                    {
                        fileContent = reader.ReadToEnd();
                    }
                }
            }
           // SQLFunc.connectToDB(filePath);
     

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            fakeTab1.TabPages.Remove(tabPage5);
            Update();
            //cheap Elgethy font installer
            string fontName = "Elgethy Upper Bold";
            float fontSize = 12;
            string fontName2 = "OCR A Extended";
            float fontSize2 = 12;

            using (Font fontTester = new Font(
                   fontName,
                   fontSize,
                   FontStyle.Regular,
                   GraphicsUnit.Pixel))
            {
                if (fontTester.Name == fontName)
                {
                    using (Font fontTester2 = new Font(fontName2, fontSize2, FontStyle.Regular, GraphicsUnit.Pixel))
                    {
                        if (fontTester2.Name == fontName2)
                        {
                            // Font exists
                        }
                        else
                        {
                            System.Windows.Forms.MessageBox.Show("Some fonts were not detected on this system. Click install on the window that opens up to install it.");
                            System.Diagnostics.Process.Start("OCRAEXT.TTF");
                            this.Close();
                        }
                    }
                }
                else
                {
                    using (Font fontTester2 = new Font(fontName2, fontSize2, FontStyle.Regular, GraphicsUnit.Pixel))
                    {
                        if (fontTester2.Name == fontName2)
                        {
                            // Font exists
                        }
                        else
                        {
                            System.Windows.Forms.MessageBox.Show("Some fonts were not detected on this system. Click install on the window that opens up to install it.");
                            System.Diagnostics.Process.Start("OCRAEXT.TTF");
                        }
                    }
                    System.Windows.Forms.MessageBox.Show("Some fonts were not detected on this system. Click install on the window that opens up to install it.");
                    System.Diagnostics.Process.Start("elgethy upper bold.ttf");
                    this.Close();
                    
                }
            }



            
            centerRX.Text = rxMaiden;
            centerTX.Text = txMaiden;
            Recalculate_RXGrids();
            Recalculate_TXGrids();
            timer1.Start();
            getEnabledBands();
            setEnabledBands();
            dbfile = Properties.Settings.Default.SettingLastDB;

            string buildDate = Properties.Resources.BuildDate;
            buildLabel.Text = "ALEX NAAS - " + buildDate;
            if (File.Exists(dbfile))
            {
                updateCurrentLogs();
            }
            else { }
            contestType_SettingCheck();

            
        }

        private void Recalculate_RXGrids()
        {
            //left
            int rxLeftNumber = rxMaiden3;
            char rxLeftLetter = rxMaiden1;
            if (rxMaiden3 == 0)
            {
                rxLeftNumber = 9;
                if (rxMaiden1 == 'A')
                {
                    rxLeftLetter = 'R';
                }
                else
                {
                    rxLeftLetter--;
                }
            }
            else
            {
                rxLeftNumber -= 1;
            }
            leftRX.Text = rxLeftLetter.ToString() + rxMaiden2.ToString() + rxLeftNumber.ToString() + rxMaiden4.ToString();
            //right
            int rxRightNumber;
            rxRightNumber = rxMaiden3;
            char rxRightLetter = rxMaiden1;
            if (rxMaiden3 == 9)
            {
                rxRightNumber = 0;
                if (rxMaiden1 == 'R')
                {
                    rxRightLetter = 'A';
                }
                else
                {
                    rxRightLetter++;
                }
            }
            else
            {
                rxRightNumber += 1;
            }
            rightRX.Text = rxRightLetter.ToString() + rxMaiden2.ToString() + rxRightNumber.ToString() + rxMaiden4.ToString();
            //upper
            int rxUpperNumber;
            char rxUpperLetter = rxMaiden2;
            rxUpperNumber = rxMaiden4;
            if (rxMaiden4 == 9)
            {
                rxUpperNumber = 0;
                if (rxMaiden2 == 'R')
                {
                    rxUpperLetter = 'A';
                }
                else
                {
                    rxUpperLetter++;
                }
            }
            else
            {
                rxUpperNumber += 1;
            }
            upperRX.Text = rxMaiden1.ToString() + rxUpperLetter.ToString() + rxMaiden3.ToString() + rxUpperNumber.ToString();
            //lower
            int rxLowerNumber;
            char rxLowerLetter = rxMaiden2;
            rxLowerNumber = rxMaiden4;
            if (rxMaiden4 == 0)
            {
                rxLowerNumber = 9;
                if (rxMaiden1 == 'A')
                {
                    rxLowerLetter = 'R';
                }
                else
                {
                    rxLowerLetter--;
                }
            }
            else
            {
                rxLowerNumber -= 1;
            }
            lowerRX.Text = rxMaiden1.ToString() + rxLowerLetter.ToString() + rxMaiden3.ToString() + rxLowerNumber.ToString();
            //upperleft
            upperLeftRX.Text = rxLeftLetter.ToString() + rxUpperLetter.ToString() + rxLeftNumber.ToString() + rxUpperNumber.ToString();
            //lowerleft
            lowerLeftRX.Text = rxLeftLetter.ToString() + rxLowerLetter.ToString() + rxLeftNumber.ToString() + rxLowerNumber.ToString();
            //upperright
            upperRightRX.Text = rxRightLetter.ToString() + rxUpperLetter.ToString() + rxRightNumber.ToString() + rxUpperNumber.ToString();
            //lowerright
            lowerRightRX.Text = rxRightLetter.ToString() + rxUpperLetter.ToString() + rxRightNumber.ToString() + rxLowerNumber.ToString();
        }
        private void Recalculate_TXGrids()
        {
            //left
            int txLeftNumber = txMaiden3;
            char txLeftLetter = txMaiden1;
            if (txMaiden3 == 0)
            {
                txLeftNumber = 9;
                txLeftLetter--;
            }
            else
            {
                txLeftNumber -= 1;
            }
            leftTX.Text = txLeftLetter.ToString() + txMaiden2.ToString() + txLeftNumber.ToString() + txMaiden4.ToString();
            //right
            int txRightNumber;
            txRightNumber = txMaiden3;
            char txRightLetter = txMaiden1;
            if (txMaiden3 == 9)
            {
                txRightNumber = 0;
                txRightLetter++;
            }
            else
            {
                txRightNumber += 1;
            }
            rightTX.Text = txRightLetter.ToString() + txMaiden2.ToString() + txRightNumber.ToString() + txMaiden4.ToString();
            //upper
            int txUpperNumber;
            char txUpperLetter = txMaiden2;
            txUpperNumber = txMaiden4;
            if (txMaiden4 == 9)
            {
                txUpperNumber = 0;
                txUpperLetter++;
            }
            else
            {
                txUpperNumber += 1;
            }
            upperTX.Text = txMaiden1.ToString() + txUpperLetter.ToString() + txMaiden3.ToString() + txUpperNumber.ToString();
            //lower
            int txLowerNumber;
            char txLowerLetter = txMaiden2;
            txLowerNumber = txMaiden4;
            if (txMaiden4 == 0)
            {
                txLowerNumber = 9;
                txLowerLetter--;
            }
            else
            {
                txLowerNumber -= 1;
            }
            lowerTX.Text = txMaiden1.ToString() + txLowerLetter.ToString() + txMaiden3.ToString() + txLowerNumber.ToString();
            //upperleft
            upperLeftTX.Text = txLeftLetter.ToString() + txUpperLetter.ToString() + txLeftNumber.ToString() + txUpperNumber.ToString();
            //lowerleft
            lowerLeftTX.Text = txLeftLetter.ToString() + txLowerLetter.ToString() + txLeftNumber.ToString() + txLowerNumber.ToString();
            //upperright
            upperRightTX.Text = txRightLetter.ToString() + txUpperLetter.ToString() + txRightNumber.ToString() + txUpperNumber.ToString();
            //lowerright
            lowerRightTX.Text = txRightLetter.ToString() + txUpperLetter.ToString() + txRightNumber.ToString() + txLowerNumber.ToString();
        }
        private void setrxGridButton(string output)
        {
            char[] outputText = output.ToCharArray();
            rxMaiden1 = outputText[0];
            rxMaiden2 = outputText[1];
            rxMaiden3 = Convert.ToInt32(Char.GetNumericValue(outputText[2]));
            rxMaiden4 = Convert.ToInt32(Char.GetNumericValue(outputText[3]));
            centerRX.Text = rxMaiden1.ToString() + rxMaiden2.ToString() + rxMaiden3.ToString() + rxMaiden4.ToString();

        }
        private void settxGridButton(string output)
        {
            char[] outputText = output.ToCharArray();
            txMaiden1 = outputText[0];
            txMaiden2 = outputText[1];
            txMaiden3 = Convert.ToInt32(Char.GetNumericValue(outputText[2]));
            txMaiden4 = Convert.ToInt32(Char.GetNumericValue(outputText[3]));
            centerTX.Text = txMaiden1.ToString() + txMaiden2.ToString() + txMaiden3.ToString() + txMaiden4.ToString();

        }

        private void centerRX_Click(object sender, EventArgs e)
        {
            gridRX.Text = centerRX.Text;
        }
        private void upperRX_Click(object sender, EventArgs e)
        {
            gridRX.Text = upperRX.Text;
            setrxGridButton(upperRX.Text);


            Recalculate_RXGrids();
        }

        private void lowerRX_Click(object sender, EventArgs e)
        {
            gridRX.Text = lowerRX.Text;
            setrxGridButton(lowerRX.Text);


            Recalculate_RXGrids();
        }

        private void leftRX_Click(object sender, EventArgs e)
        {
            gridRX.Text = leftRX.Text;
            setrxGridButton(leftRX.Text);


            Recalculate_RXGrids();
        }

        private void rightRX_Click(object sender, EventArgs e)
        {
            gridRX.Text = rightRX.Text;
            setrxGridButton(rightRX.Text);


            Recalculate_RXGrids();
        }

        private void upperLeftRX_Click(object sender, EventArgs e)
        {
            gridRX.Text = upperLeftRX.Text;
            setrxGridButton(upperLeftRX.Text);


            Recalculate_RXGrids();
        }

        private void lowerLeftRX_Click(object sender, EventArgs e)
        {
            gridRX.Text = lowerLeftRX.Text;
            setrxGridButton(lowerLeftRX.Text);


            Recalculate_RXGrids();
        }

        private void upperRightRX_Click(object sender, EventArgs e)
        {
            gridRX.Text = upperRightRX.Text;
            setrxGridButton(upperRightRX.Text);


            Recalculate_RXGrids();
        }

        private void lowerRightRX_Click(object sender, EventArgs e)
        {
            gridRX.Text = lowerRightRX.Text;
            setrxGridButton(lowerRightRX.Text);


            Recalculate_RXGrids();
        }

        private void centerTX_Click(object sender, EventArgs e)
        {
            gridTX.Text = centerTX.Text;
        }
        private void upperTX_Click(object sender, EventArgs e)
        {
            gridTX.Text = upperTX.Text;
            settxGridButton(upperTX.Text);


            Recalculate_TXGrids();
        }

        private void lowerTX_Click(object sender, EventArgs e)
        {
            gridTX.Text = lowerTX.Text;
            settxGridButton(lowerTX.Text);


            Recalculate_TXGrids();
        }

        private void leftTX_Click(object sender, EventArgs e)
        {
            gridTX.Text = leftTX.Text;
            settxGridButton(leftTX.Text);


            Recalculate_TXGrids();
        }

        private void rightTX_Click(object sender, EventArgs e)
        {
            gridTX.Text = rightTX.Text;
            settxGridButton(rightTX.Text);


            Recalculate_TXGrids();
        }

        private void upperLeftTX_Click(object sender, EventArgs e)
        {
            gridTX.Text = upperLeftTX.Text;
            settxGridButton(upperLeftTX.Text);


            Recalculate_TXGrids();
        }

        private void lowerLeftTX_Click(object sender, EventArgs e)
        {
            gridTX.Text = lowerLeftTX.Text;
            settxGridButton(lowerLeftTX.Text);


            Recalculate_TXGrids();
        }

        private void upperRightTX_Click(object sender, EventArgs e)
        {
            gridTX.Text = upperRightTX.Text;
            settxGridButton(upperRightTX.Text);


            Recalculate_TXGrids();
        }

        private void lowerRightTX_Click(object sender, EventArgs e)
        {
            gridTX.Text = lowerRightTX.Text;
            settxGridButton(lowerRightTX.Text);


            Recalculate_TXGrids();
        }

        private void statusStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void groupBox6_Enter(object sender, EventArgs e)
        {

        }

        private void createLogBtn_Click(object sender, EventArgs e)
        {
          //  var fileContent = string.Empty;
            var filePath = string.Empty;

            using (SaveFileDialog createDBFileDialog = new SaveFileDialog())
            {
                // openFileDialog.InitialDirectory = "c:\\";
                createDBFileDialog.Filter = "GCL files (*.gcl)|*.gcl";
                createDBFileDialog.FilterIndex = 2;
                createDBFileDialog.RestoreDirectory = true;

                if (createDBFileDialog.ShowDialog() == DialogResult.OK)
                {
                    //Get the path of specified file
                    filePath = createDBFileDialog.FileName;
                    SQLFunc.createDB(filePath);
                    Properties.Settings.Default.SettingLastDB = filePath;
                    Properties.Settings.Default.Save();
                    //Read the contents of the file into a stream
                    //var fileStream = createDBFileDialog.OpenFile();

                    // using (StreamReader reader = new StreamReader(fileStream))
                    // {
                    // fileContent = reader.ReadToEnd();
                    // }
                    dbfile = filePath;
                    updateCurrentLogs();
                }
            }
            
        }
        private void updateCurrentLogs()
        {
            CurrentDatabase = SQLFunc.readContestLogsFromDB(dbfile);
            logBox.Items.Clear();
            int counter = CurrentDatabase.Length;
            recalculateQuickSelect();
            updateScore(contestType.Text);
            try
            {
                while(counter > 0)
                {
                    counter--;
                    logBox.Items.Add(Convert.ToString(counter +1)+ " QSO:\t" + Convert.ToString(CurrentDatabase[counter].DBfreq) +"MHz\t" +CurrentDatabase[counter].DBtype +"\t" + CurrentDatabase[counter].DBdateTime+"\t"+CurrentDatabase[counter].DBmyCall+"\t"+CurrentDatabase[counter].DBmyGrid+"\t"+CurrentDatabase[counter].DBtheirCall+"\t"+CurrentDatabase[counter].DBtheirGrid);
                    myCallsignTextbox.Text = CurrentDatabase[counter].DBmyCall;
                    
                    qsoLabel.Text = "QSOs:" + Convert.ToString(CurrentDatabase.Length);

                }
                if (CurrentDatabase.Length != 0)
                {
                  //  statusLabel.ForeColor = Color.Black;
                  //  statusLabel.Text = "DB Read! "; Debugging code basically
                   // errorBox.Items.Add(statusLabel.Text);
                }
                else
                {
                    if (File.Exists(dbfile) == false)
                    {
                        statusLabel.ForeColor = Color.Red;
                        statusLabel.Text = "Is a DB Loaded?";
                        errorBox.Items.Add(statusLabel.Text);
                    }
                    qsoLabel.Text = "QSOs:" + Convert.ToString(CurrentDatabase.Length);
                    logBox.Items.Clear();

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                //updateCurrentLogs();
            }
            catch (Exception ex)
            {
                statusLabel.Text = "Load a DB!";
                errorBox.Items.Add(statusLabel.Text);
            }
            ctzLabel.Text = "LOCAL:" + DateTime.Now.ToString("HH:mm:ss");
            utcLabel.Text = "UTC:" + DateTime.UtcNow.ToString("HH:mm:ss");
            //if (network_mode_checkbox.Checked == true)
            //{
            //    updateCurrentLogs();
            //} //Moved to timer2
        }

        private void utcLabel_Click(object sender, EventArgs e)
        {
            timer1_Tick(null, null);
        }

        private void tabPage2_Click(object sender, EventArgs e)
        {

        }

        private int getCurrentFreq()
        {
            if (radio5cm.Checked == true)
            {
                return 57000;
            }
            if (radio13cm.Checked == true)
            {
                return 2300;
            }
            else if (radio1m.Checked == true) {
                return 222;
            }
            else if (radio23cm.Checked == true) {
                return 1200;
            }
            else if (radio2m.Checked == true)
            {
                return 144;
            }
            else if (radio33cm.Checked == true)
            {
                return 902;
            }
            else if (radio6m.Checked == true)
            {
                return 50;
            }
            else if (radio70cm.Checked == true)
            {
                return 432;
            }
            else if (radio9cm.Checked == true)
            {
                return 3400;
            }
            else if (radioLight.Checked == true)
            {
                return 275000;
            }
            else
            {
                return 0;
            }

        }

        private void logBox_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {

            
            clearButton_Click(null, null); //Fix this, we need to change this to it's own function
            gridRX.Text = CurrentDatabase[logBox.Items.Count - (logBox.SelectedIndex + 1)].DBtheirGrid;
            gridTX.Text = CurrentDatabase[logBox.Items.Count - (logBox.SelectedIndex + 1)].DBmyGrid;
            callRX.Text = CurrentDatabase[logBox.Items.Count - (logBox.SelectedIndex + 1)].DBtheirCall;

                setrxGridButton(gridRX.Text);
                Recalculate_RXGrids();

                settxGridButton(gridTX.Text);
                Recalculate_TXGrids();
                //Radio buttons are hard, this needs to be it's own function
                if (CurrentDatabase[logBox.Items.Count - (logBox.SelectedIndex + 1)].DBfreq == 2300)
            {
                radio13cm.Checked = true;
            }
                else if (CurrentDatabase[logBox.Items.Count - (logBox.SelectedIndex + 1)].DBfreq == 5700)
                {
                    radio5cm.Checked = true;
            }
            else if (CurrentDatabase[logBox.Items.Count - (logBox.SelectedIndex + 1)].DBfreq == 222)
            {
                radio1m.Checked = true;
            }
            else if (CurrentDatabase[logBox.Items.Count - (logBox.SelectedIndex + 1)].DBfreq == 1200)
            {
                radio23cm.Checked = true;
            }
            else if (CurrentDatabase[logBox.Items.Count - (logBox.SelectedIndex + 1)].DBfreq == 144)
            {
                radio2m.Checked = true;
            }
            else if (CurrentDatabase[logBox.Items.Count - (logBox.SelectedIndex + 1)].DBfreq == 902)
            {
                radio33cm.Checked = true;
            }
            else if (CurrentDatabase[logBox.Items.Count - (logBox.SelectedIndex + 1)].DBfreq == 50)
            {
                radio6m.Checked = true;
            }
            else if (CurrentDatabase[logBox.Items.Count - (logBox.SelectedIndex + 1)].DBfreq == 432)
            {
                radio70cm.Checked = true;
            }
            else if (CurrentDatabase[logBox.Items.Count - (logBox.SelectedIndex + 1)].DBfreq == 3400)
            {
                radio9cm.Checked = true;
            }
            else if (CurrentDatabase[logBox.Items.Count - (logBox.SelectedIndex + 1)].DBfreq == 275000)
            {
                radioLight.Checked = true;
            }
            else
            {
                    throw new Exception(); //Create a proper "Frequency Wrong" Exception
            }

            }
            catch
            {
                System.Windows.Forms.MessageBox.Show("Something went wrong with your database entry! You can recover from this error by filling in all the required fields and clicking \"Update Entry\"");

            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            DBData toWrite = new DBData();
            int updateEntry = 0;
            currentFreq = getCurrentFreq();
            if (callRX.Text == "" | myCallsignTextbox.Text == "")
            {
                statusLabel.Text = "Check Callsigns!";
                errorBox.Items.Add(statusLabel.Text);
                return;
            }
            if (currentFreq != 0)
            {
                try 
                {
                    updateEntry = CurrentDatabase[logBox.Items.Count - (logBox.SelectedIndex + 1)].DBmainNumber;
                }
                catch
                {
                    statusLabel.Text = "No log selected!";
                    errorBox.Items.Add(statusLabel.Text);
                    return;
                }
                
                toWrite.DBfreq = currentFreq;
                toWrite.DBmyCall = myCallsignTextbox.Text;
                toWrite.DBmyGrid = gridTX.Text;
                toWrite.DBtheirCall = callRX.Text;
                toWrite.DBtheirGrid = gridRX.Text;
                toWrite.DBtype = modeBoxParse();
                //toWrite.DBdateTime = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss");
                SQLFunc.updateDBEntry(dbfile, toWrite, updateEntry);
                SQLFunc.readContestLogsFromDB(dbfile);
                updateCurrentLogs();
                updateScore(contestType.Text);
                callRX.Text = "";
                gridRX.Text = "";
                //gridTX.Text = "";
                radio6m.Checked = false;
                radio2m.Checked = false;
                radio1m.Checked = false;
                radio70cm.Checked = false;
                radio33cm.Checked = false;
                radio23cm.Checked = false;
                radio13cm.Checked = false;
                radio9cm.Checked = false;
                radio5cm.Checked = false;
                radioLight.Checked = false;
                clearRadioColors();
            }
            else
            {
                statusLabel.Text = "Enter a frequency!";
                errorBox.Items.Add(statusLabel.Text);
            }


        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Are you sure you want to delete this entry?", "Delete Database Entry", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                try
                {
                    SQLFunc.deleteDBEntry(dbfile, CurrentDatabase[logBox.Items.Count - (logBox.SelectedIndex + 1)].DBmainNumber);
                }
                catch
                {
                    statusLabel.Text = "No log entry!";
                    errorBox.Items.Add(statusLabel.Text);
                    return;
                }
                
                SQLFunc.readContestLogsFromDB(dbfile);
                updateCurrentLogs();
                updateScore(contestType.Text);
                clearButton_Click(null, null); //needs to be it's own function
            }
            else if (dialogResult == DialogResult.No)
            {
                return;
            }
        }
        private string modeBoxParse()
        {
            if (modeBox.SelectedIndex == 0)
            {
                return "PH";
            }
            else if (modeBox.SelectedIndex == 1)
            {
                return "DG";
            }
            else if (modeBox.SelectedIndex == 2)
            {
                return "CW";
            }
            else
            {
                return "PH";
            }
        }

        private bool recalculateQuickSelect()
        {
            List<string> callSigns = CurrentDatabase.Select(CurrentDatabase => CurrentDatabase.DBtheirCall).Reverse().Distinct().ToList();
            List<string> rxGrid = CurrentDatabase.Select(CurrentDatabase => CurrentDatabase.DBmyGrid).Reverse().Distinct().ToList();
            List<string> txGrid = CurrentDatabase.Select(CurrentDatabase => CurrentDatabase.DBtheirGrid).Reverse().Distinct().ToList();

            //Cheap way to make sure there are 5 entries in each list, can this be done better?
            for (int counter = callSigns.Count(); counter <= 4;counter++)
            {
                callSigns.Add("");
            }
            for (int counter = rxGrid.Count(); counter <= 4; counter++)
            {
                rxGrid.Add("");
            }
            for (int counter = txGrid.Count(); counter <= 4; counter++)
            {
                txGrid.Add("");
            }

            gridRXSelect1.Text = txGrid[0];
            gridRXSelect2.Text = txGrid[1];
            gridRXSelect3.Text = txGrid[2];
            gridRXSelect4.Text = txGrid[3];
            gridRXSelect5.Text = txGrid[4];

            gridTXSelect1.Text = rxGrid[0];
            gridTXSelect2.Text = rxGrid[1];
            gridTXSelect3.Text = rxGrid[2];
            gridTXSelect4.Text = rxGrid[3];
            gridTXSelect5.Text = rxGrid[4];

            callSelect1.Text = callSigns[0];
            callSelect2.Text = callSigns[1];
            callSelect3.Text = callSigns[2];
            callSelect4.Text = callSigns[3];
            callSelect5.Text = callSigns[4];



            return true;
        }

        private void tableLayoutPanel3_Paint(object sender, PaintEventArgs e)
        {

        }
        private void setEnabledBands()
        {
            radio6mLabel.Visible = enable6m.Checked;
            radio6m.Visible = enable6m.Checked;
            Properties.Settings.Default.Setting6m = enable6m.Checked;

            radio2mLabel.Visible = enable2m.Checked;
            radio2m.Visible = enable2m.Checked;
            Properties.Settings.Default.Setting2m = enable2m.Checked;

            radio1mLabel.Visible = enable1m.Checked;
            radio1m.Visible = enable1m.Checked;
            Properties.Settings.Default.Setting1m = enable1m.Checked;

            radio70cmLabel.Visible = enable70cm.Checked;
            radio70cm.Visible = enable70cm.Checked;
            Properties.Settings.Default.Setting70cm = enable70cm.Checked;

            radio33cmLabel.Visible = enable33cm.Checked;
            radio33cm.Visible = enable33cm.Checked;
            Properties.Settings.Default.Setting33cm = enable33cm.Checked;

            radio23cmLabel.Visible = enable23cm.Checked;
            radio23cm.Visible = enable23cm.Checked;
            Properties.Settings.Default.Setting23cm = enable23cm.Checked;

            radio13cmLabel.Visible = enable13cm.Checked;
            radio13cm.Visible = enable13cm.Checked;
            Properties.Settings.Default.Setting13cm = enable13cm.Checked;

            radio9cmLabel.Visible = enable9cm.Checked;
            radio9cm.Visible = enable9cm.Checked;
            Properties.Settings.Default.Setting9cm = enable9cm.Checked;

            radio5cmLabel.Visible = enable5cm.Checked;
            radio5cm.Visible = enable5cm.Checked;
            Properties.Settings.Default.Setting5cm = enable5cm.Checked;

            radioLightLabel.Visible = enableLight.Checked;
            radioLight.Visible = enableLight.Checked;
            Properties.Settings.Default.SettingLight = enableLight.Checked;

            Properties.Settings.Default.Save();

        }

        private void getEnabledBands()
        {
            enable6m.Checked = Properties.Settings.Default.Setting6m;
            enable2m.Checked = Properties.Settings.Default.Setting2m;
            enable1m.Checked = Properties.Settings.Default.Setting1m;
            enable70cm.Checked = Properties.Settings.Default.Setting70cm;
            enable33cm.Checked = Properties.Settings.Default.Setting33cm;
            enable23cm.Checked = Properties.Settings.Default.Setting23cm;
            enable13cm.Checked = Properties.Settings.Default.Setting13cm;
            enable9cm.Checked = Properties.Settings.Default.Setting9cm;
            enable5cm.Checked = Properties.Settings.Default.Setting5cm;
            enableLight.Checked = Properties.Settings.Default.SettingLight;
        }

        private void enable6m_Clicked(object sender, EventArgs e)
        {
            setEnabledBands();
        }

        private void exportCSVToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var filePath = string.Empty;

            using (SaveFileDialog createDBFileDialog = new SaveFileDialog())
            {
                // openFileDialog.InitialDirectory = "c:\\";
                createDBFileDialog.Filter = "CSV file (*.csv)|*.csv";
                createDBFileDialog.FilterIndex = 2;
                createDBFileDialog.RestoreDirectory = true;

                if (createDBFileDialog.ShowDialog() == DialogResult.OK)
                {
                    //Get the path of specified file
                    filePath = createDBFileDialog.FileName;

                    ImportExport.exportCSV(filePath, CurrentDatabase);
                }
            }
            
        }

        private void cabrilloToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            cabrilloExport export = new cabrilloExport(CurrentDatabase, myCallsignTextbox.Text, contestType.Text, operatorCategory.Text, stationCategory.Text, transmitterCategory.Text, Convert.ToString(score));
            export.ShowDialog();
        }

        private void pointsLabel_Click(object sender, EventArgs e)
        {

        }
        private void updateScore(string contestName)
        {
            int rxtxGridsCounter = 0;
            List<string> rxgrids = new List<string>();
            List<string> txgrids = new List<string>();
            while (rxtxGridsCounter < CurrentDatabase.Length)
            {
                rxgrids.Add(CurrentDatabase[rxtxGridsCounter].DBtheirGrid + "," + CurrentDatabase[rxtxGridsCounter].DBfreq);
                txgrids.Add(CurrentDatabase[rxtxGridsCounter].DBmyGrid);
                rxtxGridsCounter++;
               // System.IO.File.WriteAllLines("RXGridMults.txt", rxgrids.Distinct().ToList());
               // System.IO.File.WriteAllLines("TXGridMults.txt", txgrids.Distinct().ToList());
            }
            if (contestName == "ARRL-VHF-JUN")
            {
                
                ScoreTable JuneScores = new ScoreTable();
                JuneScores.m6 = 1;
                JuneScores.m2 = 1;
                JuneScores.m1 = 2;
                JuneScores.cm70 = 2;
                JuneScores.cm33 = 3;
                JuneScores.cm23 = 3;
                JuneScores.cm13 = 4;
                JuneScores.cm9 = 4;
                JuneScores.cm5 = 4;
                JuneScores.light = 4;
                points = Scoring.calculatePoints(JuneScores, CurrentDatabase);
                pointsLabel.Text = "Points:" + Convert.ToString(points);
                multiplier = Scoring.calculateMultipliers(rxgrids.Distinct().ToList(), txgrids.Distinct().ToList(), true);
                multiplierLabel.Text = "Mult:" + Convert.ToString(multiplier);
                score = points * multiplier;
                scoreLabel.Text = "Score:" + Convert.ToString(score);
                
                if (transmitterCategory.Text != "UNLIMITED")
                {
                    string Input = Scoring.checkLimitedRoverRules(CurrentDatabase);
                    if (Input != "")
                    {
                        statusLabel.ForeColor = Color.DarkGray;
                        statusLabel.Text = "Too Many Entries: Check Log";
                        errorBox.Items.Add("Limited Rover Warning: " + Input + ">");
                        errorBox.Items.Add("Please select \"Unlimited\" or remove entries to stop these messages");
                    }
                    
                }

            }
            else if (contestName == "ARRL-VHF-SEP")
            {
                ScoreTable SeptemberScores = new ScoreTable();
                SeptemberScores.m6 = 1;
                SeptemberScores.m2 = 1;
                SeptemberScores.m1 = 2;
                SeptemberScores.cm70 = 2;
                SeptemberScores.cm33 = 3;
                SeptemberScores.cm23 = 3;
                SeptemberScores.cm13 = 4;
                SeptemberScores.cm9 = 4;
                SeptemberScores.cm5 = 4;
                SeptemberScores.light = 4;
                points = Scoring.calculatePoints(SeptemberScores, CurrentDatabase);
                pointsLabel.Text = "Points:" + Convert.ToString(points);
                multiplier = Scoring.calculateMultipliers(rxgrids.Distinct().ToList(), txgrids.Distinct().ToList(), true);
                multiplierLabel.Text = "Mult:" + Convert.ToString(multiplier);
                score = points * multiplier;
                scoreLabel.Text = "Score:" + Convert.ToString(score);
                Scoring.checkLimitedRoverRules(CurrentDatabase);
                if (transmitterCategory.Text != "UNLIMITED")
                {
                    string Input = Scoring.checkLimitedRoverRules(CurrentDatabase);
                    if (Input != "")
                    {
                        statusLabel.ForeColor = Color.DarkGray;
                        statusLabel.Text = "Too Many Entries: Check Log";
                        errorBox.Items.Add("Limited Rover Warning: " + Input+">");
                        errorBox.Items.Add("Please select \"Unlimited\" or remove entries to stop these messages");
                    }

                }
            }
            else if (contestName == "ARRL-VHF-JAN")
            {
                ScoreTable JanuaryScores = new ScoreTable();
                JanuaryScores.m6 = 1;
                JanuaryScores.m2 = 1;
                JanuaryScores.m1 = 2;
                JanuaryScores.cm70 = 2;
                JanuaryScores.cm33 = 4;
                JanuaryScores.cm23 = 4;
                JanuaryScores.cm13 = 8;
                JanuaryScores.cm9 = 8;
                JanuaryScores.cm5 = 8;
                JanuaryScores.light = 8;
                points = Scoring.calculatePoints(JanuaryScores, CurrentDatabase);
                pointsLabel.Text = "Points:" + Convert.ToString(points);
                multiplier = Scoring.calculateMultipliers(rxgrids.Distinct().ToList(), txgrids.Distinct().ToList(), true);
                multiplierLabel.Text = "Mult:" + Convert.ToString(multiplier);
                score = points * multiplier;
                scoreLabel.Text = "Score:" + Convert.ToString(score);
                Scoring.checkLimitedRoverRules(CurrentDatabase);
                if (transmitterCategory.Text != "UNLIMITED")
                {
                    string Input = Scoring.checkLimitedRoverRules(CurrentDatabase);
                    if (Input != "")
                    {
                        statusLabel.ForeColor = Color.DarkGray;
                        statusLabel.Text = "Too Many Entries: Check Log";
                        errorBox.Items.Add("Limited Rover Warning: " + Input + ">");
                        errorBox.Items.Add("Please select \"Unlimited\" or remove entries to stop these messages");
                    }

                }
            }
            else
            {
                pointsLabel.Text = "Points: 0";
                multiplierLabel.Text = "Mult: 0";
                scoreLabel.Text = "Score: 0";
            }
        }

        private void overwriteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string filePath = "";
            filePath = openCSV();
            if (filePath != "")
            {
                ImportExport.importCSV(filePath, dbfile, 1);
                updateCurrentLogs();
                statusLabel.Text = "Imported!";
                errorBox.Items.Add(statusLabel.Text);
            }
            
        }

        private void updateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string filePath = "";
            filePath = openCSV();
            if (filePath != "")
            {
                ImportExport.importCSV(filePath, dbfile, 2);
                updateCurrentLogs();
                statusLabel.Text = "Imported!";
                errorBox.Items.Add(statusLabel.Text);
            }
        }

        private void appendToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string filePath = "";
            filePath = openCSV();
            if (filePath != "")
            {
                ImportExport.importCSV(filePath, dbfile, 3);
                updateCurrentLogs();
                statusLabel.Text = "Imported!";
                errorBox.Items.Add(statusLabel.Text);
            }
        }

        private string openCSV()
        {
            var filePath = string.Empty;
            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                // openFileDialog.InitialDirectory = "c:\\";
                openFileDialog.Filter = "CSV File (*.csv)|*.CSV";
                openFileDialog.FilterIndex = 2;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    //Get the path of specified file
                    filePath = openFileDialog.FileName;

                    //Read the contents of the file into a stream
                    // var fileStream = openFileDialog.OpenFile();

                    // using (StreamReader reader = new StreamReader(fileStream))
                    //{
                    // fileContent = reader.ReadToEnd();
                    //}
                    return filePath;
                }
                else
                {
                    return "";
                }

            }
        }

        private void renumberRowsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                ImportExport.exportCSV("temp", CurrentDatabase);
                ImportExport.importCSV("temp", dbfile, 1);
                File.Delete("temp");
                statusLabel.Text = "Renumbered!";
                errorBox.Items.Add(statusLabel.Text);
            }
            catch
            {
                statusLabel.Text = "No Write Permissions!";
                errorBox.Items.Add(statusLabel.Text);
            }

        }

        private void dupeCSVButton_Click(object sender, EventArgs e)
        {
            ScoreTable nullTable = new ScoreTable();
            nullTable.m6 = 0;
            nullTable.m2 = 0;
            nullTable.m1 = 0;
            nullTable.cm70 = 0;
            nullTable.cm33 = 0;
            nullTable.cm23 = 0;
            nullTable.cm13 = 0;
            nullTable.cm9 = 0;
            nullTable.light = 0;
            points = Scoring.calculatePoints(nullTable, CurrentDatabase, true);
        }

        private void contestType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (contestType.Text == "ARRL-VHF-JAN")
            {
                Properties.Settings.Default.SettingLastContestType = 1;
            }
            else if (contestType.Text == "ARRL-VHF-JUN")
            {
                Properties.Settings.Default.SettingLastContestType = 2;
            }
            else if (contestType.Text == "ARRL-VHF-SEP")
            {
                Properties.Settings.Default.SettingLastContestType = 3;
            }
            else if (contestType.Text == "CUSTOM")
            {
                Properties.Settings.Default.SettingLastContestType = 4;
            }
            Properties.Settings.Default.Save();
            updateScore(contestType.Text);
        }
        private void contestType_SettingCheck()
        {
            if (Properties.Settings.Default.SettingLastContestType == 1)
            {
                contestType.Text = "ARRL-VHF-JAN";
            }
            else if (Properties.Settings.Default.SettingLastContestType == 2)
            {
                contestType.Text = "ARRL-VHF-JUN";
            }
            else if (Properties.Settings.Default.SettingLastContestType == 3)
            {
                contestType.Text = "ARRL-VHF-SEP";
            }
            else if (Properties.Settings.Default.SettingLastContestType == 4)
            {
                contestType.Text = "CUSTOM";
            }
        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.SettingDebug = checkBox3.Checked;
            if (checkBox3.Checked == true)
            {
                fakeTab1.TabPages.Add(tabPage5);
            }
            else
            {
                fakeTab1.TabPages.Remove(tabPage5);
            }
            Properties.Settings.Default.Save();
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
                updateCurrentLogs();
            errorBox.Items.Add("Updated log from network!");
        }

        private void network_mode_checkbox_CheckedChanged(object sender, EventArgs e)
        {
            if (network_mode_checkbox.Checked == true)
            {
                timer2.Start();
            }
            else
            {
                timer2.Stop();
            }
        }

        private void updateSecs_ValueChanged(object sender, EventArgs e)
        {
            timer2.Interval = Convert.ToInt32(updateSecs.Value) * 1000;
        }

        private void enable9cm_CheckedChanged(object sender, EventArgs e)
        {
            setEnabledBands();
        }

        private void enable2m_CheckedChanged(object sender, EventArgs e)
        {
            setEnabledBands();
        }

        private void enable6m_CheckedChanged(object sender, EventArgs e)
        {
            setEnabledBands();
        }

        private void enable1m_CheckedChanged(object sender, EventArgs e)
        {
            setEnabledBands();
        }

        private void enable70cm_CheckedChanged(object sender, EventArgs e)
        {
            setEnabledBands();
        }

        private void enable33cm_CheckedChanged(object sender, EventArgs e)
        {
            setEnabledBands();
        }

        private void enable23cm_CheckedChanged(object sender, EventArgs e)
        {
            setEnabledBands();
        }

        private void enable13cm_CheckedChanged(object sender, EventArgs e)
        {
            setEnabledBands();
        }

        private void enable5cm_CheckedChanged(object sender, EventArgs e)
        {
            setEnabledBands();
        }

        private void enableLight_CheckedChanged(object sender, EventArgs e)
        {
            setEnabledBands();
        }
    }



    }
