﻿namespace ContestLoggers
{
    partial class cabrilloExport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.contestType = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.transmitterCategory = new System.Windows.Forms.ComboBox();
            this.power = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.assisted = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.bands = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.modes = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.address = new System.Windows.Forms.TextBox();
            this.city = new System.Windows.Forms.TextBox();
            this.state = new System.Windows.Forms.TextBox();
            this.zip = new System.Windows.Forms.TextBox();
            this.country = new System.Windows.Forms.TextBox();
            this.name = new System.Windows.Forms.TextBox();
            this.operators = new System.Windows.Forms.TextBox();
            this.stationCategory = new System.Windows.Forms.ComboBox();
            this.operatorCategory = new System.Windows.Forms.ComboBox();
            this.soapbox = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.callsign = new System.Windows.Forms.TextBox();
            this.location = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.club = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.claimedScore = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.email = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button1.Font = new System.Drawing.Font("Elgethy Upper Bold", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.Black;
            this.button1.Location = new System.Drawing.Point(188, 6);
            this.button1.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(170, 51);
            this.button1.TabIndex = 0;
            this.button1.Text = "EXPORT";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button2.Font = new System.Drawing.Font("Elgethy Upper Bold", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.Black;
            this.button2.Location = new System.Drawing.Point(6, 6);
            this.button2.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(170, 51);
            this.button2.TabIndex = 1;
            this.button2.Text = "CANCEL";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(37, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(149, 24);
            this.label1.TabIndex = 2;
            this.label1.Text = "CONTEST TYPE";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(63, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(97, 24);
            this.label2.TabIndex = 3;
            this.label2.Text = "CALLSIGN";
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(63, 122);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(97, 24);
            this.label3.TabIndex = 4;
            this.label3.Text = "LOCATION";
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(18, 244);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(188, 24);
            this.label4.TabIndex = 5;
            this.label4.Text = "STATION CATEGORY";
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(36, 306);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(152, 24);
            this.label5.TabIndex = 6;
            this.label5.Text = "STATION RULES";
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(74, 368);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(75, 24);
            this.label6.TabIndex = 7;
            this.label6.Text = "POWER";
            // 
            // contestType
            // 
            this.contestType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.contestType.FormattingEnabled = true;
            this.contestType.Items.AddRange(new object[] {
            "ARRL-VHF-JAN",
            "ARRL-VHF-JUN",
            "ARRL-VHF-SEP"});
            this.contestType.Location = new System.Drawing.Point(3, 27);
            this.contestType.Name = "contestType";
            this.contestType.Size = new System.Drawing.Size(218, 32);
            this.contestType.TabIndex = 8;
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 182);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(209, 24);
            this.label7.TabIndex = 9;
            this.label7.Text = "OPERATOR CATEGORY";
            // 
            // transmitterCategory
            // 
            this.transmitterCategory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.transmitterCategory.FormattingEnabled = true;
            this.transmitterCategory.Items.AddRange(new object[] {
            "ONE",
            "TWO",
            "LIMITED",
            "UNLIMITED"});
            this.transmitterCategory.Location = new System.Drawing.Point(3, 333);
            this.transmitterCategory.Name = "transmitterCategory";
            this.transmitterCategory.Size = new System.Drawing.Size(218, 32);
            this.transmitterCategory.TabIndex = 10;
            // 
            // power
            // 
            this.power.Dock = System.Windows.Forms.DockStyle.Fill;
            this.power.FormattingEnabled = true;
            this.power.Items.AddRange(new object[] {
            "QRP",
            "LOW",
            "HIGH"});
            this.power.Location = new System.Drawing.Point(3, 395);
            this.power.Name = "power";
            this.power.Size = new System.Drawing.Size(218, 32);
            this.power.TabIndex = 11;
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(63, 430);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(97, 24);
            this.label8.TabIndex = 12;
            this.label8.Text = "ASSISTED";
            // 
            // assisted
            // 
            this.assisted.Dock = System.Windows.Forms.DockStyle.Fill;
            this.assisted.FormattingEnabled = true;
            this.assisted.Items.AddRange(new object[] {
            "ASSISTED",
            "NON-ASSISTED"});
            this.assisted.Location = new System.Drawing.Point(3, 457);
            this.assisted.Name = "assisted";
            this.assisted.Size = new System.Drawing.Size(218, 32);
            this.assisted.TabIndex = 13;
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(108, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(70, 24);
            this.label9.TabIndex = 14;
            this.label9.Text = "BANDS";
            // 
            // bands
            // 
            this.bands.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bands.FormattingEnabled = true;
            this.bands.Items.AddRange(new object[] {
            "ALL",
            "VHF-3-BAND",
            "VHF-FM-ONLY",
            "50",
            "144",
            "223",
            "440",
            "900",
            "1200",
            "2400",
            "3400",
            "LIGHT"});
            this.bands.Location = new System.Drawing.Point(3, 27);
            this.bands.Name = "bands";
            this.bands.Size = new System.Drawing.Size(281, 32);
            this.bands.TabIndex = 15;
            // 
            // label10
            // 
            this.label10.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(107, 62);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(72, 24);
            this.label10.TabIndex = 16;
            this.label10.Text = "MODES";
            // 
            // modes
            // 
            this.modes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.modes.FormattingEnabled = true;
            this.modes.Items.AddRange(new object[] {
            "MIXED",
            "SSB",
            "CW",
            "RTTY",
            "DIGITAL"});
            this.modes.Location = new System.Drawing.Point(3, 89);
            this.modes.Name = "modes";
            this.modes.Size = new System.Drawing.Size(281, 32);
            this.modes.TabIndex = 17;
            // 
            // label11
            // 
            this.label11.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(84, 124);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(118, 24);
            this.label11.TabIndex = 18;
            this.label11.Text = "OPERATORS";
            this.label11.Click += new System.EventHandler(this.label11_Click);
            // 
            // label12
            // 
            this.label12.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(113, 184);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(60, 24);
            this.label12.TabIndex = 19;
            this.label12.Text = "NAME";
            // 
            // label13
            // 
            this.label13.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(96, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(94, 24);
            this.label13.TabIndex = 20;
            this.label13.Text = "ADDRESS";
            // 
            // label14
            // 
            this.label14.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(336, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(49, 24);
            this.label14.TabIndex = 21;
            this.label14.Text = "CITY";
            // 
            // label15
            // 
            this.label15.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(438, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(70, 24);
            this.label15.TabIndex = 22;
            this.label15.Text = "STATE";
            // 
            // label16
            // 
            this.label16.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(514, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(118, 24);
            this.label16.TabIndex = 23;
            this.label16.Text = "ZIP/POSTAL";
            // 
            // label17
            // 
            this.label17.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(665, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(94, 24);
            this.label17.TabIndex = 24;
            this.label17.Text = "COUNTRY";
            // 
            // address
            // 
            this.address.Dock = System.Windows.Forms.DockStyle.Fill;
            this.address.Location = new System.Drawing.Point(3, 27);
            this.address.Name = "address";
            this.address.Size = new System.Drawing.Size(281, 30);
            this.address.TabIndex = 25;
            // 
            // city
            // 
            this.city.Dock = System.Windows.Forms.DockStyle.Fill;
            this.city.Location = new System.Drawing.Point(290, 27);
            this.city.Name = "city";
            this.city.Size = new System.Drawing.Size(142, 30);
            this.city.TabIndex = 26;
            // 
            // state
            // 
            this.state.Dock = System.Windows.Forms.DockStyle.Fill;
            this.state.Location = new System.Drawing.Point(438, 27);
            this.state.Name = "state";
            this.state.Size = new System.Drawing.Size(70, 30);
            this.state.TabIndex = 27;
            // 
            // zip
            // 
            this.zip.Dock = System.Windows.Forms.DockStyle.Fill;
            this.zip.Location = new System.Drawing.Point(514, 27);
            this.zip.Name = "zip";
            this.zip.Size = new System.Drawing.Size(118, 30);
            this.zip.TabIndex = 28;
            // 
            // country
            // 
            this.country.Dock = System.Windows.Forms.DockStyle.Fill;
            this.country.Location = new System.Drawing.Point(638, 27);
            this.country.Name = "country";
            this.country.Size = new System.Drawing.Size(148, 30);
            this.country.TabIndex = 29;
            // 
            // name
            // 
            this.name.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.name.Location = new System.Drawing.Point(3, 221);
            this.name.Name = "name";
            this.name.Size = new System.Drawing.Size(281, 30);
            this.name.TabIndex = 30;
            // 
            // operators
            // 
            this.operators.Dock = System.Windows.Forms.DockStyle.Fill;
            this.operators.Location = new System.Drawing.Point(3, 151);
            this.operators.Name = "operators";
            this.operators.Size = new System.Drawing.Size(281, 30);
            this.operators.TabIndex = 31;
            // 
            // stationCategory
            // 
            this.stationCategory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.stationCategory.FormattingEnabled = true;
            this.stationCategory.Items.AddRange(new object[] {
            "ROVER",
            "LIMITED ROVER"});
            this.stationCategory.Location = new System.Drawing.Point(3, 271);
            this.stationCategory.Name = "stationCategory";
            this.stationCategory.Size = new System.Drawing.Size(218, 32);
            this.stationCategory.TabIndex = 32;
            // 
            // operatorCategory
            // 
            this.operatorCategory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.operatorCategory.FormattingEnabled = true;
            this.operatorCategory.Items.AddRange(new object[] {
            "ROVER"});
            this.operatorCategory.Location = new System.Drawing.Point(3, 209);
            this.operatorCategory.Name = "operatorCategory";
            this.operatorCategory.Size = new System.Drawing.Size(218, 32);
            this.operatorCategory.TabIndex = 33;
            // 
            // soapbox
            // 
            this.soapbox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.soapbox.Location = new System.Drawing.Point(3, 27);
            this.soapbox.Multiline = true;
            this.soapbox.Name = "soapbox";
            this.soapbox.Size = new System.Drawing.Size(407, 179);
            this.soapbox.TabIndex = 34;
            // 
            // label18
            // 
            this.label18.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(159, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(94, 24);
            this.label18.TabIndex = 35;
            this.label18.Text = "SOAPBOX";
            // 
            // callsign
            // 
            this.callsign.Dock = System.Windows.Forms.DockStyle.Fill;
            this.callsign.Location = new System.Drawing.Point(3, 89);
            this.callsign.Name = "callsign";
            this.callsign.Size = new System.Drawing.Size(218, 30);
            this.callsign.TabIndex = 36;
            this.callsign.TextChanged += new System.EventHandler(this.callsign_TextChanged);
            // 
            // location
            // 
            this.location.Dock = System.Windows.Forms.DockStyle.Fill;
            this.location.Location = new System.Drawing.Point(3, 149);
            this.location.Name = "location";
            this.location.Size = new System.Drawing.Size(218, 30);
            this.location.TabIndex = 37;
            // 
            // label19
            // 
            this.label19.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(509, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(58, 24);
            this.label19.TabIndex = 38;
            this.label19.Text = "CLUB";
            // 
            // club
            // 
            this.club.Dock = System.Windows.Forms.DockStyle.Fill;
            this.club.Location = new System.Drawing.Point(290, 27);
            this.club.Name = "club";
            this.club.Size = new System.Drawing.Size(496, 30);
            this.club.TabIndex = 39;
            // 
            // label20
            // 
            this.label20.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(461, 62);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(154, 24);
            this.label20.TabIndex = 40;
            this.label20.Text = "CLAIMED SCORE";
            // 
            // claimedScore
            // 
            this.claimedScore.Dock = System.Windows.Forms.DockStyle.Fill;
            this.claimedScore.Location = new System.Drawing.Point(290, 89);
            this.claimedScore.Name = "claimedScore";
            this.claimedScore.Size = new System.Drawing.Size(496, 30);
            this.claimedScore.TabIndex = 41;
            // 
            // label21
            // 
            this.label21.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(506, 184);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(63, 24);
            this.label21.TabIndex = 42;
            this.label21.Text = "EMAIL";
            // 
            // email
            // 
            this.email.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.email.Location = new System.Drawing.Point(290, 221);
            this.email.Name = "email";
            this.email.Size = new System.Drawing.Size(496, 30);
            this.email.TabIndex = 43;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.contestType, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.callsign, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.location, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.label7, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.operatorCategory, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.stationCategory, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 10);
            this.tableLayoutPanel1.Controls.Add(this.transmitterCategory, 0, 11);
            this.tableLayoutPanel1.Controls.Add(this.label6, 0, 12);
            this.tableLayoutPanel1.Controls.Add(this.power, 0, 13);
            this.tableLayoutPanel1.Controls.Add(this.label8, 0, 14);
            this.tableLayoutPanel1.Controls.Add(this.assisted, 0, 15);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 16;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(224, 581);
            this.tableLayoutPanel1.TabIndex = 44;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 5;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.Controls.Add(this.label13, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.address, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.label14, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.city, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.label15, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.state, 2, 1);
            this.tableLayoutPanel2.Controls.Add(this.label16, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.zip, 3, 1);
            this.tableLayoutPanel2.Controls.Add(this.label17, 4, 0);
            this.tableLayoutPanel2.Controls.Add(this.country, 4, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 274);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.Size = new System.Drawing.Size(789, 83);
            this.tableLayoutPanel2.TabIndex = 45;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel3.Controls.Add(this.label9, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.bands, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.label19, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.club, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.label10, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.email, 1, 8);
            this.tableLayoutPanel3.Controls.Add(this.claimedScore, 1, 3);
            this.tableLayoutPanel3.Controls.Add(this.label21, 1, 7);
            this.tableLayoutPanel3.Controls.Add(this.label20, 1, 2);
            this.tableLayoutPanel3.Controls.Add(this.name, 0, 8);
            this.tableLayoutPanel3.Controls.Add(this.operators, 0, 6);
            this.tableLayoutPanel3.Controls.Add(this.label12, 0, 7);
            this.tableLayoutPanel3.Controls.Add(this.modes, 0, 3);
            this.tableLayoutPanel3.Controls.Add(this.label11, 0, 5);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 9;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.Size = new System.Drawing.Size(789, 265);
            this.tableLayoutPanel3.TabIndex = 46;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel4.AutoSize = true;
            this.tableLayoutPanel4.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Controls.Add(this.button1, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.button2, 0, 0);
            this.tableLayoutPanel4.Location = new System.Drawing.Point(422, 149);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(364, 63);
            this.tableLayoutPanel4.TabIndex = 47;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 1;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Controls.Add(this.label18, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.soapbox, 0, 1);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 2;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(413, 209);
            this.tableLayoutPanel5.TabIndex = 48;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.AutoSize = true;
            this.tableLayoutPanel6.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel6.ColumnCount = 2;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel6.Controls.Add(this.tableLayoutPanel5, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.tableLayoutPanel4, 1, 0);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(3, 363);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 1;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(789, 215);
            this.tableLayoutPanel6.TabIndex = 49;
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 1;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.Controls.Add(this.tableLayoutPanel3, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.tableLayoutPanel6, 0, 2);
            this.tableLayoutPanel7.Controls.Add(this.tableLayoutPanel2, 0, 1);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(233, 3);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 3;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel7.Size = new System.Drawing.Size(795, 581);
            this.tableLayoutPanel7.TabIndex = 50;
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 2;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel8.Controls.Add(this.tableLayoutPanel1, 0, 0);
            this.tableLayoutPanel8.Controls.Add(this.tableLayoutPanel7, 1, 0);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 1;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(1031, 587);
            this.tableLayoutPanel8.TabIndex = 51;
            // 
            // cabrilloExport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(1031, 587);
            this.Controls.Add(this.tableLayoutPanel8);
            this.Font = new System.Drawing.Font("Elgethy Upper Bold", 17.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.White;
            this.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.Name = "cabrilloExport";
            this.Text = "cabrilloExport";
            this.Load += new System.EventHandler(this.cabrilloExport_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel7.PerformLayout();
            this.tableLayoutPanel8.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox contestType;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox transmitterCategory;
        private System.Windows.Forms.ComboBox power;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox assisted;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox bands;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox modes;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox address;
        private System.Windows.Forms.TextBox city;
        private System.Windows.Forms.TextBox state;
        private System.Windows.Forms.TextBox zip;
        private System.Windows.Forms.TextBox country;
        private System.Windows.Forms.TextBox name;
        private System.Windows.Forms.TextBox operators;
        private System.Windows.Forms.ComboBox stationCategory;
        private System.Windows.Forms.ComboBox operatorCategory;
        private System.Windows.Forms.TextBox soapbox;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox callsign;
        private System.Windows.Forms.TextBox location;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox club;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox claimedScore;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox email;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
    }
}