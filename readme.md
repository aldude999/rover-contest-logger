# Rover Contest Logger

Rover Contest Logger is a response to the lack of good contest logging software made for roving in the ham radio community.

## TODO

Add more scoring rules

Add Cabrillo Import

Add Contacts Per Hour Calculations

Post Tab

Bug fixes, I know there will be a bunch right now, If you encounter one, please send me an email. For the time being, you can press Continue, and nothing should break or corrupt your database.

Future: User Defined Score Calculations

Future: Custom Frequencies

Future: Switch between Most Used and Last Unique for Quick Select

Future: DB Logs, so if one makes a mistake, they can view changes they've made and revert to a specific changes

Future: Visual Tab

## Installation

At the moment, there is no installer, simply download the [.zip file](https://bitbucket.org/aldude999/rover-contest-logger/downloads/ContestLogger_UI_Mockup.zip) and extract it somewhere. Right click on the two .ttf files
and click Install to install the proper fonts needed for this application.


## Usage

For a Quick Start on testing:

1. Run the ContestLoggers.exe program

2. Click "Create A New Database" or use the premade Database in the program folder, "Test Database.gcl"

3. Set the Callsign and Bands you want to enable

4. Select the Contest Tab

5. You can Type in your RX Grid, RX Call, and TX Grid, or use the quick select buttons or grid map. You can select a mode, otherwise Phone will be selected by default. Make sure to Select a Band as well.

6. Clicking Submit will add your entry to the database

7. To edit an entry, click the entry you want to edit to load it into the fields, make the changes you want, and click "Update Entry"

8. To delete an entry, select the entry you want to delete, and click "Delete Entry". It will prompt you to make sure you want to delete the entry.

## Contributing
Members of the OKRover team are free to contribute. You will need to register on BitBucket, and download VS2019 Community Edition. You can then click "Clone or Check Out Code" in Visual Studio, and paste in the link from
clicking the Clone button at the top of this page. When you Push code, you will have to enter your BitBucket credentials.

Send me an email for push permissions on this project.

## License
FREEWARE ONLY, Do not release the source code please. This is for the members of the OKRover team only!

## [Changelog](https://bitbucket.org/aldude999/rover-contest-logger/commits/)